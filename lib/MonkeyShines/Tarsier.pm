package MonkeyShines::Tarsier;

use warnings;
use strict;
use Carp;

our $VERSION = '1.0';

use File::Path qw(make_path);

use MonkeyShines::Utils qw (
	read_from_file
	read_from_delim
	save_to_file
	coord_union_all
	random_select
	flip_hash
	execute_check
);

use Exporter;
our @ISA = qw(Exporter);
our @EXPORT_OK = qw(
	download_rfam rarefy_rfam parse_rfam
	stockholm_sequences find_nearest
	rnaz rnaz2 rnaz_inferred
	cmfinder cmfinder_inferred cmfinder_inferred_all
	evofold evofold_inferred
	inferred_accuracy
	parse_species
);

# This module contains wrapper scripts that execute various bits of software.
# If these are in the user's execution path, the global variable $bin_dir
# can simply be an empty string; otherwise it needs to be the path to the
# directory that contains the executables (or that contains symbolic links to
# the executables).
my $bin_dir = "";

# The easiest and safest way to call scripts in the directory below the
# current one is to hard-code the top-level MonkeyShines directory.
my $monkeyshines_dir = "~/MonkeyShines";

=pod

=head1 Name

MonkeyShines::Tarsier - Testing and Analysing RNA gene Software,
Including Evolutionary Relationships..

=head1 Description

MonkeyShines::Tarsier is a collection of functions for using genomic
and /or Rfam data to test RNA gene prediction programs (currently,
CMfinder, EvoFold, and RNAz versions 1 and 2).

This module contains wrapper scripts that execute various bits of software.
Generally, these rely on the software being in the user's execution path;
an alternative is to set the global variable $bin_dir in this module to the
path to the directory that contains the executables (or that contains
symbolic links to the executables).

=head1 Functions

=head2 Parse Local Rfam Data

=over

=item B<C<undef> = download_rfam($data_dir[, $dataset][, $ftp_url][, $build][, $unzip])>

Download all of the Rfam text files that are required by other functions.
They will be saved in "$data_dir/rfam", which will be created if it doesn't
already exist. The default $ftp_url is the Sanger site, and the default
build is 'CURRENT'. The files are gzipped, and need to be unzipped for
subsequent processing; so by default they are unzipped. But if you want to
postpone that till later, set $unzip to be zero.

=item B<$rarefied_file = rarefy_rfam($data_dir, $prefix, $species)>

In order to save a lot of execution time later, redundant parts of one of
the Rfam text files are removed. Unnecessary columns can be removed, as can
rows that relate to species that we are not interested in. The resultant file
is stored as $data_dir/rfam/rfamseq_min_$prefix.txt, which is
returned as $rarefied_file. The species of interest are specified as an
arrayref of NCBI Taxonomy IDs.

=item B<($structure, $sequences, $accessions) = parse_rfam($data_dir, $rfam_id, $prefix, $species[, $seed])>

Using the Rfam data from the files in $data_dir/rfam, extract the data for
$rfam_id, and save different bits of information in separate files in
$data_dir/$rfam_id; $prefix is prepended to each of these file names.
The species of interest need to be specified as an arrayref of NCBI Taxonomy
IDs ($species). By default, the full dataset is processed, but to only use
the seed dataset set $seed to be true.

This function only processes the Rfam data if necessary, and if the result
files already exist, they are simply read into the output variables.
$structure is in dot-bracket notation. $sequences is a hashref of sequences
indexed by the concatenation of EMBL ID and start-stop co-ordinates
(aka 'extended accession IDs'). $accessions is a hashref with
species as keys and arrayrefs of extended accession IDs as values.

=item B<($header, $structure, $sequences, $extended_accessions) = stockholm_sequences($alignment_file, $rfam_id, $accessions)>

Extract data from a Stockholm-formatted file, for a given Rfam ID
and a list of accession numbers; $accessions should be a hashref,
with accession IDs as keys and species names as values. The annotation text
is returned as a string ($header), along with the structure in dot-bracket
notation ($structure). $sequences is a hashref of the sequences, indexed
by the concatenation of the accession number and the start-stop co-ordinates.
$extended_accessions is a hashref with the accession number and the
start-stop co-ordinates as keys, and species names as values.

=back

=head2 RNA Gene Detection

All of these functions are designed to have generic input and output, which
is why I don't allow for a bunch of program-specific parameters to be passed.
The functions mostly use default or author-recommended parameters; but here and
there I use different settings if I think they make more sense. Also, note that
the bin and monkeyshines directories, which contain the executables and any
ancillary scripts or files, respectively, are global parameters. It's clearer
than passing them around, I think.

It should, then, be easy to add a new program to the Tarsier pipeline, by
creating a function here that takes an alignment file and an output location,
and which returns 1 or 0 depending on whether an RNA gene is found, or not. To
also do the analysis of RNA gene boundary detection, an '_inferred' function
is required, with two parameters: the relevant output from the program as a
string and the reference sequence. It returns a string of 1s and 0s, indicating
presence and absence of RNA.

=head3 RNAz

=over

=item B<$rna_gene = rnaz($maf_file, $output_dir)>

Execute RNAz for a maf/ama alignment, $maf_file; not clustal format,
otherwise the co-ordinates reported by the RNAz windowing script may be
incorrect. Mostly using default settings, with a strict cut-off for
reporting results.

Windowing is done by ./scripts/rnaz/rnazWindow.pl, with all the default
settings. This also reduces the alignment to a maximum of 6 species. RNAz
is executed against both strands, and gaps are retained in the output. The
results are saved to the file "$output_dir/rnaz_$basename.res", where
$basename is the name of the $maf_file sans filetype.

Using sliding windows means that the results need to be merged to get overall
values; another RNAz script, ./scripts/rnaz/rnazCluster.pl, does this, and its
output is saved to "$output_dir/rnaz_$basename.out", with a threshold
probability of 0.9. If this file is not empty, then RNAz has detected RNA,
and the return value of $rna_gene will be 1.

=item B<$rna_gene = rnaz2($maf_file, $output_dir)>

Mostly the same as the rnaz function, but we now have a limit of 32 species
(which could easily be raised further). And we use the dinucleotide
background model.

=item B<$inferred = rnaz_inferred($out, $ref_seq)>

To get the coordinates of the inferred RNA, the RNAz output, $out, is parsed.
RNAz reports the position of the detected RNA genes relative to the ungapped
reference sequence; so $ref_seq is used to convert these to be relative to
the original (gapped) alignment. The return value, $inferred, is a  string of
0s and 1s, indicating where the RNA is predicted.

This function works with output from either version of RNAz, since the format
has not changed between versions.

=back

=head3 CMfinder

=over

=item B<$rna_gene = cmfinder($seq_file, $out_dir)>

Execute CMfinder for a set of unaligned (but homologous) sequences, $seq_file,
in fasta format; and save the output files in $out_dir. The
./scripts/cmfinder/cmfinder.pl script is used to do the analysis, and this
script has been superficially altered from that provided by the original
authors, to place output files in customisable locations.

CMfinder can detect varying levels of motif complexity; this function searches
for 5 single stem-loop motifs and 5 double stem-loop motifs, and the results
are merged with ./scripts/cmfinder/CombMotif.pl. These are the author-recommended
parameters.

Depending on the settings, several motifs could be detected. The CMfinder
executable 'summarize' is used to collate data on each motif, and the results
are stored in "$out_dir/cmfinder_$basename.out", where $basename is the name
of the $seq_file sans filetype. In addition to the variables reported by the
summarize program, the start and end points of each motif are calculated,
as are the species, which can vary from motif to motif. Finally, a 'Rank'
value is calculated, which combines various other values, in a seemingly ad
hoc manner, as per the Torarinsson et al (2008) paper. We use the thresholds
from that paper to determine if an RNA gene has been detected: Rank > 5 and
Energy < -5. These may not be appropriate in all, or even any, cases, but in
lieu of a biologically or statistically meaningful way of determining success,
there is no other option.

=item B<$inferred = cmfinder_inferred($out, $ref_seq)>

To get the coordinates of the inferred RNA, the CMfinder output, $out, is
parsed. In determining where the RNA gene has been detected, the motif with
the highest 'Rank' (above a threshold of 5) is selected, as long as the
'Energy' value is less than -5. In the unlikely event that we have two
identical Ranks, one is selected randomly. CMfinder reports the position of
the detected RNA gene relative to the ungapped reference sequence; so $ref_seq
is used to convert these to be relative to the original (gapped) alignment.
The return value, $inferred, is a  string of 0s and 1s, indicating where the
RNA is predicted.

=item B<$rna_gene = evofold($maf_file, $tree_file, $out_dir)>

Execute EvoFold for a maf/ama alignment, $maf_file, and a tree, $tree_file.
Save the output files in $out_dir. The full results are stored in
"$out_dir/evofold_$basename.res", where $basename is the name of the $maf_file
sans filetype; the RNA gene region(s), if detected, are saved in
"$out_dir/evofold_$basename.out", and reported with a return value of 1 or 0.

=item B<$inferred = evofold_inferred($out, $ref_seq)>

EvoFold reports the position of the RNA relative to the gapped alignment, so
the position of inferred RNA genes is easily parsed from the output string,
$out. $ref_seq is not actually required, but is retained for consistency with
the other '_inferred' functions. The return value, $inferred, is a  string of
0s and 1s, indicating where the RNA is predicted.

=item B<[%results | $results = inferred_accuracy($pattern, $inferred)>

Given two strings of 1s and 0s, $pattern and $inferred, calculate where they
are the same/different. $pattern represents the true RNA gene, as a continuous
block of 1s, possibly with 0s either side as flanking sequence. $inferred
shows where the program has predicted RNA as 1s, possibly in disjoint blocks.
The two strings are compared character by character (effectively, base by base)
and true/false positives/negatives are summed across the whole string (ie
alignment). Sensitivity, Specificity, and MCC are calculated, and all of the
statistics are returned as a hash or a hashref, with those statistics as keys.

=back

=head2 Process Species Data

=head3 Parse a file containing species information

=over

=item B<$data | @data | %data = parse_species($species_file[, $format])>

Extract species names and/or IDS from a text file, $species_file, in
tab-delimited format (without a header row), with columns showing the
NCBI taxonomy ID, the scientific name, the common name, Rfam name, and
UCSC name. The common and Rfam names may be empty strings. If the
optional $format parameter is 'tax_id', 'species', 'common', 'rfam', or
'ucsc' the function returns the appropriate column as an array (or arrayref).

UCSC names are the most practical; they're short, mnemonic (if sometimes
inconsistent), and don't contain spaces. So it's useful to have a hash
structure that allows us to easily look up the UCSC, given any other form
of name. If no format is provided, such a hash (or hashref) is returned,
with the other IDs as hash keys, and UCSC names as values.

=back

=head1 Author

James Allen
(james@monkeyshines.co.uk)

=head1 Copyright

Copyright 2009-2011 James Allen; University of Manchester

=head1 Licence

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program. If not, see <http://www.gnu.org/licenses/>.

=cut

# SVN Date: $Date: 2012-02-21 17:12:37 +0000 (Tue, 21 Feb 2012) $
# SVN ID: $Id: Tarsier.pm 855 2012-02-21 17:12:37Z Julio $

################################################################################
# PARSE LOCAL RFAM DATA
################################################################################
# Download relevant Rfam data files.
sub download_rfam {
	my ($data_dir, $dataset, $ftp_url, $build, $unzip) = @_;
	croak "Data directory must be specified" unless $data_dir;
	$dataset = "seed" unless $dataset;
	$ftp_url = "ftp://ftp.sanger.ac.uk/pub/databases/Rfam" unless $ftp_url;
	$build = "CURRENT" unless $build;
	$unzip = 1 unless defined($unzip);
	
	my $rfam_dir = "$data_dir/rfam";
	make_path($rfam_dir) unless -e $rfam_dir;
	
	my @files = qw(	Rfam.$dataset.gz
					database_files/rfam.txt.gz
					database_files/rfam_reg_$dataset.txt.gz
					database_files/rfamseq.txt.gz
					database_files/taxonomy.txt.gz
				);
	foreach my $file (@files) {
		`wget --no-verbose --timestamping --directory-prefix=$rfam_dir $ftp_url/$build/$file`;
		if ($unzip) {
			$file =~ s/^database_files//;
			(my $unzipped = $file) =~ s/\.gz$//;
			`gunzip -c $rfam_dir/$file > $rfam_dir/$unzipped`;
		}
	}
	
	return;
}

# Process the original Rfam data, to make it easier/quicker to process.
sub rarefy_rfam {
	my ($data_dir, $prefix, $species) = @_;
	my $rfamseq = "$data_dir/rfam/rfamseq_min_$prefix.txt";
	unless (-s $rfamseq) {
		my $rfamseq_min = "$data_dir/rfam/rfamseq_min.txt";
		unless (-s $rfamseq_min) {
			_minimise_rfamseq("$data_dir/rfam/rfamseq.txt", $rfamseq_min);
		}
		_cache_rfamseq($rfamseq_min, $rfamseq, $species);
	}
	return $rfamseq;
}

# The full rfamseq file is huge, so we excise the unnecessary columns.
sub _minimise_rfamseq {
	my ($rfamseq_file, $rfamseq_min_file) = @_;
	`cut -f1,2,4 $rfamseq_file > $rfamseq_min_file`;
	return;
}

# The full rfamseq file is still huge, so it is useful to be able to create
# a much smaller version containing data for a list of species.
sub _cache_rfamseq {
	my ($rfamseq_file, $rfamseq_cache_file, $species) = @_;
	unlink($rfamseq_cache_file);
	foreach my $species (@$species) {
		`grep '	$species\$' $rfamseq_file >> $rfamseq_cache_file`;
	}
	# Alphanumerical, rt numerical sort, for later joining.
	`sort -d -k1 $rfamseq_cache_file > $rfamseq_cache_file.sort`;
	`mv $rfamseq_cache_file.sort $rfamseq_cache_file`;
	return;
}

# The file formats changed after release 9.1 of Rfam; these
# are the old versions of the scripts.
sub _minimise_rfamseq_9 {
	my ($rfamseq_file, $rfamseq_min_file) = @_;
	`cut -f1,3,6 $rfamseq_file > $rfamseq_min_file`;
	return;
}

sub _cache_rfamseq_9 {
	my ($rfamseq_file, $rfamseq_cache_file, $species) = @_;
	unlink($rfamseq_cache_file);
	foreach my $species (@$species) {
		`grep "$species" $rfamseq_file >> $rfamseq_cache_file`;
	}
	# Alphanumerical, rt numerical sort, for later joining.
	`sort -d -k1 $rfamseq_cache_file > $rfamseq_cache_file.sort`;
	`mv $rfamseq_cache_file.sort $rfamseq_cache_file`;
	return;
}

sub parse_rfam {
	my ($data_dir, $rfam_id, $prefix, $species, $seed) = @_;
	my $dataset = $seed ? "seed" : "full";
	
	# Retrieve and filter Rfam data; unless we've already done that,
	# in which case we just read in the data from a file.
	my ($structure, $sequences, $accessions);
	if (-s "$data_dir/$rfam_id/$prefix.$dataset.structure.txt") {
		$structure = read_from_file("$data_dir/$rfam_id/$prefix.$dataset.structure.txt");
		my $sequences_text = read_from_file("$data_dir/$rfam_id/$prefix.$dataset.sequences.txt");
		%$sequences = $sequences_text =~ /^>(\S+)[^\n]*\n(\S+)$/gms if $sequences_text;
		my $accessions_text = read_from_file("$data_dir/$rfam_id/$prefix.$dataset.accessions.txt");
		%$accessions = $accessions_text =~ /^([^\t]+)\t([^\t\n]+)$/gms if $accessions_text;
	} else {
		(undef, $structure, $sequences, $accessions) = 
			_rfam_sequences($data_dir, $rfam_id, $prefix, $species, $dataset);
	}
	
	# We don't want accessions as keys and species as values, so we flip
	# them round, making an accession list for each species.
	my %accessions = flip_hash($accessions, 1);
	
	return ($structure, $sequences, \%accessions);
}

# Extract sequence (and structure) data from local Rfam files. The analysis
# can be restricted to a set of species by passing a filtered version of the
# rfamseq file, generated by the rarefy_rfam function.
sub _rfam_sequences {
	my ($data_dir, $rfam_id, $prefix, $species, $dataset) = @_;
	
	unless ($rfam_id =~ /^RF\d{5}$/) {
		die "Invalid Rfam ID '$rfam_id'";
	}
	
	my $rfam_reg_file = "$data_dir/rfam/rfam_reg_$dataset\_min.txt";
	my $rfam_file = "$data_dir/rfam/rfam.txt";
	my $alignment_file = "$data_dir/rfam/Rfam.$dataset";
	
	my ($accessions, $header, $structure, $sequences, $extended_accessions);
	unless (-e $rfam_reg_file) {
		# We do this so that we don't run out of memory...
		(my $rfam_reg_file_orig = $rfam_reg_file) =~ s/_min//;
		_minimise_rfam_reg($rfam_reg_file_orig, $rfam_reg_file);
	}
	
	# Generate a list of EMBL/GenBank accession numbers, to use for later filtering.
	my $rfamseq_file = rarefy_rfam($data_dir, $prefix, $species);
	$accessions = _parse_rfamseq($rfam_file, $rfam_reg_file, $rfamseq_file, $rfam_id);
	
	# The first two returned variables are strings, the other two are hashes indexed
	# by the unique combination of accession number and position (hence 'extended').
	($header, $structure, $sequences, $extended_accessions) =
		stockholm_sequences($alignment_file, $rfam_id, $accessions);
	
	my ($sequences_text, $accessions_text) = ("", "");
	foreach my $seq_id (keys %$sequences) {
		$$sequences{$seq_id} =~ s/\./\-/gms;
		$sequences_text .= ">$seq_id  $rfam_id\n".$$sequences{$seq_id}."\n";
	}
	
	foreach my $acc (sort keys %$extended_accessions) {
		$accessions_text .= "$acc\t".$$extended_accessions{$acc}."\n";
	}
	
	save_to_file($header, "$data_dir/$rfam_id/$prefix.$dataset.header.txt");
	save_to_file($structure, "$data_dir/$rfam_id/$prefix.$dataset.structure.txt");
	save_to_file($sequences_text, "$data_dir/$rfam_id/$prefix.$dataset.sequences.txt");
	save_to_file($accessions_text, "$data_dir/$rfam_id/$prefix.$dataset.accessions.txt");
	
	return ($header, $structure, $sequences, $extended_accessions);
}

sub _minimise_rfam_reg {
	my ($rfam_reg_file, $rfam_reg_min_file) = @_;
	`cut -f1,2 $rfam_reg_file > $rfam_reg_min_file`;
	return;
}

# Find the EMBL/GenBank accession numbers that relate to an Rfam ID.
sub _parse_rfamseq {
	my ($rfam_file, $rfam_reg_file, $rfamseq_file, $rfam_id) = @_;
	
	# Find the family autonumber ID that relates to the given Rfam ID.
	my $rfam = read_from_file($rfam_file);
	my ($auto_rfam) = $rfam =~ /^(\d+)\t\d+\t$rfam_id/gms;
	
	# Find the species autonumber IDs that relate to the family autonumber ID.
	my $rfam_reg = read_from_file($rfam_reg_file);
	my @auto_rfamseq = $rfam_reg =~ /^$auto_rfam\t(\d+)/gms;
	
	# Write the species autonumber IDs to a file, then join to the rfamseq file.
	# The files need to be sorted on the relevant ID for 'join' to work; in theory
	# the sort should be numerical, but alphanumerical sort seems to be required.
	#save_to_file(join("\n", sort {$a <=> $b} @auto_rfamseq), "$rfam_reg_file.tmp");
	save_to_file(join("\n", sort @auto_rfamseq), "$rfam_reg_file\_$rfam_id.tmp");
	
	my $rfamseq = `join -1 1 -2 1 -t '	' -o 2.2,2.3 $rfam_reg_file\_$rfam_id.tmp $rfamseq_file`;
	my %accessions = $rfamseq =~ /^([^\t]+)\t([^\t\n]+)/gms;
	
	unlink("$rfam_reg_file\_$rfam_id.tmp");
	return wantarray ? %accessions : \%accessions;
}

# Extract Rfam data from a Stockholm-formatted file, for a given ID and
# an optional list of accession numbers.
sub stockholm_sequences {
	my ($alignment_file, $rfam_id, $accessions) = @_;
	my (%sequences, %extended_accessions);
	my $rfam_id_file = "$alignment_file\_$rfam_id";
	
	# The full file is way too big to use without a bit of fiddling. We
	# work out the lines in the file which correspond to each ID, then
	# extract the relevant chunk for the one given as a parameter.
	unless (-e $rfam_id_file) {
		my $rfam_index_file = "$alignment_file.index";
		unless (-e $rfam_index_file) {
			`grep -n '=GF AC' $alignment_file > $rfam_index_file`;
		}
		my $rfam_index = read_from_file($rfam_index_file);
		my %rfam_index = $rfam_index =~ /^(\d+).+(RF\d+)/gm;
		%rfam_index = flip_hash(\%rfam_index);
		my @positions = sort {$a <=> $b} values(%rfam_index);
		# Need the next line to cover the case of the last ID in the file.
		push @positions, `wc -l $alignment_file`;
		my $index_position;
		for (my $i=0; $i<scalar(@positions); $i++) {
			if ($rfam_index{$rfam_id} == $positions[$i]) {
				$index_position = $i;
				last;
			}
		}
		# Subtract 3 at the end because the 'GF AC' line is the 3rd, rather
		# than the 1st, line in each chunk.
		my $lines = $positions[$index_position+1] - $positions[$index_position] - 3;
		`grep -A $lines '^\#=GF AC   $rfam_id' $alignment_file > $rfam_id_file`;
	}
	
	my @header = `grep '^\#=GF ' $rfam_id_file`;
	my $header = join("", @header);
	
	my @structure = `grep '^\#=GC SS_cons' $rfam_id_file`;
	my $structure = join("\n", @structure);
	$structure =~ s/#=GC\s+SS_cons//gms;
	$structure =~ s/\s+//gms;
	
	foreach my $id (keys %$accessions) {
		my @sequences = `grep '^$id' $rfam_id_file`;
		
		foreach my $seq (@sequences) {
			$seq =~ /^([^\.]+)(\S+)\s+(\S+)$/;
			$sequences{"$1$2"} .= $3;
			$extended_accessions{"$1$2"} = $$accessions{$1};
	 	}
	}
	
	return ($header, $structure, \%sequences, \%extended_accessions);
}

# This is not needed anymore, but is left, just in case it,
# or something like it, is required in the future.
sub find_nearest {
	my ($seed_accession, $full_accessions) = @_;
	# Sometimes the full alignment has a slightly different position than
	# the seed alignment; so we need to look for the nearest match.
	
	my ($embl_id, $start, $stop) = $seed_accession =~ /^([^\.]+)\.\d+\/(\d+)\-(\d+)$/;
	my ($nearest, $diff);
	
	foreach my $possible (@$full_accessions) {
		my ($new_start, $new_stop) = $possible =~ /^[^\.]+\.\d+\/(\d+)\-(\d+)$/;
		if ($nearest) {
			my $new_diff = abs($start-$new_start) + abs($stop-$new_stop);
			if ($new_diff < $diff) {
				$nearest = $possible;
				$diff = $new_diff;
			}
		} else {
			$nearest = $possible;
			$diff = abs($start-$new_start) + abs($stop-$new_stop);
		}
	}
	
	return $nearest;
}
################################################################################

################################################################################
# RNA GENE DETECTION
################################################################################
# Execute RNAz as simply as possible, . The alignment file must be in maf/ama
# format
sub rnaz {
	my ($maf_file, $output_dir) = @_;
	my ($basename) = $maf_file =~ /([\w\.]+)\.maf$/;
	my $windowed_file = "$output_dir/$basename.maf";
	my $results_file = "$output_dir/$basename.res";
	my $out_file = "$output_dir/$basename.out";
	my $script_dir = "$monkeyshines_dir/resources/rnaz";
	
	my $window_cmd = "$script_dir/rnazWindow.pl $maf_file";
	my $window = execute_check($window_cmd, "ERROR", 0, "RNAz - Window", "fatal");
	save_to_file($window, $windowed_file);
	
	if ($window) {
		my $rnaz_cmd = "$bin_dir"."rnaz --both-strands --show-gaps $windowed_file";
		my $results = execute_check($rnaz_cmd, "ERROR", 0, "RNAz", "fatal");
		save_to_file($results, $results_file);
		
		my $cluster_cmd = "$script_dir/rnazCluster.pl --cutoff 0.9 $results_file";
		my $out = execute_check($cluster_cmd, "ERROR", 0, "RNAz - Cluster", "fatal");
		save_to_file($out, $out_file);
		
		my $rna_gene = -s $out_file ? 1 : 0;
		return $rna_gene;
	} else {
		carp "Data in '$maf_file' is of insufficient quality for RNAz.";
		save_to_file("", $results_file);
		save_to_file("", $out_file);
		
		return;
	}
}

# The 'rnaz2' function is very similar to the 'rnaz' one, but it's
# clearer to have two distinct functions.
sub rnaz2 {
	my ($maf_file, $output_dir, $max_seqs) = @_;
	my ($basename) = $maf_file =~ /([\w\.]+)\.maf$/;
	my $windowed_file = "$output_dir/$basename.maf";
	my $results_file = "$output_dir/$basename.res";
	my $out_file = "$output_dir/$basename.out";
	my $script_dir = "$monkeyshines_dir/resources/rnaz2";
	
	my $window_cmd;
	if ($max_seqs) {
		$window_cmd = "$script_dir/rnazWindow.pl --max-seqs $max_seqs $maf_file";
	} else {
		$window_cmd = "$script_dir/rnazWindow.pl $maf_file";
	}
	my $window = execute_check($window_cmd, "ERROR", 0, "RNAz - Window", "fatal");
	save_to_file($window, $windowed_file);
	
	if ($window) {
		my $rnaz_cmd = "$bin_dir"."rnaz2 --both-strands --dinucleotide $windowed_file";
		my $results = execute_check($rnaz_cmd, "ERROR", 0, "RNAz v2", "fatal");
		save_to_file($results, $results_file);
		
		my $cluster_cmd = "$script_dir/rnazCluster.pl --cutoff 0.9 $results_file";
		my $out = execute_check($cluster_cmd, "ERROR", 0, "RNAz v2 - Cluster", "fatal");
		save_to_file($out, $out_file);
		
		my $rna_gene = -s $out_file ? 1 : 0;
		return $rna_gene;
	} else {
		carp "Data in '$maf_file' is of insufficient quality for RNAz v2\n";
		save_to_file("", $results_file);
		save_to_file("", $out_file);
		
		return;
	}
}

# RNAz reports the position of the detected RNA genes relative to the ungapped
# reference sequence; we need to convert these to be relative to the original
# alignment. We return a string of 0s and 1s, indicating where the RNA is.
sub rnaz_inferred {
	my ($out, $ref_seq) = @_;
	
	my $inferred = "";
	if ($out) {
		my @loci = $out =~ /^(locus[^\n]+)/gms;
		my @ref_seq = split(//, $ref_seq);
		my $position = 0;
		my $nucleotides_seen = 0;
		
		foreach my $locus (@loci) {
			my @locus = split(/\t/, $locus);
			my ($start, $stop) = ($locus[2], $locus[3]);
			
			# RNAz reports zero-based co-ordinates, which is why we
			# have a strictly-less-than operator here.
			while ($nucleotides_seen < $start) {
				$inferred .= "0";
				unless ($ref_seq[$position] eq "-") {
					$nucleotides_seen++;
				}
				$position++;
			}
			
			while ($nucleotides_seen < $stop) {
				$inferred .= "1";
				unless ($ref_seq[$position] eq "-") {
					$nucleotides_seen++;
				}
				$position++;
			}
		}
		
		$inferred .= "0"x(length($ref_seq)-length($inferred));
	} else {
		$inferred = "0"x(length($ref_seq));
	}
	return $inferred;
}
################################################################################

################################################################################
# Execute CMfinder and parcel up the output nicely
sub cmfinder {
	my ($seq_file, $out_dir) = @_;
	my ($basename) = $seq_file =~ /([\w\.]+)\.seq$/;
	my $out_file = "$out_dir/cmfinder_$basename.out";
	my $script_dir = "$monkeyshines_dir/resources/cmfinder";
	
	# Need to delete any previous analyses of the given sequence, as they
	# might interfere with the merging and/or summarising in this analysis.
	`rm $out_dir/$basename.seq.cm* &> /dev/null`;
	`rm $out_dir/$basename.seq.motif* &> /dev/null`;
	
	# Count the species in the sequence file, for use in subsequent calculations.
	my $seqs = read_from_file($seq_file);
	my $species_count = () = $seqs =~ /(>)/gm;
	
	# The reference sequence is always first.
	my ($ref_species) = $seqs =~ /^>(\S+)/m;
	
	# The output of CMfinder is a bunch of motif files, and the default
	# is to put these files alongside the sequence file, but we've hotwired
	# the executable to place them elsewhere... This causes a problem, because
	# the CMfinder code is hard-wired to use a specific string length, which
	# we potentially exceed with descriptive, multiply-nested output paths.
	# So we use temporary symbolic links.
	my $seq_file_link = "$basename.seq";
	my $out_dir_link = "tmp_out_dir";
	`ln -s $seq_file $seq_file_link`;
	`ln -s $out_dir $out_dir_link`;
	
	my $simple_cmd = "$script_dir/cmfinder.pl -b -o $out_dir_link -n 5 $seq_file_link";
	execute_check($simple_cmd, "FATAL", 0, "CMfinder - Simple", "fatal");
	
	my $complex_cmd = "$script_dir/cmfinder.pl -b -o $out_dir_link -s 2 -n 5 $seq_file_link";
	execute_check($complex_cmd, "FATAL", 0, "CMfinder - Complex", "fatal");
	
	my $combine_cmd = "$script_dir/CombMotif.pl $seq_file_link $out_dir_link/$basename.seq.motif";
	# Only warn if we run into combining motif errors (generally, finding
	# one that's too long, ie > 500 - it rarely happens).
	execute_check($combine_cmd, "FATAL", 0, "CMfinder - Combine", 0);
	
	# The latest.cm file is left intentionally, so that you can pause
	# execution of CMfinder, then pick up where you left off. But it
	# makes the place untidy, so we delete it.
	unlink("latest.cm") if -e "latest.cm";
	
	# Having created the motifs, collate stats for each. If at least one
	# passes the score threshold, then we report that we've got a gene.
	my $out = "";
	my $rna_gene = 0;
	opendir(OUT_DIR, $out_dir);
	foreach my $motif_file (grep { /$basename.seq.motif/ } readdir(OUT_DIR)) {
		my $summarize_cmd = "$bin_dir"."summarize $out_dir_link/$motif_file";
		my $summary = execute_check($summarize_cmd, "FATAL", 0, "CMfinder - Summarize", "fatal");
		my %summary = $summary =~ /(\S+)=(\S+)/gms;
		next if $summary{'Num'} < 2;
		$summary{'File'} = "$out_dir/$motif_file";
		
		# Calculate the motif's rank by combining the information from the
		# various fields. The formula might look a bit contrived, but it's
		# what the CMfinder authors use in the Yao et al (2007) paper...
		#$summary{'Rank'} = sqrt(($summary{'Conserved_pos'} + 0.2) * $summary{'BP'}/$summary{'Seq_id'})*$species_count*(1+ log($summary{'Num'}/$species_count));
		
		# The score in the Torarinsson et al (2008) paper is probably better:
		$summary{'Rank'} = $summary{'Num'} * sqrt($summary{'Conserved_pos'}/$summary{'Seq_id'}) * ($summary{'BP'}/$summary{'Len'});
		# They use a cut-off of Rank > 5, Energy < -5, determined by the number
		# of FPs these values gave; so no reason to use these cut-offs elsewhere,
		# but in lieu of any other advice, might as well use these...
		$rna_gene = 1 if $summary{'Rank'} > 5 && $summary{'Energy'} < -5;
		
		# Extract the position information for human from the motif file...
		my $motif = read_from_file("$out_dir/$motif_file");
		($summary{'Start'}, $summary{'Stop'}) = $motif =~ /^#=GS\s+$ref_species\s+DE\s+(\d+)\s*\.\.\s*(\d+)/ms;
		
		# ... and store the species in this alignment,
		# since that can vary from motif to motif.
		my @species = $motif =~ /^#=GS\s+(\S+)\s+DE/gms;
		$summary{'Species'} = join(", ", @species);
		
		unless ($out) {
			$out = join("\t", sort(keys(%summary)));
		}
		$out .= "\n".join("\t", @summary{sort(keys(%summary))});
	}
	closedir(OUT_DIR);
	save_to_file($out, $out_file);
	
	unlink($seq_file_link);
	unlink($out_dir_link);
	
	return $rna_gene;
}

# Choose the motif with the highest score, selecting randomly in the
# unlikely event that we have two identical scores. Since CMfinder
# doesn't use the original alignment we need to convert the co-ordinates
# it reports (relative to the ungapped reference sequence) to ones that
# are relative to that alignment.
sub cmfinder_inferred {
	my ($out, $ref_seq) = @_;
	
	my $inferred = "";
	if ($out) {
		my @lines = split(/\n/, $out);
		
		my $header = shift(@lines);
		my @cols = split(/\t/, $header);
		my %cols;
		$cols{$cols[$_]} = $_ for (0..(scalar(@cols)-1));
		
		my (@best, $best_rank);
		foreach my $line (@lines) {
			my @line = split(/\t/, $line);
			next if $line[$cols{'Num'}] < 2;
			next if $line[$cols{'Energy'}] > -5;
			next if $line[$cols{'Rank'}] < 5;
			my $current_rank = $line[$cols{'Rank'}];
			if ($best_rank) {
				if ($best_rank < $current_rank) {
					undef @best;
					push @best, \@line;
					$best_rank = $current_rank;
				} elsif ($best_rank == $current_rank) {
					push @best, \@line;
				}
			} else {
				push @best, \@line;
				$best_rank = $current_rank;
			}
		}
		
		my $selected = random_select(\@best);
		
		my @ref_seq = split(//, $ref_seq);
		
		if ($selected) {
			my $position = 0;
			my $nucleotides_seen = 0;
			my ($start, $stop) = ($$selected[$cols{'Start'}], $$selected[$cols{'Stop'}]);
			
			while ($nucleotides_seen <= $start) {
				$inferred .= "0";
				unless ($ref_seq[$position] eq "-") {
					$nucleotides_seen++;
				}
				$position++;
			}
			while ($nucleotides_seen <= $stop) {
				$inferred .= "1";
				unless ($ref_seq[$position] eq "-") {
					$nucleotides_seen++;
				}
				$position++;
			}
		}
		
		$inferred .= "0"x(length($ref_seq)-length($inferred));
	} else {
		$inferred = "0"x(length($ref_seq));
	}
	
	return $inferred;
}

# This function isn't used, but is here for future reference. I don't
# think it's valid to merge the start and end points of multiple,
# possibly overlapping, motifs, and take those locations as regions
# of RNA genes. That's already been done to a certain extent, by the
# CombMotif.pl script, and you'd end up with a complicated dependent
# mess. But you do miss out on CMfinder predicting disjoint sections
# of RNA within an alignment, and I was curious to see what effect this
# had on boundary detection. This function was designed to answer that
# question - there is an increase in the prediction of RNA genes
# approximately evenly across all positions, which isn't at all helpful
# for detecting boundaries.
sub cmfinder_inferred_all {
	my ($out, $ref_seq) = @_;
	
	my $inferred = "";
	if ($out) {
		my @lines = split(/\n/, $out);
		
		my $header = shift(@lines);
		my @cols = split(/\t/, $header);
		my %cols;
		$cols{$cols[$_]} = $_ for (0..(scalar(@cols)-1));
		
		# Gather all of the coords that are beyond a threshold score.
		# There will be lots of overlapping, but we parcel that off
		# to another function to work out.
		my (@loci);
		foreach my $line (@lines) {
			my @line = split(/\t/, $line);
			next if $line[$cols{'Num'}] < 2;
			next if $line[$cols{'Energy'}] > -5;
			next if $line[$cols{'Rank'}] < 5;
			
			push @loci, [$line[$cols{'Start'}], $line[$cols{'Stop'}]];
		}
		
		# This function is recursive, and works on the array by reference.
		coord_union_all(\@loci);
		my @ref_seq = split(//, $ref_seq);
		my $position = 0;
		my $nucleotides_seen = 0;
		
		foreach my $locus (sort {$$a[0] <=> $$b[0]} @loci) {
			my ($start, $stop) = ($$locus[0], $$locus[1]);
			
			while ($nucleotides_seen <= $start) {
				$inferred .= "0";
				unless ($ref_seq[$position] eq "-") {
					$nucleotides_seen++;
				}
				$position++;
			}
			while ($nucleotides_seen <= $stop) {
				$inferred .= "1";
				unless ($ref_seq[$position] eq "-") {
					$nucleotides_seen++;
				}
				$position++;
			}
		}
		
		$inferred .= "0"x(length($ref_seq)-length($inferred));
	} else {
		$inferred = "0"x(length($ref_seq));
	}
	return $inferred;
}
################################################################################

################################################################################
# Execute EvoFold
sub evofold {
	my ($maf_file, $tree_file, $output_dir) = @_;
	my ($basename) = $maf_file =~ /([\w\.]+)\.maf$/;
	my $config_dir = "$monkeyshines_dir/resources/evofold";
	
	# The out file contains only the subsequences where structure is
	# detected; the results file has all the results, for the full length.
	my $results_file = "$output_dir/$basename.res";
	my $out_file = "$output_dir/$basename.out";
	
	# Evofold assumes that the config directory is always given relative
	# to the current working directory; so even if an absolute path is given,
	# a '.' is	prepended, which results in an EvoFold error. So we move to
	# the root directory before executing, and it all works out.
	my $initial_dir = `pwd`;
	$initial_dir =~ s/\n//gm;
	chdir("/");
	
	my $evofold = "$bin_dir"."evofold -c $config_dir -o $out_file -f $results_file $maf_file $tree_file";
	execute_check($evofold, "Error", 0, "EvoFold", "fatal");
	
	chdir($initial_dir);
	
	# There will always be a header row in the output, so check if there's
	# data by looking for a lines after the header...
	my $rna_gene = 0;
	my ($data, $cols) = read_from_delim($out_file, "\t", 1);
	if (scalar(@$data) > 0) {
		$rna_gene = 1;
	}
	return $rna_gene;
}

# EvoFold reports the position of the RNA relative to the gapped alignment,
# so it's less work than the other methods to get a string representing the
# inferred structure.
sub evofold_inferred {
	my ($out, $ref_seq) = @_;
	
	my $inferred = "";
	if ($out) {
		my @loci = $out =~ /^(Alignment[^\n]+)/gms;
		my $position = 0;
		
		if (scalar(@loci)) {
			foreach my $locus (@loci) {
				my @locus = split(/\t/, $locus);
				my ($start, $stop) = ($locus[1], $locus[2]);
				
				while ($position <= $start) {
					$inferred .= "0";
					$position++;
				}
				while ($position <= $stop) {
					$inferred .= "1";
					$position++;
				}
			}
			
			$inferred .= "0"x(length($ref_seq)-length($inferred));
		} else {
			$inferred = "0"x(length($ref_seq));
		}
	} else {
		$inferred = "0"x(length($ref_seq));
	}
	return $inferred;
}
################################################################################

################################################################################
# Compare two strings representing RNA structure, with 0s and 1s
# as flanking and RNA, respectively.
sub inferred_accuracy {
	my ($pattern, $inferred) = @_;
	
	my @pattern = split(//, $pattern);
	my @inferred = split(//, $inferred);
	my %results;
	
	my ($tp, $tn, $fp, $fn) = (0, 0, 0, 0);
	for (my $i=0; $i<length($pattern); $i++) {
		$tp++ if ($pattern[$i] + $inferred[$i]) == 2;
		$tn++ if ($pattern[$i] + $inferred[$i]) == 0;
		$fp++ if $pattern[$i] < $inferred[$i];
		$fn++ if $pattern[$i] > $inferred[$i];
	}
	
	$results{'TP'} = $tp;
	$results{'TN'} = $tn;
	$results{'FP'} = $fp;
	$results{'FN'} = $fn;
	$results{'Sensitivity'} = $tp+$fn > 0 ? $tp/($tp+$fn) : 0;
	$results{'Specificity'} = $tn+$fp > 0 ? $tn/($tn+$fp) : 0;
	if (($tp+$fp)*($tp+$fn)*($tn+$fp)*($tn+$fn) > 0) {
		$results{'MCC'} = (($tp*$tn)-($fp*$fn))/(sqrt(($tp+$fp)*($tp+$fn)*($tn+$fp)*($tn+$fn)));
	} else {
		$results{'MCC'} = ($tp*$tn)-($fp*$fn);
	}
	
	return wantarray ? %results : \%results;
}
################################################################################

################################################################################
# PROCESS SPECIES DATA
################################################################################
# Parse a file containing species information
sub parse_species {
	my ($species_file, $format) = @_;
	$format = "" unless $format;
	# Species file has five columns, Tax ID, Linnaean name,
	# Common name, Rfam name, UCSC name. We allow common and
	# Rfam names to be blank.
	my $species = read_from_file($species_file);
	
	if (lc($format) eq "tax_id") {
		my @tax_id = $species =~ /^([^\t]+)/gms;
		return wantarray ? @tax_id : \@tax_id;
	} elsif (lc($format) eq "species") {
		my @species = $species =~ /^[^\t]+\t([^\t]+)/gms;
		return wantarray ? @species : \@species;
	} elsif (lc($format) eq "common") {
		my @common = $species =~ /^[^\t]+\t[^\t]+\t([^\t]*)/gms;
		return wantarray ? @common : \@common;
	} elsif (lc($format) eq "rfam") {
		my @rfam = $species =~ /^[^\t]+\t[^\t]+\t[^\t]*\t([^\t]*)/gms;
		return wantarray ? @rfam : \@rfam;
	} elsif (lc($format) eq "ucsc") {
		my @ucsc = $species =~ /^[^\t]+\t[^\t]+\t[^\t]*\t[^\t]*\t([^\t\n]+)$/gms;
		return wantarray ? @ucsc : \@ucsc;
	} else {
		# UCSC names are the most practical; they're short, mnemonic
		# (if sometimes inconsistent), and don't contain spaces. So we want
		# a hash structure that allows us to easily look up the UCSC, given
		# any other form of name.
		my @tax_id = $species =~ /^([^\t]+)/gms;
		my @species = $species =~ /^[^\t]+\t([^\t]+)/gms;
		my @common = $species =~ /^[^\t]+\t[^\t]+\t([^\t]*)/gms;
		my @rfam = $species =~ /^[^\t]+\t[^\t]+\t[^\t]*\t([^\t]*)/gms;
		my @ucsc = $species =~ /^[^\t]+\t[^\t]+\t[^\t]*\t[^\t]*\t([^\t\n]+)$/gms;
		my %species;
		for (my $i=0; $i<scalar(@ucsc); $i++) {
			$species{$tax_id[$i]} = $ucsc[$i];
			$species{$species[$i]} = $ucsc[$i];
			$species{$common[$i]} = $ucsc[$i];
			$species{$rfam[$i]} = $ucsc[$i];
		}
		delete $species{''} if exists $species{''};
		return wantarray ? %species : \%species;
	}
}
################################################################################

1;

