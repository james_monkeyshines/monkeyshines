package MonkeyShines::Baboon;

use warnings;
use strict;
use Carp;

our $VERSION = '0.1';

use GD;
use POSIX qw (floor);

use MonkeyShines::Utils qw(save_to_file);

use Exporter;
our @ISA = qw(Exporter);
our @EXPORT_OK = qw(
	aa_groups
	colours
	colors
	colour_alignment
	color_alignment
	barcode
	legend
	legend_groups
	annotate_positions
	image_dimensions
	image_resize
	image_tile);

=pod

=head1 Name

MonkeyShines::Baboon

=head1 Description

MonkeyShines::Baboon generates an image from an alignment,
consisting of vertical stripes (either in colour or greyscale).

The general idea is that is difficult to get a sense for the overall
pattern of a relatively long alignments; producing a condensed image
can be an effective way of assessing gap structure, or general patterns
of base composition. This idea is not really new, but there's currently
no software that enables customisable colours, and that can work within
Perl scripts or on the command line (as opposed to a webserver, ie
'Fingerprint': http://evol.mcmaster.ca/fingerprint/index.php).
The TeXshade software is a very flexible alternative, but would require
a number of modifications to produce 'barcodes', and struggles with
memory requirements for long alignments - as clever as TeXshade is, here
we're doing something much simpler, and it's easier to just implement
the functionality in Perl. The GD library does all the graphics.

=head1 Functions

=head2 Alignment Barcodes

=over

=item B<[$|%]groups = aa_groups($group)>

Given a group name, return a hash of the category names, indexed by
the amino acids in that group. Possible groups are 'size', 'charge',
'hydrophobicity', 'chemical'.

=item B<[$|%]colours = colours($scheme)>
=item B<[$|%]colours = colors($scheme)>

Determine how to colour characters in the alignment for the given scheme.
The colours are specially selected so that if the colours are blended
within a column, alignment properties are visually maintained.
$scheme can be one of:
'nuc[leotide]': bases are coloured, gaps are white, unknown bases are black
	A: light blue, C: gold, G: red, T: dark blue, R: violet, Y: green
'nuc[leotide]_grey_acgt': greyscale, highlights CG and AT content, at the expense of RY
	A: dark grey, C: lightest grey, G: light grey, T: darkest grey, R: medium grey, Y: medium grey
'nuc[leotide]_grey_ry': greyscale, highlights RY, at the expense of CG and AT content
	A: lightest grey, C: dark grey, G: light grey, T: darkest grey, R: lighter grey, Y: darker grey
'aa': amino acids are coloured according to the Taylor scheme, gaps are white, unknown bases are black.
'aa_charge': HKR = 'Basic', ACFGILMNPQSTVWY = 'Neutral', DE = 'Acidic'
'aa_chem[ical]': HKR = 'Basic', DE = 'Acidic', AGILPV = 'Aliphatic', NQ = 'Amide', FWY = 'Aromatic', ST = 'Hydroxyl', CM = 'Sulphur'
'aa_hydro[phobicity]': ACFILMVWY = 'Hydrophobic', GHQST = 'Neutral', DEKNPR = 'Hydrophilic'
'aa_size': ACDGNPSTV = 'Small', EFHIKLMQRWY = 'Large'
'b_and_w' (default): characters are black, gaps are white

=item B<undef = colour_alignment($seqs, $order, $img_file[, $scheme][, $column_width][, $row_height][, $neighbours])>
=item B<undef = color_alignment($seqs, $order, $img_file[, $scheme][, $column_width][, $row_height][, $neighbours])>

Create a representation of the alignment defined by the sequences in the
hash $seqs, preserving the order in $order - these are the outputs from
any of the 'from_<type>' functions in the 'Sequence' module. Also required 
is the name of the image file, which will be saved in .png format. $scheme
is the name of a colour scheme recognised by the 'colours' function; the
default is black and white. $column_width and $row_height are self-evident;
defaults are 1 and 10, respectively. If $neighbours is given, then the colour
of each character will be a blend of that character and the relevant number
of characters on either side.

=item B<undef = barcode($img_file[, $barcode_file])>
Blur colours within columns only, to create a consistent stripes of colour.
If $barcode_file is provided then a separate file will be created, otherwise
the barcode image is saved as $img_file.

=item B<undef = legend($legend_file[, $scheme][, $padding][, $chars])>

Create a legend image to accompany a barcode, saved as $legend_file.
$scheme is the name of a colour scheme recognised by the 'scheme_colours'
function; the default is black and white. $padding (default 5) adds a
border around the image. $chars is an arrayref of characters to include
in the legend; by default these are derived from $scheme.

=item B<undef = legend_groups($legend_file[, $scheme][, $padding])>

Similar to the 'legend' function, but show the relevant group names,
rather than individual amino acids.

=item B<undef = annotate_positions($img_file[, $ann_file], $positions[, $padding][, $line_thickness][, $line_height][, $line_colour][, $background])>

For a pre-existing image $img_file, add a blocky bracket at the co-ordinates
in $positions, which is a string of 1-based start and end points, e.g.
"1,10 45,105", where commas separate the two ends of the block, and spaces
separate block definitions. $padding (default 5 pixels) defines how much
padding appears above and below the blocky bracket. $line_thickness and
$line_height control the appearance of the block, and default to 1 and 10
pixels, respectively. If $ann_file is given, the edited image will be saved
there, otherwise it will overwrite the original file. To define the colour
of the line and background provide arrayrefs $line_colour and/or $background,
of the R, G, B values (defaults are black lines on a white background).

Note that by repeated calls to this function, overlapping brackets can be
drawn such that they are vertically offset rather than overwriting one another.
Also, to add whitespace at the bottom of an image, call this function with an
empty string for $positions.

=item B<($width, $height) = image_dimensions($img_file)>

Return the width and height of an image, in pixels.

=item B<undef = image_resize($img_file[, $resize_file][, $width][, $height])>

Resize the image $img_file to be $width x $height pixels. If one dimension
is not given then it remains the same (i.e. the resize does not preserve the
aspect ratio). If $resize_file is given, the new image is saved there,
otherwise $img_file is overwritten.

=item B<$status = image_tile($img_files, $tile_file[, $padding][, $justification][, $stack][, $background])>

Merge the images whose filenames are given in the arrayref $img_files, saving
the resultant file as $tile_file. $padding is the size of the border, in
pixels, that is added to each image (default 5). $justification defines how
the images are arranged, either 'left', 'right', or 'centre' (the default).
The images will be joined left-to-right, by default; set $stack to true to tile
the images from top to bottom. To define the colour of the background, define
an arrayref of the R, G, B values as $background (the default is white).

=back

=head1 Author

James Allen
(james@monkeyshines.co.uk)

=head1 Copyright

Copyright 2011-2012 James Allen; University of Manchester

=head1 Licence

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program. If not, see <http://www.gnu.org/licenses/>.

=cut

################################################################################
# ALIGNMENT BARCODES
################################################################################
sub aa_groups {
	my ($group) = @_;
	my %groups;
	if ($group =~ /size/i) {
		%groups = ('ACDGNPSTV' => 'Small', 'EFHIKLMQRWY' => 'Large');
	} elsif ($group =~ /hydro/i) {
		%groups = ('ACFILMVWY' => 'Hydrophobic', 'GHQST' => 'Neutral', 'DEKNPR' => 'Hydrophilic');
	} elsif ($group =~ /charge/i) {
		%groups = ('HKR' => 'Basic', 'ACFGILMNPQSTVWY' => 'Neutral', 'DE' => 'Acidic');
	} elsif ($group =~ /chem/i) {
		%groups = ('HKR' => 'Basic', 'DE' => 'Acidic', 'AGILPV' => 'Aliphatic', 'NQ' => 'Amide', 'FWY' => 'Aromatic', 'ST' => 'Hydroxyl', 'CM' => 'Sulphur');
	}
	
	return wantarray ? %groups : \%groups;
}

sub aa_colours {
	my ($scheme) = @_;
	my %colours = (
		'A' => {'R' => 204, 'G' => 255, 'B' =>   0},
		'C' => {'R' => 255, 'G' => 255, 'B' =>   0},
		'D' => {'R' => 255, 'G' =>   0, 'B' =>   0},
		'E' => {'R' => 255, 'G' =>   0, 'B' => 102},
		'F' => {'R' =>   0, 'G' => 255, 'B' => 102},
		'G' => {'R' => 255, 'G' => 153, 'B' =>   0},
		'H' => {'R' =>   0, 'G' => 102, 'B' => 255},
		'I' => {'R' => 102, 'G' => 255, 'B' =>   0},
		'K' => {'R' => 102, 'G' =>   0, 'B' => 255},
		'L' => {'R' =>  51, 'G' => 255, 'B' =>   0},
		'M' => {'R' =>   0, 'G' => 255, 'B' =>   0},
		'N' => {'R' => 204, 'G' =>   0, 'B' => 255},
		'P' => {'R' => 255, 'G' => 204, 'B' =>   0},
		'Q' => {'R' => 255, 'G' =>   0, 'B' => 204},
		'R' => {'R' =>   0, 'G' =>   0, 'B' => 255},
		'S' => {'R' => 255, 'G' =>  51, 'B' =>   0},
		'T' => {'R' => 255, 'G' => 102, 'B' =>   0},
		'V' => {'R' => 153, 'G' => 255, 'B' =>   0},
		'W' => {'R' =>   0, 'G' => 204, 'B' => 255},
		'Y' => {'R' =>   0, 'G' => 255, 'B' => 204},
		'-' => {'R' => 255, 'G' => 255, 'B' => 255},
		'.' => {'R' => 255, 'G' => 255, 'B' => 255},
		'default' => {'R' => 0, 'G' => 0, 'B' => 0}
	);
	
	my %groups = aa_groups($scheme);
	foreach my $group (keys %groups) {
		my ($r, $g, $b);
		foreach my $char (split(//, $group)) {
			$r += $colours{$char}{'R'};
			$g += $colours{$char}{'G'};
			$b += $colours{$char}{'B'};
		}
		$r /= length($group);
		$g /= length($group);
		$b /= length($group);
		foreach my $char (split(//, $group)) {
			$colours{$char} = {'R' => $r, 'G' => $g, 'B' => $b};
		}
	}
	
	return wantarray ? %colours : \%colours;
}

sub nuc_groups {
	my ($group) = @_;
	my %groups;
	if ($group =~ /ry/i) {
		%groups = ('AGR' => 'Purine', 'CTY' => 'Pyrimidine');
	} elsif ($group =~ /sw/i) {
		%groups = ('ATW' => 'Weak', 'CGS' => 'Strong');
	}
	
	return wantarray ? %groups : \%groups;
}

sub nuc_colours {
	my ($scheme) = @_;
	my %colours;
	
	if ($scheme =~ /gr[ae]y_acgt/i) {
		%colours = (
			'A' => {'R' => 153, 'G' => 153, 'B' => 153},
			'C' => {'R' => 229, 'G' => 229, 'B' => 229},
			'G' => {'R' => 204, 'G' => 204, 'B' => 204},
			'T' => {'R' => 127, 'G' => 127, 'B' => 127},
			'U' => {'R' => 127, 'G' => 127, 'B' => 127},
			'R' => {'R' => 179, 'G' => 179, 'B' => 179},
			'Y' => {'R' => 179, 'G' => 179, 'B' => 179},
			'N' => {'R' =>   0, 'G' =>   0, 'B' =>   0},
			'-' => {'R' => 255, 'G' => 255, 'B' => 255},
			'.' => {'R' => 255, 'G' => 255, 'B' => 255},
			'default' => {'R' => 0, 'G' => 0, 'B' => 0}
		);
	} elsif ($scheme =~ /gr[ae]y_ry/i) {
		%colours = (
			'A' => {'R' => 229, 'G' => 229, 'B' => 229},
			'C' => {'R' => 153, 'G' => 153, 'B' => 153},
			'G' => {'R' => 204, 'G' => 204, 'B' => 204},
			'T' => {'R' => 127, 'G' => 127, 'B' => 127},
			'U' => {'R' => 127, 'G' => 127, 'B' => 127},
			'R' => {'R' => 217, 'G' => 217, 'B' => 217},
			'Y' => {'R' => 140, 'G' => 140, 'B' => 140},
			'N' => {'R' =>   0, 'G' =>   0, 'B' =>   0},
			'-' => {'R' => 255, 'G' => 255, 'B' => 255},
			'.' => {'R' => 255, 'G' => 255, 'B' => 255},
			'default' => {'R' => 0, 'G' => 0, 'B' => 0}
		);
	} else {
		# %colours = (
			# 'A' => {'R' => 90,  'G' => 150, 'B' => 240},
			# 'C' => {'R' => 255, 'G' => 215, 'B' =>   0},
			# 'G' => {'R' => 210, 'G' => 20,  'B' => 60 },
			# 'T' => {'R' => 35,  'G' => 65,  'B' => 140},
			# 'U' => {'R' => 35,  'G' => 65,  'B' => 140},
			# 'R' => {'R' => 160, 'G' => 85,  'B' => 150},
			# 'Y' => {'R' => 145, 'G' => 140, 'B' => 70 },
			# 'N' => {'R' => 0,   'G' => 0,   'B' =>   0},
			# '-' => {'R' => 255, 'G' => 255, 'B' => 255},
			# '.' => {'R' => 255, 'G' => 255, 'B' => 255},
			# 'default' => {'R' => 0, 'G' => 0, 'B' => 0}
		# );
		%colours = (
			'A' => {'R' =>   0, 'G' =>   0, 'B' => 255},
			'C' => {'R' => 255, 'G' => 255, 'B' =>   0},
			'G' => {'R' => 255, 'G' =>   0, 'B' =>   0},
			'T' => {'R' =>   0, 'G' => 255, 'B' => 255},
			'U' => {'R' =>   0, 'G' => 255, 'B' => 255},
			'R' => {'R' => 127, 'G' =>   0, 'B' => 127},
			'Y' => {'R' => 127, 'G' => 255, 'B' => 127},
			'N' => {'R' => 0,   'G' => 0,   'B' =>   0},
			'-' => {'R' => 255, 'G' => 255, 'B' => 255},
			'.' => {'R' => 255, 'G' => 255, 'B' => 255},
			'default' => {'R' => 0, 'G' => 0, 'B' => 0}
		);
	}
	
	my %groups = nuc_groups($scheme);
	foreach my $group (keys %groups) {
		my ($r, $g, $b);
		foreach my $char (split(//, $group)) {
			$r += $colours{$char}{'R'};
			$g += $colours{$char}{'G'};
			$b += $colours{$char}{'B'};
		}
		$r /= length($group);
		$g /= length($group);
		$b /= length($group);
		foreach my $char (split(//, $group)) {
			$colours{$char} = {'R' => $r, 'G' => $g, 'B' => $b};
		}
	}
	
	return wantarray ? %colours : \%colours;
}

sub colours {
	my ($scheme) = @_;
	my %colours;
	if ($scheme && $scheme =~ /^aa/) {
		%colours = aa_colours($scheme);
	} elsif ($scheme && $scheme =~ /^nuc/) {
		%colours = nuc_colours($scheme);
	} else {
		%colours = (
			'X' => {'R' =>   0, 'G' =>   0, 'B' =>   0},
			'-' => {'R' => 255, 'G' => 255, 'B' => 255},
			'.' => {'R' => 255, 'G' => 255, 'B' => 255},
			'default' => {'R' => 0, 'G' => 0, 'B' => 0}
		);
		
	}
	return wantarray ? %colours : \%colours;
}

sub aa_colors {
	my ($scheme) = @_;
	return aa_colours($scheme);
}

sub nuc_colors {
	my ($scheme) = @_;
	return nuc_colours($scheme);
}

sub colors {
	my ($scheme) = @_;
	return colours($scheme);
}

sub colour_alignment {
	my ($seqs, $order, $img_file, $scheme, $column_width, $row_height, $neighbours) = @_;
	$column_width = 1 unless $column_width;
	$row_height = 10 unless $row_height;
	my %colours = colours($scheme);
	
	my @seqs = values(%$seqs);
	my $width = length($seqs[0]) * $column_width;
	my $height = scalar(@seqs) * $row_height;
	
	GD::Image->trueColor(1);
	my $image = new GD::Image($width, $height);
	my %gd_colours;
	foreach my $char (keys %colours) {
		$gd_colours{$char} = $image->colorAllocate($colours{$char}{'R'}, $colours{$char}{'G'}, $colours{$char}{'B'});
	}
	
	my ($y1, $y2) = (0, $row_height);
	foreach my $id (sort {$$order{$a} <=> $$order{$b}} keys %$order) {
		next unless exists $$seqs{$id};
		my $seq = uc($$seqs{$id});
		my @subseqs;
		if ($neighbours) {
			# The colour of each character is the average of its neighbours
			# and itself, and it's easy to get these chunks with a regex. But
			# at the ends of the sequence we run out of neighbours on one side;
			# in these cases, we average over a region centred on the character,
			# with as many neighbours as we can get (but always equal numbers
			# on either side).
			my $length = (2*$neighbours) + 1;
			for (my $i=1; $i<$length; $i=$i+2) {
				$seq =~ /^(.{$i})/;
				push @subseqs, $1;
			}
			
			my @overlapping = $seq =~ /(?=(.{$length}))/g;
			push @subseqs, @overlapping;
			
			for (my $i=$length-2; $i>0; $i=$i-2) {
				$seq =~ /(.{$i})$/;
				push @subseqs, $1;
			}
			
			# We sort each one, so that we can minimise the calculations
			# that we do when averaging colours, since the average of
			# ACGT is exactly the same as for GCTA, CTAG etc.
			@subseqs = map {join '', sort split(//, $_)} @subseqs;
			foreach my $subseq (@subseqs) {
				if (! exists $gd_colours{uc($subseq)}) {
					my ($r, $g, $b);
					foreach my $char (split(//, $subseq)) {
						$r += $colours{$char}{'R'} || $colours{'default'}{'R'};
						$g += $colours{$char}{'G'} || $colours{'default'}{'G'};
						$b += $colours{$char}{'B'} || $colours{'default'}{'B'};
					}
					$r /= length($subseq);
					$g /= length($subseq);
					$b /= length($subseq);
					$gd_colours{uc($subseq)} = $image->colorAllocate($r, $g, $b);
				}
			}
		} else {
			@subseqs = split(//, $seq);
		}
		
		my ($x1, $x2) = (0, $column_width);
		foreach my $subseq (@subseqs) {
			my $colour;
			if (exists $gd_colours{uc($subseq)}) {
				$colour = $gd_colours{uc($subseq)};
			} else {
				$colour = $gd_colours{'default'};
			}
			$image->filledRectangle($x1, $y1, $x2, $y2, $colour);
			$x1 += $column_width;
			$x2 += $column_width;
		}
		
		$y1 += $row_height;
		$y2 += $row_height;
	}
	
	save_to_file($image->png, $img_file);
	return;
}

sub color_alignment {
	my ($seqs, $order, $img_file, $scheme, $column_width, $row_height, $neighbours) = @_;
	return colour_alignment($seqs, $order, $img_file, $scheme, $column_width, $row_height, $neighbours);
}

sub barcode {
	my ($img_file, $barcode_file) = @_;
	$barcode_file = $img_file unless $barcode_file;
	
	GD::Image->trueColor(1);
	my $image = GD::Image->new($img_file);
	my ($width, $height) = $image->getBounds();
	my $resized = GD::Image->new($width, 1);
   	$resized->copyResampled($image, 0, 0, 0, 0, $width, 1, $width, $height);
	my $barcode = GD::Image->new($width, $height);
	$barcode->copyResampled($resized, 0, 0, 0, 0, $width, $height, $width, 1);
	save_to_file($barcode->png, $barcode_file);
	
	return;
}

sub legend {
	my ($legend_file, $scheme, $padding, $chars) = @_;
	my %colours = colours($scheme);
	unless ($chars) {
		foreach my $char (sort keys(%colours)) {
			if ($char !~ /(default|\.|U)/) {
				push @$chars, $char;
			}
		}
	}
	$padding = 5 unless $padding;
	my $box_size = 15;
	
	# This width is based on font characters being about 10px wide.
	my $width = (scalar(@$chars) * ($box_size + 25)) + ($padding * 2);
	my $height = $box_size + ($padding * 2);
	
	GD::Image->trueColor(1);
	my $image = GD::Image->new($width, $height);
	my $white = $image->colorAllocate(255, 255, 255);
	my $black = $image->colorAllocate(0, 0, 0);
	$image->fill(0, 0, $white);
	
	my ($x1, $y1, $x2, $y2) = ($padding, $padding, $padding+$box_size, $padding+$box_size);
	foreach my $char (@$chars) {
		my $colour = $image->colorAllocate($colours{$char}{'R'}, $colours{$char}{'G'}, $colours{$char}{'B'});
		$image->rectangle($x1, $y1, $x2, $y2, $black);
		$image->fill($x1+1, $y1+1, $colour);
		$image->string(gdLargeFont, $x2+5, $y1, $char, $black);
		$x1 += $box_size + 25;
		$x2 += $box_size + 25;
	}
	
	save_to_file($image->png, $legend_file);
	return;
}

sub legend_groups {
	my ($legend_file, $scheme, $padding) = @_;
	my %colours = colours($scheme);
	$padding = 5 unless $padding;
	my $box_size = 15;
	
	my %groups;
	if ($scheme =~ /^aa/) {
		%groups = aa_groups($scheme);
	} else {
		%groups = nuc_groups($scheme);
	}
	my $width = $padding * 2;
	foreach my $group (keys %groups) {
		my $length = length($group) + length($groups{$group}) + 3;
		$width += $box_size + ($length * 9) + 15;
	}
	my $height = $box_size + ($padding * 2);
	
	GD::Image->trueColor(1);
	my $image = GD::Image->new($width, $height);
	my $white = $image->colorAllocate(255, 255, 255);
	my $black = $image->colorAllocate(0, 0, 0);
	$image->fill(0, 0, $white);
	
	my ($x1, $y1, $x2, $y2) = ($padding, $padding, $padding+$box_size, $padding+$box_size);
	foreach my $group (keys %groups) {
		my $text = $groups{$group}." ($group)";
		my $length = length($text);
		$group =~ /^(\w)/;
		my $colour = $image->colorAllocate($colours{$1}{'R'}, $colours{$1}{'G'}, $colours{$1}{'B'});
		$image->rectangle($x1, $y1, $x2, $y2, $black);
		$image->fill($x1+1, $y1+1, $colour);
		$image->string(gdLargeFont, $x2+5, $y1, $text, $black);
		$x1 += $box_size + ($length * 9) + 15;
		$x2 += $box_size + ($length * 9) + 15;
	}
	
	save_to_file($image->png, $legend_file);
	return;
}

sub annotate_positions {
	my ($img_file, $ann_file, $positions, $padding, $line_thickness, $line_height, $line_colour, $background) = @_;
	$ann_file = $img_file unless $ann_file;
	$padding = 5 unless defined $padding;
	$line_thickness = 1 unless $line_thickness;
	$line_height = 10 unless $line_height;
	$line_thickness = $line_height if $line_thickness > $line_height;
	$line_colour = [0, 0, 0] unless $line_colour;
	$background = [255, 255, 255] unless $background;
	$positions =~ s/\s*,\s*/,/g;
	my @positions = split(/\s/, $positions);
	
	# At the minute, we assume that the positions are non-overlapping.
	# If they weren't, the positions could be split into non-overlapping
	# segments before calling this function, then repeatedly call it to
	# keep adding blocks beneath those that have already been drawn.
	GD::Image->trueColor(1);
	my $image = GD::Image->new($img_file);
	my ($width, $height) = $image->getBounds();
	my $ann_height = $height + $line_height + ($padding * 2);
	my $ann_image = GD::Image->new($width, $ann_height);
	my $fg = $ann_image->colorAllocate(@$line_colour);
	my $bg = $ann_image->colorAllocate(@$background);
	$ann_image->fill(0, 0, $bg);
	$ann_image->copy($image, 0, 0, 0, 0, $width, $height);
	
	my $y1 = $height + $padding;
	my $y3 = $y1 + $line_height - 1;
	my $y2 = $y3 - $line_thickness + 1;
	
	foreach my $position (@positions) {
		# Draw three sides of a box to make a blocky bracket.
		my ($from, $to) = split(/,/, $position);
		my $x1 = $from - 1;
		my $x2 = $x1 + $line_thickness - 1;
		my $x4 = $x1 + $to - $from;
		my $x3 = $x4 - $line_thickness + 1;
		
		$ann_image->filledRectangle($x1, $y1, $x2, $y2, $fg);
		$ann_image->filledRectangle($x1, $y2, $x4, $y3, $fg);
		$ann_image->filledRectangle($x3, $y1, $x4, $y2, $fg);
	}
	
	save_to_file($ann_image->png, $ann_file);
	return;
}

sub image_dimensions {
	my ($img_file) = @_;
	my $image = GD::Image->new($img_file);
	return $image->getBounds();
}

sub image_resize {
	my ($img_file, $resize_file, $width, $height) = @_;
	$resize_file = $img_file unless $resize_file;
	my $image = GD::Image->new($img_file);
	my ($w, $h) = $image->getBounds();
	$width = $w unless $width;
	$height = $h unless $height;
	my $resized = GD::Image->new($width, $height);
    $resized->copyResampled($image, 0, 0, 0, 0, $width, $height, $w, $h);
	save_to_file($resized->png, $resize_file);
	return;
}

sub image_tile {
	my ($img_files, $tile_file, $padding, $justification, $stack, $background) = @_;
	$padding = 5 unless defined $padding;
	$justification = 'centre' unless $justification;
	$stack = 0 unless $stack;
	$background = [255, 255, 255] unless $background;
	
	GD::Image->trueColor(1);
	my %images;
	my ($total_width, $total_height, $max_width, $max_height) = (0, 0, 0, 0);
	my $image_count = 1;
	foreach my $img_file (@$img_files) {
		my $image = GD::Image->new($img_file);
		my ($width, $height) = $image->getBounds();
		$images{$image_count}{'image'} = $image;
		$images{$image_count}{'width'} = $width;
		$images{$image_count}{'height'} = $height;
		$total_width += $width;
		$total_height += $height;
		$max_width = $width if $width > $max_width;
		$max_height = $height if $height > $max_height;
		$image_count++;
	}
	
	my ($tile_width, $tile_height);
	if ($stack) {
		# Images are stacked vertically.
		$tile_width = $max_width + ($padding * 2);
		$tile_height = $total_height + ($padding * $image_count) + $padding;
	} else {
		# Images are tiled horizontally.
		$tile_width = $total_width + ($padding * $image_count) + $padding;
		$tile_height = $max_height + ($padding * 2);
	}
	
	my $tile_image = GD::Image->new($tile_width, $tile_height);
	my $bg = $tile_image->colorAllocate(@$background);
	$tile_image->fill(0, 0, $bg);
	
	my ($x, $y) = (0, 0);
	
	foreach my $image (sort {$a <=> $b} keys %images) {
		my $width = $images{$image}{'width'};
		my $height = $images{$image}{'height'};
		
		if ($stack) {
			if ($justification =~ /left/i) {
				$x = $padding;
				$y += $padding;
			} elsif ($justification =~ /right/i) {
				$x = $padding + ($max_width - $width);
				$y += $padding;
			} else {
				$x = $padding + floor(($max_width - $width)/2);
				$y += $padding;
			}
			$tile_image->copy($images{$image}{'image'}, $x, $y, 0, 0, $width, $height);
			$y += $height;
		} else {
			if ($justification =~ /top/i) {
				$x += $padding;
				$y = $padding;
			} elsif ($justification =~ /bottom/i) {
				$x += $padding;
				$y = $padding + ($max_height - $height);
			} else {
				$x += $padding;
				$y = $padding + floor(($max_height - $height)/2);
			}
			$tile_image->copy($images{$image}{'image'}, $x, $y, 0, 0, $width, $height);
			$x += $width;
		}
	}
	
	save_to_file($tile_image->png, $tile_file);
	return;
}
################################################################################

1;

