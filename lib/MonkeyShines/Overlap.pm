package MonkeyShines::Overlap;

use warnings;
use strict;
use Carp;

our $VERSION = '0.1';

use Math::BigFloat;
use Math::BigInt;
use Math::Trig qw(acos_real asin_real pi);
use MonkeyShines::Utils qw(min max
		uniquify_list simple_intersect simple_union simple_diff
		random_select_and_remove list_exists save_to_file);

use Exporter;
our @ISA = qw(Exporter);
our @EXPORT_OK = qw(
	hypergeo hypergeo_cdf hypergeo_stats
	same_size same_size_cdf same_size_expectation
	circle_radius circle_area triangle_area circular_triangle_area
	circles_area circles_distance circular_triangle_sides
	two_sets two_sets_factor two_sets_prob
	two_lists two_lists_factor two_lists_prob
	same_size_sets same_size_sets_factor same_size_sets_prob
	same_size_lists same_size_lists_factor same_size_lists_prob
	three_sets_validity three_sets_bounds
	three_sets three_sets_factor three_sets_prob
	three_lists three_lists_factor three_lists_prob
	random_overlap random_constrained_overlap
	random_overlaps random_constrained_overlaps
);

=pod

=head1 Name

MonkeyShines::Overlap - Calculate degree and significance of overlapping sets.

=head1 Description

MonkeyShines::Overlap uses a variety of methods to calculate the degree and
statistical significance between overlapping samples. The code for the overlap
of two sets is largely based on stats.cgi, by Jim Lund 2005, available
at http://nemates.org/uky/MA/progs/stats_prog.html.

=head1 Functions

=head2 Distributions

=over

=item B<($p, $N_choose_n) = hypergeo($m1, $m2, $n, $k[, $N_choose_n])>

Calculate the probability $p, using the hypergeometric distribution, of
drawing $n balls from an urn containing $m1 white and $m2 black balls, and
selecting $k white balls. $N_choose_n is there for convenience when
calculating the cdf (see below), and can be ignored.

=item B<($cp) = hypergeo_cdf($m1, $m2, $n, $k)>

Calculate the cumulative hypergeometric probability $cp, where the number
of drawn white balls ranges between zero and $k (inclusive).

=item B<($mean, $variance, $skewness)) = hypergeo_stats($m1, $m2, $n)>

Calculate statistics for the hypergeometric distribution with $m1 white
balls, $m2 black balls, and $n drawn balls.

=item B<($p, $N_choose_m) = same_size($m, $sets, $k, $N[, $N_choose_m][, $p_cache])>

Calculate the probability $p of the overlap $k between $sets sets of size $m,
from a total set of size $N. $N_choose_m is there for convenience when
calculating the cdf (see below), and can be ignored. The function operates
recursively, and $p_cache is used to store results for quick look-up, both
during the recursion and after the call is completed (e.g. in the cdf function).

=item B<($cp) = same_size_cdf($m, $sets, $k, $N[, $p_cache])>

Calculate the cumulative probability $cp of $sets sets of size $m,
where the overlap ranges between zero and $k (inclusive).

=item B<$expected = same_size_expectation($m, $sets, $N[, $p_cache])>

Calculate the expected overlap of $sets sets of size $m, from a
population of $N.

=back

=head2 Geometry of Overlapping Circles

=over

=item B<$r = circle_radius($area)>

Evaluated with a value of pi from the Math::Trig module.

=item B<$area = circle_area($r)>

Evaluated with a value of pi from the Math::Trig module.

=item B<$area = triangle_area($a, $b, $c)>

Calculate the area of a triangle with sides $a, $b and $c, using
Heron's formula, with formulae adjusted to minimise rounding
errors in very acute triangles
(cf http://en.wikipedia.org/wiki/Heron's_formula, accessed 5/5/2011).
If the values of $a, $b and $c do not define a triangle, an error
is not raised; $area is undefined in this case.

=item B<$area = circles_area($r_A, $r_B, $d)>

Calculate the area of the overlap of two circles with radii $r_A and $r_B,
whose centres are $d apart.

=item B<$distance = circles_distance($r_A, $r_B, $overlap[, $precision][, $verbose])>

Calculate the distance between the centres of two circles with radii
$r_A and $r_B, that have an overlapping area of $overlap. There is no
simple formula for calculating this distance, so a simple numerical
optimisation is conducted, using the 'circles_area' function;
the optimisation halts when the difference between successive calls to
that function is less than $precision (default 1e-6). If the circles do not
overlap, $distance is the minimum possible distance, ie the sum of the radii.
If one circle is entirely contained within the other, $distance is undefined,
since it can take any value between zero and |$r_A - $r_B|. If $verbose is
true (default 0), the optimisation steps are printed to standard output.

=item B<($a, $b, $c) = circular_triangle_sides($r_A, $r_B, $r_C, $d_AB, $d_AC, $d_BC)>

Calculate the lengths of the straight sides $a, $b and $c of the circular
triangle defined by the overlap of three circles with radii $r_A, $r_B and $r_C,
and distances between their centres $d_AB, $d_AC and $d_BC. The algorithm is
from Fewell, M.P. (2006) 'Area of common overlap of three circles'.

=item B<$area = circular_triangle_area($a, $b, $c, $r_A, $r_B, $r_C)>

Calculate the area of a circular triangle defined by the overlap of
three circles with radii $r_A, $r_B and $r_C, whose points of intersection
form a triangle with sides $a, $b, $c.

=back

=head2 Two Overlapping Sets

=over

=item B<($expected, $factor) = two_sets_factor($a, $b, $overlap, $N)>

Divide the actual overlap of two sets by the expected overlap, to get an
'overlap factor' that measures how far the overlap deviates from expectation.

=item B<($p) = two_sets_prob($a, $b, $overlap, $N)>

Calculate the probability of two sets, of size $a and $b, drawn from a
total population $N, having overlap $overlap.

=item B<($expected, $factor, $p) = two_sets($a, $b, $overlap, $N)>

A wrapper around the two previous functions.

=back

=head2 Same Size Overlapping Sets

=over

=item B<($expected, $factor) = same_size_sets_factor($a, $sets, $overlap, $N[, $p_cache])>

Get an 'overlap factor' that measures how far the overlap deviates from expectation.

=item B<($p) = same_size_sets_prob($a, $sets, $overlap, $N[, $p_cache])>

Calculate the probability of $sets sets of size $a, drawn from a
total population $N, having overlap $overlap.

=item B<($expected, $factor, $p) = same_size_sets($a, $sets, $overlap, $N)>

A wrapper around the two previous functions.

=back

=head1 Author

James Allen
(james@monkeyshines.co.uk)

=head1 Copyright

Copyright 2011 James Allen; University of Manchester

=head1 Licence

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program. If not, see <http://www.gnu.org/licenses/>.

=cut

################################################################################
# DISTRIBUTIONS
################################################################################
sub hypergeo {
	my ($m1, $m2, $n, $k, $N_choose_n) = @_;
	my $N = $m1 + $m2;
	my $n_minus_k = $n - $k;
	croak("Cannot select $n (n) from a total of $N (N)") if $n > $N;
	croak("Cannot select $k (k) from a total of $m1 (m1)") if $k > $m1;
	croak("Cannot select $k (k) from a total of $n (n)") if $k > $n;
	
	my $m1_choose_k = Math::BigInt->new($m1);
	$m1_choose_k->bnok($k);
	
	my $m2_choose_n_k = Math::BigInt->new($m2);
	$m2_choose_n_k->bnok($n_minus_k);
	
	unless (defined $N_choose_n) {
		$N_choose_n = Math::BigInt->new($N);
		$N_choose_n->bnok($n);
	}
	
	my $p = Math::BigFloat->new($m1_choose_k);
	$p->bmul($m2_choose_n_k);
	$p->bdiv($N_choose_n);
	
	return ($p->bsstr(), $N_choose_n);
}

sub hypergeo_cdf {
	my ($m1, $m2, $n, $k) = @_;
	# Use the symmetry of the distribution to minimise calculations.
	if (($n-$k) > $k) {
		($m1, $m2) = ($m2, $m1);
		$k = $n-$k;
	}
	my $cp = Math::BigFloat->bzero();
	
	# We can save some time by caching 'N choose n' on the
	# first iteration, since it won't change.
	my $N_choose_n;
	my $max = min($m1, $n);
	for (my $x=$k; $x<=$max; $x++) {
		my $p;
		($p, $N_choose_n) = hypergeo($m1, $m2, $n, $x, $N_choose_n);
		$cp->badd($p);
	}
	
	return $cp->bround(18);
}

sub hypergeo_stats {
	my ($m1, $m2, $n) = @_;
	my $N = $m1 + $m2;
	
	my $mean = ($m1*$n)/$N;
	my $variance = (($m1*$n)*($N-$n)*($N-$m1))/(($N**2)*($N-1));
	my $skewness = (($N-2*$m1)*(sqrt($N-1))*($N-2*$n))/((sqrt($n*$m1*($N-$m1)*($N-$n)))*($N-2));
	
	return ($mean, $variance, $skewness);
}

sub same_size {
	my ($m, $sets, $k, $N, $N_choose_m, $p_cache) = @_;
	croak("Currently only works for three sets") unless $sets == 3;
	
	my $m_minus_k = $m - $k;
	unless (defined $N_choose_m) {
		$N_choose_m = Math::BigInt->new($N);
		$N_choose_m->bnok($m);
	}
	
	if (! exists $$p_cache{3}{$k}) {
		my $p = Math::BigFloat->bzero();
		
		# The calculations are iterative; we thus need a starting point,
		# which is the case for two sets, ie the hypergeometric distribution.
		for (my $x=$k; $x<=$m; $x++) {
			my $p_hg;
			if (exists $$p_cache{2}{$x}) {
				$p_hg = Math::BigFloat->new($$p_cache{2}{$x});
			} else {
				($p_hg, undef) = hypergeo($m, $N-$m, $m, $x, $N_choose_m);
				$$p_cache{2}{$x} = $p_hg;
			}
			
			my $p_ss = Math::BigFloat->new($p_hg);
			
			my $x_choose_k = Math::BigInt->new($x);
			$x_choose_k->bnok($k);
			
			my $N_minus_x = $N - $x;
			my $N_x_choose_m_k = Math::BigInt->new($N_minus_x);
			$N_x_choose_m_k->bnok($m_minus_k);
			
			$p_ss->bmul($x_choose_k);
			$p_ss->bmul($N_x_choose_m_k);
			$p_ss->bdiv($N_choose_m);
			
			$p->badd($p_ss);
		}
		
		$$p_cache{3}{$k} = $p->bsstr();
	}
	
	return ($$p_cache{3}{$k}, $N_choose_m);
}

sub same_size_cdf {
	my ($m, $sets, $k, $N, $p_cache) = @_;
	my $cp = Math::BigFloat->bzero();
	
	my $k_zero = 0;
	if ($k == 0) {
		$k = 1;
		$k_zero = 1;
	}
	
	# We can save some time by caching 'N choose n' on the
	# first iteration, since it won't change.
	my $N_choose_m;
	for (my $x=$k; $x<=$m; $x++) {
		my $p;
		($p, $N_choose_m) = same_size($m, $sets, $x, $N, $N_choose_m, $p_cache);
		$cp->badd($p);
	}
	
	if ($k_zero) {
		$cp->bmul(-1);
		$cp->badd(1);
	}
	
	return $cp->bround(18);
}

sub same_size_expectation {
	my ($m, $sets, $N, $p_cache) = @_;
	my $expected = Math::BigFloat->bzero();
	
	# We can save some time by caching 'N choose n' on the
	# first iteration, since it won't change.
	my $N_choose_m;
	for (my $x=0; $x<=$m; $x++) {
		my $p;
		($p, $N_choose_m) = same_size($m, $sets, $x, $N, $N_choose_m, $p_cache);
		
		my $x_p = Math::BigFloat->new($x);
		$x_p->bmul($p);
		$expected->badd($x_p);
	}
	
	return $expected->bround(18);
}
################################################################################

################################################################################
# GEOMETRY OF OVERLAPPING CIRCLES
################################################################################
sub circle_radius {
	my ($area) = @_;
	return sqrt($area/pi);
}

sub circle_area {
	my ($radius) = @_;
	return pi*($radius**2);
}

sub central_angle_from_apothem {
	my ($r, $apothem) = @_;
	return 2*acos_real($apothem/$r);
}

sub central_angle_from_chord {
	my ($r, $chord) = @_;
	return 2*asin_real($chord/(2*$r));
}

sub chord_from_central_angle {
	my ($r, $theta) = @_;
	return 2*$r*sin($theta/2);
}

sub chord_from_apothem {
	my ($r, $apothem) = @_;
	return 2*sqrt($r**2-$apothem**2);
}

sub sector_area {
	my ($r, $theta) = @_;
	return ($r**2)*($theta/2);
}

sub segment_area {
	my ($r, $theta) = @_;
	return (($r**2)/2)*($theta - sin($theta));
}

sub triangle_area {
	my ($a, $b, $c) = @_;
	# Sorting and extra parentheses are to avoid rounding errors, as per
	# http://en.wikipedia.org/wiki/Heron's_formula, accessed 5/5/2011.
	($c, $b, $a) = sort($a, $b, $c);
	if ($c-($a-$b) < 0) {
		return undef;
	} else {
		return sqrt(($a+($b+$c))*($c-($a-$b))*($c+($a-$b))*($a+($b-$c)))/4;
	}
}

sub circles_area {
	my ($r_A, $r_B, $d) = @_;
	# Setup: two circles, A with centre at O, and B with centre at P,
	# have radii rA and rB, respectively. The distance between their centres
	# is d. If the circles do not intersect, then the overlap area is zero;
	# if one is contained within the other, the overlap is equal to the area
	# of the smaller circle. If we have an intermediate degree of overlap,
	# then the circles intersect at two points, C and D, and the line CD is
	# a chord. This chord splits the overlap area into two, and the total
	# area is the sum of the areas of the two segments defined by CD.
	
	my $apothem_A = ($d**2 - $r_B**2 + $r_A**2)/(2*$d);
	my $apothem_B = $d - $apothem_A;
	my $theta_A = central_angle_from_apothem($r_A, $apothem_A);
	my $theta_B = central_angle_from_apothem($r_B, $apothem_B);
	my $A = segment_area($r_A, $theta_A);
	my $B = segment_area($r_B, $theta_B);
	
	return ($A + $B);
}

sub circles_distance {
	my ($r_A, $r_B, $overlap, $precision, $verbose) = @_;
	# Work out the distance between two circles A and B with radii $r_A and
	# $r_B, which have an overlapping area of $overlap. This is the inverse
	# of the common form of this problem, ie finding the area of overlap
	# between two circles, given their centres and radii. There is no easy
	# way to get the distance directly, so we use a very simple numerical
	# optimisation, since area and distance are monotonically related.
	$precision = 1e-6 unless $precision;
	$verbose = 0 unless $verbose;
	my $d;
	my ($area_A, $area_B) = (circle_area($r_A), circle_area($r_B));
	if ($overlap == 0) {
		$d = $r_A+$r_B;
	} elsif (($area_A - $overlap) < $precision || ($area_B - $overlap) < $precision) {
		$d = undef;
	} else {
		my $d_lower = 0;
		my $d_upper = $r_A+$r_B;
		$d = $d_upper/2;
		my $opt_overlap = circles_area($r_A, $r_B, $d);
		print "d: $d, overlap: $opt_overlap\n" if $verbose;
		
		while (abs($overlap - $opt_overlap) > $precision) {
			if ($overlap > $opt_overlap) {
				print "";
				$d_upper = $d;
			} else {
				print "";
				$d_lower = $d;
			}
			$d = ($d_lower+$d_upper)/2;
			my $new_overlap = circles_area($r_A, $r_B, $d);
			if ($new_overlap == $opt_overlap) {
				warn "Failed to converge (d: $d, overlap: $new_overlap)";
				$d = undef;
				last;
			} else {
				$opt_overlap = $new_overlap;
			}
			print "d: $d, overlap: $opt_overlap\n" if $verbose;
		}
	}
	
	return $d;
}

sub circular_triangle_sides {
	my ($r_A, $r_B, $r_C, $d_AB, $d_AC, $d_BC) = @_;
	# Calculate the lengths of the straight sides of the circular triangle
	# defined by the overlap of three circles A, B and C, with radii $r_A,
	# $r_B and $r_C, and distances between their centres $d_AB, $d_AC and
	# $d_BC. These straights sides are chords of the circles, with endpoints
	# defined by the intersection points of the circles. The algorithm is
	# from Fewell, M.P. (2006) 'Area of common overlap of three circles',
	# and the 'Steps' here correspond to those of the paper. (We omit his
	# step 7 - that's done by a separate function 'circular_triangle_area'.)
	my ($chord_A, $chord_B, $chord_C);
	
	# A pre-requisite is that rA >= rB >= rC.
	if ($r_A < $r_B) {
		($r_A, $r_B) = ($r_B, $r_A);
		($d_AC, $d_BC) = ($d_BC, $d_AC);
	}
	if ($r_A < $r_C) {
		($r_A, $r_C) = ($r_C, $r_A);
		($d_AB, $d_BC) = ($d_BC, $d_AB);
	}
	if ($r_B < $r_C) {
		($r_B, $r_C) = ($r_C, $r_B);
		($d_AB, $d_AC) = ($d_AC, $d_AB);
	}
	
	# Step 0 - Check that the distances form a triangle.
	if ($d_AB >= $d_AC+$d_BC || $d_AC >= $d_AB+$d_BC || $d_BC >= $d_AC+$d_BC) {
		warn "Triangle does not exist";
	} else {
		# Step 1 - Check that the circles intersect.
		if ($r_A-$r_B > $d_AB || $r_A+$r_B < $d_AB) {
			warn "Circular triangle does not exist";
		} else {
			# Step 2 - Coordinates of intersection between A and B.
			my $x_AB = ($r_A**2 - $r_B**2 + $d_AB**2) / (2*$d_AB);
			my $y_AB = (sqrt((2*$d_AB**2) * ($r_A**2+$r_B**2) - ($r_A**2-$r_B**2)**2 - $d_AB**4)) / (2*$d_AB);
			
			# Step 3 - Calculate sine and cosine of pertinent angles.
			my $cos_theta_AC = ($d_AB**2 + $d_AC**2 - $d_BC**2) / (2*$d_AB*$d_AC);
			my $cos_theta_BC = -1 * ($d_AB**2 - $d_AC**2 + $d_BC**2) / (2*$d_AB*$d_BC);
			if ($cos_theta_AC**2 > 1 || $cos_theta_BC**2 > 1) {
				warn "Circular triangle does not exist";
			} else {
				my $sin_theta_AC = sqrt(1 - $cos_theta_AC**2);
				my $sin_theta_BC = sqrt(1 - $cos_theta_BC**2);
				
				# Step 4 - Check that the third circle makes a circular triangle.
				my $term_1 = ($x_AB - ($d_AC*$cos_theta_AC))**2;
				my $term_2a = ($y_AB - ($d_AC*$sin_theta_AC))**2;
				my $term_2b = ($y_AB + ($d_AC*$sin_theta_AC))**2;
				if ($term_1+$term_2a > $r_C**2 || $term_1+$term_2b < $r_C**2) {
					warn "Circular triangle does not exist";
				} else {
					# Step 5 - Coordinates of intersection of C with A and B.
					my $x_AC_1 = ($r_A**2 - $r_C**2 + $d_AC**2) / (2*$d_AC);
					my $y_AC_1 = -1 * (sqrt((2*$d_AC**2) * ($r_A**2+$r_C**2) - ($r_A**2-$r_C**2)**2 - $d_AC**4)) / (2*$d_AC);
					my $x_AC = ($x_AC_1*$cos_theta_AC) - ($y_AC_1*$sin_theta_AC);
					my $y_AC = ($x_AC_1*$sin_theta_AC) + ($y_AC_1*$cos_theta_AC);
					
					my $x_BC_1 = ($r_B**2 - $r_C**2 + $d_BC**2) / (2*$d_BC);
					my $y_BC_1 = (sqrt((2*$d_BC**2) * ($r_B**2+$r_C**2) - ($r_B**2-$r_C**2)**2 - $d_BC**4)) / (2*$d_BC);
					my $x_BC = ($x_BC_1*$cos_theta_BC) - ($y_BC_1*$sin_theta_BC) + $d_AB;
					my $y_BC = ($x_BC_1*$sin_theta_BC) + ($y_BC_1*$cos_theta_BC);
					
					# Step 6 - Calculate the chord lengths.
					$chord_A = sqrt(($x_AB-$x_AC)**2 + ($y_AB-$y_AC)**2);
					$chord_B = sqrt(($x_AB-$x_BC)**2 + ($y_AB-$y_BC)**2);
					$chord_C = sqrt(($x_AC-$x_BC)**2 + ($y_AC-$y_BC)**2);
				}
			}
		}
	}
	
	return ($chord_A, $chord_B, $chord_C);
}

sub circular_triangle_area {
	my ($a, $b, $c, $r_A, $r_B, $r_C) = @_;
	my $triangle_area = triangle_area($a, $b, $c);
# Worry about undef triangle areas?
	my $segment_A = segment_area($r_A, central_angle_from_chord($r_A, $a));
	my $segment_B = segment_area($r_B, central_angle_from_chord($r_B, $b));
	my $segment_C = segment_area($r_C, central_angle_from_chord($r_C, $c));
	return $triangle_area + $segment_A + $segment_B + $segment_C;
}
################################################################################

################################################################################
# TWO OVERLAPPING SETS
################################################################################
sub two_sets_factor {
	my ($A, $B, $overlap, $N) = @_;
	my $expected = ($A*$B)/$N;
	my $factor = $overlap/$expected;
	return ($expected, $factor);
}

sub two_sets_prob {
	my ($A, $B, $overlap, $N) = @_;
	return hypergeo_cdf($A, $N-$A, $B, $overlap);
}

sub two_sets {
	my ($A, $B, $overlap, $N) = @_;
	my ($expected, $factor) = two_sets_factor($A, $B, $overlap, $N);
	my $p = two_sets_prob($A, $B, $overlap, $N);
	return ($expected, $factor, $p);
}
################################################################################

################################################################################
# TWO OVERLAPPING LISTS
################################################################################
sub two_lists_stats {
	my ($list_A, $list_B, $size_only) = @_;
	$size_only = 0 unless $size_only;
	$list_A = uniquify_list($list_A);
	$list_B = uniquify_list($list_B);
	my @list_A_B = simple_intersect($list_A, $list_B);
	my @union = simple_union($list_A, $list_B);
	if ($size_only) {
		return (scalar(@$list_A), scalar(@$list_B), scalar(@list_A_B), scalar(@union));
	} else {
		return ($list_A, $list_B, \@list_A_B, \@union);
	}
}

sub two_lists_factor {
	my ($list_A, $list_B, $N) = @_;
	my ($A, $B, $overlap, $union) = two_lists_stats($list_A, $list_B, 1);
	$N = $union unless $N;
	my $expected = ($A*$B)/$N;
	my $factor = $overlap/$expected;
	return ($expected, $factor, $A, $B, $overlap, $N);
}

sub two_lists_prob {
	my ($list_A, $list_B, $N) = @_;
	my ($A, $B, $overlap, $union) = two_lists_stats($list_A, $list_B, 1);
	$N = $union unless $N;
	return (hypergeo_cdf($A, $N-$A, $B, $overlap), $A, $B, $overlap, $N);
}

sub two_lists {
	my ($list_A, $list_B, $N) = @_;
	my ($expected, $factor, $A, $B, $overlap, $union) = two_lists_factor($list_A, $list_B, $N);
	$N = $union unless $N;
	my $p = two_sets_prob($A, $B, $overlap, $N);
	return ($expected, $factor, $p, $A, $B, $overlap, $N);
}
################################################################################

################################################################################
# SAME SIZE OVERLAPPING SETS
################################################################################
sub same_size_sets_factor {
	my ($A, $sets, $overlap, $N, $p_cache) = @_;
	my $expected = same_size_expectation($A, $sets, $overlap, $N, $p_cache);
	my $factor = $overlap/$expected;
	return ($expected, $factor);
}

sub same_size_sets_prob {
	my ($A, $sets, $overlap, $N, $p_cache) = @_;
	return same_size_cdf($A, $sets, $overlap, $N, $p_cache);
}

sub same_size_sets {
	my ($A, $sets, $overlap, $N) = @_;
	my $p_cache = {};
	my ($expected, $factor) = same_size_sets_factor($A, $sets, $overlap, $N, $p_cache);
	my $p = same_size_sets_prob($A, $sets, $overlap, $N, $p_cache);
	return ($expected, $factor, $p);
}
################################################################################

################################################################################
# SAME SIZE OVERLAPPING LISTS
################################################################################
sub same_size_lists_stats {
	my ($lists) = @_;
	my @overlap = @{$$lists[0]};
	my @union = @{$$lists[0]};
	my $A = scalar(@overlap);
	my $sets = scalar(@$lists);
	for (my $i=0; $i<scalar(@$lists); $i++) {
		$$lists[$i] = uniquify_list($$lists[$i]);
		if (scalar(@{$$lists[0]}) != $A) {
			die "Duplicates aren't permitted in lists of the same size";
		}
		@overlap = simple_intersect(\@overlap, $$lists[$i]);
		@union = simple_union(\@union, $$lists[$i]);
	}
	return ($A, $sets, scalar(@overlap), scalar(@union));
}

sub same_size_lists_factor {
	my ($lists, $N, $p_cache) = @_;
	my ($A, $sets, $overlap, $union) = same_size_lists_stats($lists);
	$N = $union unless $N;
	my $expected = same_size_expectation($A, $sets, $overlap, $N, $p_cache);
	my $factor = $overlap/$expected;
	return ($expected, $factor, $A, $sets, $overlap, $N);
}

sub same_size_lists_prob {
	my ($lists, $N, $p_cache) = @_;
	my ($A, $sets, $overlap, $union) = same_size_lists_stats($lists);
	$N = $union unless $N;
	return (same_size_cdf($A, $sets, $overlap, $N, $p_cache), $A, $sets, $overlap, $N);
}

sub same_size_lists {
	my ($lists, $N) = @_;
	my $p_cache = {};
	my ($expected, $factor, $A, $sets, $overlap, $union) = same_size_lists_factor($lists, $N, $p_cache);
	$N = $union unless $N;
	my $p = same_size_sets_prob($A, $sets, $overlap, $N, $p_cache);
	return ($expected, $factor, $p, $A, $sets, $overlap, $N);
}
################################################################################

################################################################################
# THREE OVERLAPPING SETS
################################################################################
sub three_sets_validity {
	my ($A, $B, $C, $AB, $AC, $BC, $ABC) = @_;
	my $A_only = $A-$AB-$AC+$ABC;
	my $B_only = $B-$AB-$BC+$ABC;
	my $C_only = $C-$AC-$BC+$ABC;
	my $AB_only = $AB-$ABC;
	my $AC_only = $AC-$ABC;
	my $BC_only = $BC-$ABC;
	my $A_not = $B_only+$C_only+$BC_only;
	my $B_not = $A_only+$C_only+$AC_only;
	my $C_not = $A_only+$B_only+$AB_only;
	
	# Note that we're not testing whether a Venn diagram exists, which requires
	# all of the above values to be non-zero. Rather, we're looking for values
	# of total overlap that are mathematically impossible, ie zeroes are OK.
	return 0 if $AB > $A+$B;
	return 0 if $AC > $A+$C;
	return 0 if $BC > $B+$C;
	return 0 if $ABC >= $AB+$AC+$BC;
	return 0 if $A_only < 0 || $B_only < 0 || $C_only < 0;
	return 0 if $AB_only < 0 || $AC_only < 0 || $BC_only < 0;
	return 0 if $A_not < 0 || $B_not < 0 || $C_not < 0;
	
	return 0 if $A < $AB_only+$AC_only+$ABC;
	return 0 if $B < $AB_only+$BC_only+$ABC;
	return 0 if $C < $AC_only+$BC_only+$ABC;
	
	return 1;
}

sub three_sets_bounds {
	my ($A, $B, $C, $AB, $AC, $BC) = @_;
	my $ABC_lower = max(0, $AB+$AC-$A, $AB+$BC-$B, $AC+$BC-$C);
	my $ABC_upper = min($AB, $AC, $BC);
	return ($ABC_lower, $ABC_upper);
}

sub three_sets_factor {
	my ($A, $B, $C, $AB, $AC, $BC, $ABC) = @_;
	my $r_A = circle_radius($A);
	my $r_B = circle_radius($B);
	my $r_C = circle_radius($C);
	my $d_AB = circles_distance($r_A, $r_B, $AB);
	my $d_AC = circles_distance($r_A, $r_C, $AC);
	my $d_BC = circles_distance($r_B, $r_C, $BC);
	my ($a, $b, $c) = circular_triangle_sides($r_A, $r_B, $r_C, $d_AB, $d_AC, $d_BC);
	return (undef, undef) unless $a;
	my $expected = circular_triangle_area($a, $b, $c, $r_A, $r_B, $r_C);
	my $factor = $ABC/$expected;
	return ($expected, $factor);
}

sub three_sets_prob {
	my ($A, $B, $C, $AB, $AC, $BC, $ABC, $N) = @_;
	return undef;
}

sub three_sets {
	my ($A, $B, $C, $AB, $AC, $BC, $ABC, $N) = @_;
	my ($expected, $factor) = three_sets_factor($A, $B, $C, $AB, $AC, $BC, $ABC);
	my $p = three_sets_prob($A, $B, $C, $AB, $AC, $BC, $ABC, $N);
	return ($expected, $factor, $p);
}
################################################################################

################################################################################
# THREE OVERLAPPING LISTS
################################################################################
sub three_lists_stats {
	my ($list_A, $list_B, $list_C, $size_only) = @_;
	$size_only = 0 unless $size_only;
	$list_A = uniquify_list($list_A);
	$list_B = uniquify_list($list_B);
	$list_C = uniquify_list($list_C);
	my @list_A_B = simple_intersect($list_A, $list_B);
	my @list_A_C = simple_intersect($list_A, $list_C);
	my @list_B_C = simple_intersect($list_B, $list_C);
	my @list_A_B_C = simple_intersect(\@list_A_B, $list_C);
	my @union = simple_union($list_A, $list_B);
	@union = simple_union(\@union, $list_C);
	if ($size_only) {
		return (scalar(@$list_A), scalar(@$list_B), scalar(@$list_C),
			scalar(@list_A_B), scalar(@list_A_C), scalar(@list_B_C),
			scalar(@list_A_B_C), scalar(@union));
	} else {
		return ($list_A, $list_B, $list_C,
			\@list_A_B, \@list_A_C, \@list_B_C, \@list_A_B_C, \@union);
	}
}

sub three_lists_factor {
	my ($list_A, $list_B, $list_C, $N) = @_;
	my ($A, $B, $C, $AB, $AC, $BC, $ABC, $union) = three_lists($list_A, $list_B, $list_C);
	$N = $union unless $N;
	my $r_A = circle_radius($A);
	my $r_B = circle_radius($B);
	my $r_C = circle_radius($C);
	my $d_AB = circles_distance($r_A, $r_B, $AB);
	my $d_AC = circles_distance($r_A, $r_C, $AC);
	my $d_BC = circles_distance($r_B, $r_C, $BC);
	my ($a, $b, $c) = circular_triangle_sides($r_A, $r_B, $r_C, $d_AB, $d_AC, $d_BC);
	my $expected = circular_triangle_area($a, $b, $c, $r_A, $r_B, $r_C);
	my $factor = $ABC/$expected;
	return ($expected, $factor, $A, $B, $C, $AB, $AC, $BC, $ABC, $N);
}

sub three_lists_prob {
	my ($list_A, $list_B, $list_C, $N) = @_;
	my ($A, $B, $C, $AB, $AC, $BC, $ABC, $union) = three_lists($list_A, $list_B, $list_C);
	$N = $union unless $N;
	return (undef, $A, $B, $C, $AB, $AC, $BC, $ABC, $N);
}

sub three_lists {
	my ($list_A, $list_B, $list_C, $N) = @_;
	my ($expected, $factor, $A, $B, $C, $AB, $AC, $BC, $ABC, $union)
		 = three_lists_factor($list_A, $list_B, $list_C, $N);
	$N = $union unless $N;
	my $p = three_sets_prob($A, $B, $C, $AB, $AC, $BC, $ABC, $N);
	return ($expected, $factor, $p, $A, $B, $C, $AB, $AC, $BC, $ABC, $N);
}
################################################################################

################################################################################
# GENERATING RANDOM DATA
################################################################################
sub random_overlap {
	my ($A, $B, $C, $N) = @_;
	my (@A, @B, @C);
	
	my @ids = (1 .. $N);
	for my $i (1 .. $A) {
		push @A, random_select_and_remove(\@ids);
	}
	
	push @ids, @A;
	for my $i (1 .. $B) {
		push @B, random_select_and_remove(\@ids);
	}
	
	push @ids, @B;
	for my $i (1 .. $C) {
		push @C, random_select_and_remove(\@ids);
	}
	
	return three_lists_stats(\@A, \@B, \@C, 1);
}

sub random_constrained_overlap {
	my ($A, $B, $C, $N, $AB, $AC, $BC) = @_;
	my (@A, @B, @C, @AB, @AC, @BC);
	
	my @ids = (1 .. $N);
	for my $i (1 .. $A) {
		push @A, random_select_and_remove(\@ids);
	}
	
	for my $i (1 .. $AB) {
		push @AB, random_select_and_remove(\@A);
	}
	for my $i ($AB+1 .. $B) {
		push @B, random_select_and_remove(\@ids);
	}
	push @A, @AB;
	push @B, @AB;
	
	my ($min_ABC, $max_ABC) = three_sets_bounds($A, $B, $C, $AB, $AC, $BC);
	
	if ($min_ABC == 0) {
		# ABC can be zero (but isn't necessarily).
		
		# Get the C-only IDs first.
		for my $i ($AC+$BC+1 .. $C) {
			push @C, random_select_and_remove(\@ids);
		}
		# Then those that overlap with A.
		for my $i (1 .. $AC) {
			push @AC, random_select_and_remove(\@A);
		}
		# Then the B overlaps, having removed any of the
		# A overlaps that are also in AB.
		my @AB_AC_int = simple_intersect(\@AB, \@AC);
		push @BC, @AB_AC_int;
		my @B_prime = simple_diff(\@B, \@AB);
		if (scalar(@AB_AC_int) < $BC) {
			for my $i (scalar(@AB_AC_int)+1 .. $BC) {
				push @BC, random_select_and_remove(\@B_prime);
			}
		}
		push @A, @AC;
		push @C, @AC;
		push @C, @BC;
	} else {
		# ABC is non-zero, so AC and BC must have some values in common.
		for my $i (1 .. $min_ABC) {
			push @AC, random_select_and_remove(\@AB);
		}
		push @AB, @AC;
		push @BC, @AC;
		
		# Note that we produce ABC overlaps that mightn't actually
		# be possible, since we don't take account of the upper
		# bound defined by the smallest set size - we do this because
		# the reason we need these random lists is to calculate
		# probabilities using the truncated normal distribution, for
		# which we need the mean and SD as if there was no truncation.
		
		# The above is wrong - want to get values only in the appropriate
		# range; already have mean/expected value, so can use that and
		# whatever we get to estimate sd, given symmetry of normal dist.
		if ($min_ABC < $AC) {
			my @A_prime = simple_diff(\@A, \@AC);
			for my $i ($min_ABC+1 .. $AC) {
				push @AC, random_select_and_remove(\@A_prime);
			}
		}
		if ($min_ABC < $BC) {
			my @B_prime = simple_diff(\@B, \@AB);
			for my $i ($min_ABC+1 .. $BC) {
				push @BC, random_select_and_remove(\@B_prime);
			}
		}
		push @C, @AC;
		my @BC_prime = simple_diff(\@BC, \@AC);
		push @C, @BC_prime;
		if ($C > scalar(@C)) {
			for my $i (scalar(@C)+1 .. $C) {
				push @C, random_select_and_remove(\@ids);
			}
		}
	}
	
	return three_lists_stats(\@A, \@B, \@C, 1);
}

sub random_overlaps {
	my ($A, $B, $C, $N, $repeats, $out_file) = @_;
	my %overlaps;
	
	for my $i (1 .. $repeats) {
		my (undef, undef, undef, $AB, $AC, $BC, $ABC, undef) = random_overlap($A, $B, $C, $N);
		$overlaps{'AB'}{$AB}++;
		$overlaps{'AC'}{$AC}++;
		$overlaps{'BC'}{$BC}++;
		$overlaps{'ABC'}{$ABC}++;
	}
	
	if ($out_file) {
		my $data;
		foreach my $type (keys %overlaps) {
			foreach my $overlap (sort keys %{$overlaps{$type}}) {
				$data .= "$type\t$overlap\t".$overlaps{$type}{$overlap}."\n";
			}
		}
		save_to_file($data, $out_file);
	}
	
	return \%overlaps;
}

sub random_constrained_overlaps {
	my ($A, $B, $C, $N, $AB, $AC, $BC, $repeats, $out_file) = @_;
	my %overlaps;
	
	for my $i (1 .. $repeats) {
		my (undef, undef, undef, undef, undef, undef, $ABC, undef) = random_constrained_overlap($A, $B, $C, $N, $AB, $AC, $BC);
		$overlaps{'AB'}{$AB}++;
		$overlaps{'AC'}{$AC}++;
		$overlaps{'BC'}{$BC}++;
		$overlaps{'ABC'}{$ABC}++;
	}
	
	if ($out_file) {
		my $data;
		foreach my $type (keys %overlaps) {
			foreach my $overlap (sort keys %{$overlaps{$type}}) {
				$data .= "$type\t$overlap\t".$overlaps{$type}{$overlap}."\n";
			}
		}
		save_to_file($data, $out_file);
	}
	
	return \%overlaps;
}
################################################################################
1;

