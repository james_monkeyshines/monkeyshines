package MonkeyShines::Marmoset;

use warnings;
use strict;
use Carp;

our $VERSION = '0.1';

use MonkeyShines::Sequence qw (
	maf_blocks
	maf_blocks_fill
	from_fasta from_maf to_maf
	excise_columns
	alignment_columns
	flanking_lengths
);

use MonkeyShines::Utils qw (
	read_from_file
	save_to_file
	execute_check
	coord_overlap
	min max
);

use Exporter;
our @ISA = qw(Exporter);
our @EXPORT_OK = qw(
	epo_alignment
	species_locations
	sequence_quality
	remove_long_inserts
	remove_gappy
	remove_species
	crop_alignment
	gene_gain_loss
	effective_species_count
	multiz_alignment
	structure_files
	phase_class_files
	randomisations
	gene_position
	flanking_genes
	mafft_xinsi
	picxaa_r
);

=pod

=head1 Name

MonkeyShines::Marmoset - Multiple Alignments of RNA, for Modelling Of Structure and Evolutionary Theories.

=head1 Description

MonkeyShines::Marmoset

=head1 Functions

=head2 Generate Timestamp

=over

=item B<$timestamp_id = timestamp_id([$max_random])>

Return a string consisting of a timestamp and a random number between 0 and
1000 (by default - specify $max_random to use a different upper limit). This
function is useful for generating names for temporary files.

=item B<$data = read_from_file($file)>

Read data from the given file location into a string.

=back

=head1 Author

James Allen
(james@monkeyshines.co.uk)

=head1 Copyright

Copyright 2012 James Allen; University of Manchester

=head1 Licence

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program. If not, see <http://www.gnu.org/licenses/>.

=cut

################################################################################
# GENERATE EPO ALIGNMENT
################################################################################
sub epo_alignment {
	my ($chr, $start, $end, $rev_comp, $slice_adaptor, $method_link_species_set) = @_;
	$chr =~ s/chr//;
	$start += 1;	# BLAT returns 0-based genomic coordinates, Ensembl uses 1-based.
	
	my $query_slice = $slice_adaptor->fetch_by_region('toplevel', $chr, $start, $end);
	if (!$query_slice) {
		croak "Ensembl call failed for chr$chr:$start-$end";
	}
	my $gab_adaptor = Bio::EnsEMBL::Registry->get_adaptor('Multi', 'compara', 'GenomicAlignBlock');
	my $genomic_align_blocks =
		$gab_adaptor->fetch_all_by_MethodLinkSpeciesSet_Slice(
			$method_link_species_set, $query_slice, undef, undef, 1);
	
	my $blocks = scalar(@$genomic_align_blocks) || 0;
	if ($blocks == 1) {
		$$genomic_align_blocks[0]->reverse_complement if $rev_comp;
		my $alignment = $$genomic_align_blocks[0]->get_SimpleAlign("uc");
		return ($blocks, $alignment);
	} else {
		return ($blocks, undef);
	}
}

sub species_locations {
	my ($seqs, $order, $file) = @_;
	my %genomic_locations;
	foreach my $id (keys %$seqs) {
		my ($species, $location) = $id =~ /^(\w+)\/(.+)$/;
		$species = ucfirst($species);
		$location =~ s!/!:!g;
		$genomic_locations{$species} = $location;
		$$seqs{$species} = delete $$seqs{$id};
		$$order{$species} = delete $$order{$id};
	}
	return wantarray ? %genomic_locations :\%genomic_locations;
}
################################################################################

################################################################################
# GENERATE MULTIZ ALIGNMENT
################################################################################
sub multiz_alignment {
	my ($chr, $start, $end, $strand, $ref_species, $epo, $maf_dir, $two_bit_dir, $maf_file) = @_;
	my ($blocks, $ambiguous_ref, $multiple_ref, $ambiguous, $multiple, $species_count) = (0, 0, 0, '', '', 0);
	my (%ambiguous, %multiple);
	
	my $maf_original_file = "$maf_file.orig";
	if (!-e $maf_original_file) {
		my $bed_file = "$maf_file.bed";
		save_to_file("$chr\t$start\t$end\t$ref_species\t0\t$strand\n", $bed_file);
		
		my @ucsc_species = keys %$epo;
		maf_blocks($bed_file, $maf_dir, $chr, \@ucsc_species, $maf_original_file);
		unlink($bed_file);
	}
	
	my $maf_original = read_from_file($maf_original_file);
	if ($maf_original =~ /^s\s+/m) {
		$blocks = () = $maf_original =~ /^(a\s)/gm;
		
		# By definition, a maf block has more than one species; but
		# this means that if we are to try and join blocks together,
		# then we will implicitly remove any sequence that only occurs
		# in the reference species. So, splice any such sequence into
		# the alignment, as a maf block with one species.
		maf_blocks_fill($maf_original_file, $two_bit_dir, $maf_file, $chr, $start, $end);
		
		# The 'from_maf' function merges blocks where the IDs match.
		# So if you have data from two scaffolds for one species,
		# these remain separate, assuming that the scaffold is part
		# of the ID.
		my ($seqs, $order) = from_maf(read_from_file($maf_file));
		
		# Check for multiple chromosomes/scaffolds within a species.
		my %species_seen;
		foreach my $id (keys %$seqs) {
			my ($ucsc_species) = $id =~ /^(\w+)/;
			my $species = $$epo{$ucsc_species};
			if (exists $species_seen{$species}) {
				if ($species eq $ref_species) {
					$multiple_ref = 1;
				} else {
					delete $$seqs{$id};
					delete $$order{$id};
					delete $$seqs{$species};
					delete $$order{$species};
					$multiple{$species} = 1;
				}
			} else {
				$species_seen{$species}++;
				$$seqs{$species} = uc(delete $$seqs{$id});
				$$order{$species} = uc(delete $$order{$id});
			}
		}
		
		# Remove sequences with ambiguity characters.
		foreach my $species (keys %$seqs) {
			if ($$seqs{$species} !~ /^[ACGTU\-]+$/) {
				if ($species eq $ref_species) {
					$ambiguous_ref = 1;
				} else {
					delete $$seqs{$species};
					delete $$order{$species};
					$ambiguous{$species} = 1;
				}
			} else {
				$species_count++;
			}
		}
		
		$seqs = excise_columns($seqs);
		save_to_file(to_maf($seqs, $order), $maf_file);
		
		$ambiguous = join(',', keys %ambiguous);
		$multiple = join(',', keys %multiple);
	}
	
	return ($blocks, $ambiguous_ref, $multiple_ref, $ambiguous, $multiple, $species_count);
}
################################################################################

################################################################################
# ALIGNMENT MANIPULATION
################################################################################
sub sequence_quality {
	my ($alignment, $ref_species) = @_;
	my ($ambiguous_ref, $multiple_ref, $ambiguous, $multiple, $species_count) = (0, 0, '', '', 0);
	my (%seqs, %ambiguous, %multiple);
	
	foreach my $seq ($alignment->each_seq) {
		my ($species) = ucfirst($seq->id) =~ /^(\w+)/;
		if ($seq->seq !~ /^[ACGTU\-]+$/) {
			if ($species eq $ref_species) {
				$ambiguous_ref = 1;
			} else {
				$alignment->remove_seq($seq);
				$ambiguous{$species} = 1;
			}
		} elsif ($seq->seq =~ /^[\-]+$/) {
			$alignment->remove_seq($seq);
			$ambiguous{$species} = 1;
		} else {
			push(@{$seqs{$species}}, $seq);
		}
	}
	
	foreach my $species (keys %seqs) {
		if (scalar(@{$seqs{$species}}) > 1) {
			if ($species eq $ref_species) {
				$multiple_ref = 1;
			}
    		foreach my $seq (@{$seqs{$species}}) {
				$alignment->remove_seq($seq);
			}
			$multiple{$species} = 1;
		} else {
			$species_count++;
		}
	}
	
	$ambiguous = join(',', keys %ambiguous);
	$multiple = join(',', keys %multiple);
	
	return ($ambiguous_ref, $multiple_ref, $ambiguous, $multiple, $species_count);
}

sub remove_long_inserts {
	my ($seqs, $order, $genomic_locations, $start, $end, $ref_species, $long_insert) = @_;
	my ($long_insert_ref, $long_insert_species, $species_count) = (0, '', 0);
	
	my $insert_threshold = ($end - $start + 1) * $long_insert;
	my ($ref_counter, $non_ref_counter) = (0, 0);
	my (%inserts, %long_insert_species);
	
	my @non_ref;
	foreach my $species (sort { $$order{$a} <=> $$order{$b} } keys %{$order}) {
		next if $species eq $ref_species;
		push @non_ref, $species;
	}
	
	my @cols = alignment_columns($seqs, $order);
	foreach my $col (@cols) {
		if ($col =~ /^\-(.*)/) {
			my @matches = split(//, $1);
			for my $i (0..$#matches) {
				if ($matches[$i] ne '-') {
					$inserts{$non_ref[$i]}++;
				}
			}
			$non_ref_counter++;
		} elsif ($col =~ /^[^\-]\-+$/) {
			$ref_counter++;
		} else {
			if ($non_ref_counter > $insert_threshold) {
				foreach my $species (keys %inserts) {
					if ($inserts{$species} > $insert_threshold) {
						$long_insert_species{$species}++;
					}
				}
			}
			if ($ref_counter > $insert_threshold) {
				$long_insert_ref = 1;
			}
			undef %inserts;
			$non_ref_counter = 0;
			$ref_counter = 0;
		}
	}
	if (scalar(keys %long_insert_species) > 0) {
		foreach my $species (keys %long_insert_species) {
			delete $$seqs{$species};
			delete $$order{$species};
			delete $$genomic_locations{$species};
		}
		$long_insert_species = join(',', keys %long_insert_species);
		
		# Remove any all gap columns.
		$seqs = excise_columns($seqs);
	}
	$species_count = scalar(keys(%$seqs));
	
	return ($long_insert_ref, $long_insert_species, $species_count, $seqs);
}

sub remove_gappy {
	my ($seqs, $order, $genomic_locations, $ref_species, $max_gap) = @_;
	my ($max_gap_ref, $max_gap_species, $species_count) = (0, '', 0);
	
	my $gap_threshold = length($$seqs{$ref_species}) * $max_gap;
	my %max_gap_species;
	
	foreach my $species (keys %$seqs) {
		my $gaps = () = $$seqs{$species} =~ /(\-)/g;
		if ($gaps >= $gap_threshold) {
			if ($species eq $ref_species) {
				$max_gap_ref = 1;
			}
			$max_gap_species{$species}++;
		}
	}
	if (scalar(keys %max_gap_species) > 0) {
		foreach my $species (keys %max_gap_species) {
			delete $$seqs{$species};
			delete $$order{$species};
			delete $$genomic_locations{$species};
		}
		$max_gap_species = join(',', keys %max_gap_species);
		
		# Remove any all gap columns.
		$seqs = excise_columns($seqs);
	}
	$species_count = scalar(keys(%$seqs));
	
	return ($max_gap_ref, $max_gap_species, $species_count, $seqs);
}

sub remove_species {
	my ($seqs, $order, $genomic_locations, $species) = @_;
	foreach my $species (@$species) {
		delete $$seqs{$species};
		delete $$order{$species};
		delete $$genomic_locations{$species};
	}
	my $species_count = scalar(keys(%$seqs));
	return ($species_count);
}

sub crop_alignment {
	my ($seqs, $order, $flanking, $ref_species, $ref_file) = @_;
	my $no_flanking = 0;
	
	my ($rna_seqs, undef) = from_fasta(read_from_file($ref_file));
	my $rna_seq = $$rna_seqs{'reference'};
	
	# Crop each alignment to have a given amount of flanking
	# (rather than the ref sequence having a particular number
	# of flanking bases, excluding gaps).
	my $ref_seq = $$seqs{$ref_species};
	my ($upstream_length, $aligned_rna_length, $downstream_length)
		= flanking_lengths($rna_seq, $ref_seq);
	if ($upstream_length < $flanking || $downstream_length < $flanking) {
		$no_flanking = 1;
	} else {
		my $offset = $upstream_length - $flanking;
		my $length = 2*$flanking + $aligned_rna_length;
		foreach my $id (sort {$$order{$a} <=> $$order{$b}} keys %$order) {
			$$seqs{$id} = substr($$seqs{$id}, $offset, $length);
		}
	}
	
	return ($no_flanking, $aligned_rna_length);
}

sub gene_gain_loss {
	my ($seqs) = @_;
	my $gain_loss = 0;
	foreach my $seq (values %$seqs) {
		if ($seq =~ /^\-+$/) {
			$gain_loss = 1;
			next;
		}
	}
	return ($gain_loss);
}

sub effective_species_count {
	my ($seqs) = @_;
	my %unique;
	foreach my $seq (values %$seqs) {
		$unique{$seq}++;
	}
	my $effective = scalar(keys(%unique));
	
	return ($effective);
}
################################################################################

################################################################################
# GENERATE STRUCTURE FILES
################################################################################
sub structure_files {
	my ($structure, $ref_sequence, $flanking, $target_dir) = @_;
	
	$structure =~ tr/<>/()/;
	my @structure = split(//, $structure);
	my $aligned_rna_length = length($ref_sequence) - 2*$flanking;
	
	my @seq = split(//, $ref_sequence);
	my @aligned_structure;
	my $structure_position = 0;
	for my $position ($flanking..($flanking + $aligned_rna_length - 1)) {
		if ($seq[$position] eq '-') {
			push @aligned_structure, '.';
		} else {
			push @aligned_structure, $structure[$structure_position];
			$structure_position++;
		}
	}
	my $aligned_structure = join('', @aligned_structure);
	my $flanking_structure = '.' x $flanking;
	$aligned_structure = "$flanking_structure$aligned_structure$flanking_structure";
	
	my $aligned_structure_file = "$target_dir/$flanking.structure.txt";
	save_to_file($aligned_structure, $aligned_structure_file);
	
	return $aligned_structure;
}

sub phase_class_files {
	my ($aligned_structure, $flanking, $target_dir) = @_;
	
	# These class definitions are used in PHASE to partition the alignment
	# according to a) whether the whole RNA is considered separately from
	# any flanking, b) whether stems are different from loops (but where
	# loops are the same as flanking), c) whether stems, loops and flanking
	# are all in separate paritions.
	my $aligned_rna_length = length($aligned_structure) - 2*$flanking;
	my $rna = '2 ' x $aligned_rna_length;
	(my $stem = $aligned_structure) =~ s/\./1 /g;
	$stem =~ s/[^1 ]/2 /g;
	my $stem_loop = substr($aligned_structure, $flanking, $aligned_rna_length);
	$stem_loop =~ s/\./2 /g;
	$stem_loop =~ s/[^2 ]/3 /g;
	
	if ($flanking == 0) {
		save_to_file($stem, "$target_dir/$flanking.stem_class.txt");
	} else {
		my $flanking_pattern = '1 ' x $flanking;
		save_to_file("$flanking_pattern$rna$flanking_pattern", "$target_dir/$flanking.rna_class.txt");
		save_to_file("$stem", "$target_dir/$flanking.stem_class.txt");
		save_to_file("$flanking_pattern$stem_loop$flanking_pattern", "$target_dir/$flanking.stem_loop_class.txt");
	}
	
	return;
}
################################################################################

################################################################################
# RANDOMISATIONS
################################################################################
# Randomise an alignment with MultiPerm.
sub randomisations {
	my ($maf_file, $randomisations) = @_;
	for my $i (1..$randomisations) {
		(my $random_file = $maf_file) =~ s/(\.\w+)$/_random_$i$1/;
		my $execute = "multiperm $maf_file";
		my $out = execute_check($execute, "ERROR", 0, 'MultiPerm', 1);
		if ($out) {
			save_to_file($out, $random_file);
		} else {
			croak "ERROR: Randomisation failed: '$execute'";
		}
	}
	return;
}
################################################################################

################################################################################
# ENSEMBL GENE OVERLAP
################################################################################
# The next two functions require the following in the calling script:
# Bio::EnsEMBL::Registry->load_registry_from_db(
#	-host => 'ensembldb.ensembl.org',
#	-user => 'anonymous'
#);

# Determine overlap with protein-coding gene
sub gene_position {
	my ($chr, $start, $end, $species) = @_;
	
	my $slice_adaptor = Bio::EnsEMBL::Registry->get_adaptor($species, 'Core', 'Slice');
	my $slice = $slice_adaptor->fetch_by_region('chromosome', $chr, $start, $end);
	if (!$slice) {
		croak "Cannot fetch data for given co-ordinates.";
	}
	
	my %positions;
	my $location = [$start, $end];
	
	my $start_offset = defined $start ? $start-1 : 0;
	my $genes = $slice->get_all_Genes_by_type('protein_coding');
	foreach my $gene (@{$genes}) {
		my $id = $gene->stable_id();
		
		my $t = $gene->canonical_transcript();
		
		my $t_start = $t->start();
		my $t_end = $t->end();
		my $cds_start = $t->coding_region_start();
		my $cds_end = $t->coding_region_end();
		
		my $utr5_location;
		my $utr3_location;
		if ($t->strand() == 1) {
			$utr5_location = [$t_start, $cds_start-1];
			$utr3_location = [$cds_end+1, $t_end];
		} else {
			$utr3_location = [$t_start, $cds_start-1];
			$utr5_location = [$cds_end+1, $t_end];
		}
		
		my %position;
		
		# The overlap value returned indicates the nature of the overlap;
		# if 'a' is the first location and 'b' the second:
		# 0 = no overlap
		# 1 = a entirely within b
		# 2 = b entirely within a
		# 3 = end of a overlaps start of b
		# 4 = start of a overlap end of b
		
		my $utr5_overlap = coord_overlap($location, $utr5_location);
		if ($utr5_overlap == 1) {
			$position{"Within 5' UTR"}++;
		} elsif ($utr5_overlap) {
			$position{"Overlap 5' UTR"}++;
		}
		my $utr3_overlap = coord_overlap($location, $utr3_location);
		if ($utr3_overlap == 1) {
			$position{"Within 3' UTR"}++;
		} elsif ($utr3_overlap) {
			$position{"Overlap 3' UTR"}++;
		}
		
		my $exons = $t->get_all_Exons();
		foreach my $exon (@{$exons}) {
			# If either of the next two are true, then we're in a UTR-only exon.
			
			#next if $exon->start() + $start_offset > $cds_end;
			#next if $exon->end() + $start_offset < $cds_start;
			next unless defined $exon->coding_region_start($t);
			
			my $exon_location = [max($cds_start, $exon->coding_region_start($t)), min($cds_end, $exon->coding_region_end($t))];
			my $exon_overlap = coord_overlap($location, $exon_location);
			if ($exon_overlap == 1) {
				$position{"Within CDS"}++;
			} elsif ($exon_overlap) {
				$position{"Overlap CDS"}++;
			}
		}
		
		my $introns = $t->get_all_Introns();
		foreach my $intron (@{$introns}) {
			my $intron_location = [$intron->start(), $intron->end()];
			my $intron_overlap = coord_overlap($location, $intron_location);
			if ($intron_overlap == 1) {
				$position{"Within Intron"}++;
			} elsif ($intron_overlap) {
				$position{"Overlap Intron"}++;
			}
		}
		
		if (scalar(keys %position)) {
			$positions{join(",", sort keys %position)}++;
		}
	}
	
	my $position = '';
	$position .= join(";", sort keys %positions);
	return ($position);
}

# Protein-coding genes in flanking regions
sub flanking_genes {
	my ($chr, $start, $end, $species, $flanking) = @_;
	
	my $slice_adaptor = Bio::EnsEMBL::Registry->get_adaptor($species, 'Core', 'Slice');
	my $slice = $slice_adaptor->fetch_by_region('chromosome', $chr, $start-$flanking, $end+$flanking);
	if (!$slice) {
		croak "Cannot fetch data for given co-ordinates.";
	}
	
	my %positions;
	my $location = [$start-$flanking, $end+$flanking];
	
	my $start_offset = defined $start ? $start-1 : 0;
	my $genes = $slice->get_all_Genes_by_type('protein_coding');
	foreach my $gene (@{$genes}) {
		my $id = $gene->stable_id();
		
		my $t = $gene->canonical_transcript();
		
		my $t_start = $t->start();
		my $t_end = $t->end();
		
		my $overlap = coord_overlap($location, [$t_start, $t_end]);
		if ($overlap) {
			if ($t->strand() == 1) {
				if ($t_end < $start) {
					$positions{'Downstream'}++;
				} elsif ($end < $t_start) {
					$positions{'Upstream'}++;
				}
			} else {
				if ($t_end < $start) {
					$positions{'Upstream'}++;
				} elsif ($end < $t_start) {
					$positions{'Downstream'}++;
				}
			}
		}
	}
	
	my $position = '';
	$position .= join(";", keys %positions);
	return ($position);
}
################################################################################

################################################################################
# STRUCTURAL ALIGNMENT
################################################################################
sub mafft_xinsi {
	my ($fasta_file, $new_fasta_file) = @_;
	my $success = 0;
	
	my $execute = "mafft-xinsi --quiet --preservecase $fasta_file";
	my $out = execute_check($execute, "MAFFT", 0, 'MAFFT X-INS-i', 0);
	if ($out) {
		if ($out =~ /^>\w+/) {
			save_to_file($out, $new_fasta_file);
			$success = 1;
		} else {
			$out =~ s/\n//gm;
			carp "MAFFT failed with error '$out'. Executing: '$execute'";
		}
	} else {
		carp "MAFFT failed, no data. Executing: '$execute'";
	}
	return $success;
}

sub picxaa_r {
	my ($fasta_file, $new_fasta_file, $structure_file) = @_;
	my $success = 0;
	
	my $execute = "picxaa-r $fasta_file";
	my $out = execute_check($execute, 'ERROR', 0, 'PicXAA-R', 0);
	if ($out) {
		if ($out !~ /ERROR/) {
			my ($structure) = $out =~ /SS_cons(.+)/ms;
			$structure =~ s/\s//gm;
			save_to_file($structure, $structure_file);
			
			$out =~ s/^>#=GC[^>]+//m;
			save_to_file($out, $new_fasta_file);
			
			$success = 1;
		} else {
			$out =~ s/\n//gm;
			carp "PICXAA-R failed with error '$out'. Executing: '$execute'";
		}
	} else {
		carp "PICXAA-R failed, no data. Executing: '$execute'";
	}
	return $success;
}
################################################################################

1;

