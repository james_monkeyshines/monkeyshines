package MonkeyShines::Tree;

use warnings;
use strict;
use Carp;

our $VERSION = '0.1';

use Exporter;
our @ISA = qw(Exporter);
our @EXPORT_OK = qw(
	parse_tree
	print_tree
	unroot_tree
	bifurcate_node
	bifurcate_tree
	prune_node
	prune_tree
	tree_length
	leaf_nodes
	relabel
);

use MonkeyShines::Utils qw (
	sum
	flip_hash
);

=pod

=head1 Name

MonkeyShines::Tree - Tree operations

=head1 Description

MonkeyShines::Tree is a collection of simple functions for the
manipulation of phylogenetic trees. It uses a simple representation
which should make it relatively easy to add new functionality.
The BioPerl code for tree manipulation is more extensive,
but correspondingly more complex. Ditto the Bio::Phylo modules, which
are very object-oriented. The Unix Newick Utilities are a good
alternative to using this module if you're only performing simple
operations: http://cegg.unige.ch/newick_utils

=head1 Functions

=over

=item C<$tree = parse_tree($tree_string, [, $prune_nodes])>

$tree_string must be in Newick format; tabs and newlines are OK, as are
spaces in taxon names - spaces anywhere else will likely cause erroneous
results (but not necessarily an error message). Bifurcating or
multifurcating trees can be handled, and the trees can have branch
lengths and/or named internal nodes. An arrayref of nodes to
delete from the tree can optionally be given with the $prune_nodes
parameter; anything in the list that isn't in the tree will be ignored
(see also the prune_tree function).

C<$tree> is a hashref with node names as keys. If internal nodes are not
named in C<$tree_string>, a name is generated, of the format 'int_node_<num>'.
If you have any nodes in C<$tree_string> that match this format (!) then
you'll need to rename them. The root is given special treatment among
internal nodes and is imaginatively named 'root'. For programmatic
convenience, unrooted trees still have a 'root', with a tri-, rather than
bi-, furcation.

Each node (i.e. key) in C<$tree> has one or more subkeys, depending on the
type of node. All nodes except the root have a 'parent' subkey. All
internal nodes have an 'internal' subkey (always set to '1'), and a
'children' subkey, which is an arrayref of the names of child nodes. If
branch lengths are given in C<$tree_string> then all nodes except the root
have a 'blength' subkey.

=item C<$tree_string = print_tree($tree[, $pretty][, $node])>

Convert a hashref representation of a tree (i.e. the format returned by
the parse_tree function) to a Newick format string. By default, the
data is output on a single line; to add tabs and newlines that
enhance human readability, set C<$pretty> to a true value. By default
the entire tree is printed; to print a subtree, set C<$node> to the
internal node name that roots the subtree.

=item C<undef = unroot_tree($tree)>

Unroot a hashref representation of a tree (i.e. the format returned by
the parse_tree function) by creating a trifurcation at the root. If
the two children of the root are internal nodes, then the choice of
which one to "collapse", to create the trifurcation is arbitrary - this
function does it with the first child it processes, i.e. the one that
is leftmost in the original Newick tree.

=item C<undef = bifurcate_node($tree[, $recurse][, $node])>

Remove multifurcation from a node in a hashref representation of a tree
(i.e. the format returned by the parse_tree function) by inserting
branches of zero-length. C<$node> is the name of the internal node to process;
by default it is the root node. If C<$recurse> is set to a true value then
all internal nodes in the subtree rooted at C<$node> will be processed.

=item C<undef = bifurcate_tree($tree)>

Equivalent to C<bifurcate_node($tree, 1, 'root');>.

=item C<undef = prune_node($tree, $node)>

Delete a node in a hashref representation of a tree (i.e. the format
returned by the parse_tree function). If C<$node> is a leaf node with
only one sibling (i.e. the parent node is bifurcating), then the parent
node is also deleted, and the remaining sibling's former grandparent
becomes its parent. If C<$node> is an internal node, then the subtree
rooted at that node is deleted. The method will currently not remove
multifurcations properly, so don't give it a node that multifurcates.

=item C<undef = prune_tree($tree, $nodes)>

Execute C<prune_node> for every node in the arrayref $nodes. Note the
caveat about multifurcating nodes, above.

=item C<$length = tree_length($tree_string)>

Sum the branch lengths in a Newick-formatted tree.

=item C<@species|$species = leaf_nodes($tree_string)>

Return an array or arrayref of the leaf nodes, i.e. the taxa in the tree.

=item C<$relabelled_tree_string = relabel($tree_string, $mapping)>

Relabel a Newick-formatted tree, replacing the keys from the hashref
$mapping with the corresponding values.

=back

=head1 Examples

=head2 Delete nodes from a tree

=over

=item Remove the gorilla node and the monkey subtree from a tree with branch lengths.

=begin html

<pre>
    use MonkeyShines::Tree qw (parse_tree print_tree);
</pre>
<pre>
    my $tree_string = '((((Human:0.01,Chimp:0.01):0.005,Gorilla:0.01):0.02,Orangutan:0.1)Apes:0.3,(Macaque:0.2,Baboon:0.25)Monkeys:0.5)';
    my $prune_nodes = ['Gorilla', 'Monkeys'];
    my $tree = parse_tree($tree_string, $prune_nodes);
    print print_tree($tree)."\n";
</pre>

=end html

=item Output:

=begin html

<pre>
    ((Human:0.01,Chimp:0.01):0.025,Orangutan:0.1)Apes;
</pre>

=end html

=back

=head2 Unroot a tree

=over

=item Create a trifurcation at the root of a tree, effectively unrooting it.

=begin html

<pre>
    use MonkeyShines::Tree qw (parse_tree print_tree unroot_tree);
</pre>
<pre>
    my $tree_string = '((((Human:0.01,Chimp:0.01):0.005,Gorilla:0.01):0.02,Orangutan:0.1)Apes:0.3,(Macaque:0.2,Baboon:0.25)Monkeys:0.5)';
    my $tree = parse_tree($tree_string);
    unroot_tree($tree);
    print print_tree($tree)."\n";
</pre>

=end html

=item Output:

=begin html

<pre>
    (((Human:0.01,Chimp:0.01):0.005,Gorilla:0.01):0.02,Orangutan:0.1,(Macaque:0.2,Baboon:0.25)Monkeys:0.8);
</pre>

=end html

=back

=head2 Force a multifurcating tree to bifurcate

=over

=item Insert zero-length branches to make trifurcations into bifurcations. Print the tree with tabs and newlines, for readability.

=begin html

<pre>
    use MonkeyShines::Tree qw (parse_tree print_tree bifurcate_tree);
</pre>
<pre>
    my $tree_string = '(((Human:0.01,Chimp:0.01,Gorilla:0.01):0.02,Orangutan:0.1)Apes:0.3,(Macaque:0.2,Baboon:0.25,Marmoset:0.4)Monkeys:0.5)';
    my $tree = parse_tree($tree_string);
    bifurcate_tree($tree);
    print print_tree($tree, "pretty")."\n";
</pre>

=end html

=item Output:

=begin html

<pre>
    (
        (
            (
                Human:0.01,
                (
                    Chimp:0.01,
                    Gorilla:0.01
                ):0
            ):0.02,
            Orangutan:0.1
        )Apes:0.3,
        (
            Macaque:0.2,
            (
                Baboon:0.25,
                Marmoset:0.4
            ):0
        )Monkeys:0.5
    );
</pre>

=end html

=back

=head2 Find the length of a subtree

=over

=item Find the length of the ape subtree.

=begin html

<pre>
    use MonkeyShines::Tree qw (parse_tree print_tree tree_length);
</pre>
<pre>
    my $tree_string = '((((Human:0.01,Chimp:0.01):0.005,Gorilla:0.01):0.02,Orangutan:0.1)Apes:0.3,(Macaque:0.2,Baboon:0.25)Monkeys:0.5)';
    my $tree = parse_tree($tree_string);
    print "Ape subtree length: ".tree_length(print_tree($tree, 0, 'Apes'))."\n";
</pre>

=end html

=item Output:

=begin html

<pre>
    Ape subtree length: 0.155
</pre>

=end html

=back

=head1 Author

James Allen
(james@monkeyshines.co.uk)

=head1 Copyright

Copyright 2010-2012 James Allen; University of Manchester

=head1 Licence

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program. If not, see <http://www.gnu.org/licenses/>.

=cut

################################################################################
# TREE OPERATIONS
################################################################################
# Parse a Newick-formatted tree
sub parse_tree {
	my ($tree_string, $prune_nodes) = @_;
	$tree_string =~ s/[\t\n;]//gm;
	$tree_string =~ s/,\s+/,/g;
	
	$prune_nodes = [] unless $prune_nodes;
	
	# To help with subsequent parsing we take all of the labels in the
	# sequence, construct a mapping to new labels which we know are easy to
	# parse, then map back in the final parsed tree.
	
	# This'll get all labels, leaf nodes and internal ones.
	my @labels = $tree_string =~ /[\(,\)]\s*([\w\.]+|[\'\"][^\'\"]+[\'\"])/g;
	my $i = 0;
	my %labels = map {$_ => 'label_'.$i++} @labels;
	if (scalar(keys(%labels)) != scalar(@labels)) {
		croak "Duplicate label names in tree - cannot parse";
	}
	
	foreach my $label (sort {length($b) <=> length($a)} keys %labels) {
		$tree_string =~ s/\Q$label\E/$labels{$label}/g;
	}
	my %nodes = flip_hash(\%labels);
	
	my $left_bracket = () = $tree_string =~ /\(/gms;
	my $right_bracket = () = $tree_string =~ /\)/gms;
	if ($left_bracket != $right_bracket) {
		croak "Unequal numbers of left ($left_bracket) and right ($right_bracket) brackets";
	}
	
	my %tree;
	_parse_tree($tree_string, \%tree, \%nodes, 1);
	
	foreach my $prune_node (@$prune_nodes) {
		prune_node(\%tree, $prune_node);
	}
	
	return \%tree;
}

# Print a parsed tree in Newick format
sub print_tree {
	my ($tree, $pretty, $node) = @_;
	my ($separator1, $separator2);
	($separator1, $separator2) = ("\n", "\t") if $pretty;
	my $tree_string = _print_tree($tree, $node, $separator1, $separator2);
	$tree_string .= ";";
	return $tree_string;
}

# Unroot a parsed tree by creating a trifurcation at the root
sub unroot_tree {
	my ($tree) = @_;
	# For convenience we still keep a 'root', we just change it
	# from a bifurcation to a multifurcation. We add one child
	# to the children of the other, and make that the new root,
	# as long as it's an internal node.
	my $children = $$tree{'root'}{'children'};
	if (scalar(@{$children}) == 2) {
		my ($new_root, $new_child);
		if (exists($$tree{$$children[0]}{'internal'})) {
			($new_root, $new_child) = ($$children[0], $$children[1]);
		} elsif (exists($$tree{$$children[1]}{'internal'})) {
			($new_root, $new_child) = ($$children[1], $$children[0]);
		}
		if ($new_root) {
			push(@{$$tree{$new_root}{'children'}}, $new_child);
			if (exists($$tree{$new_root}{'blength'})) {
				$$tree{$new_child}{'blength'} += $$tree{$new_root}{'blength'};
				delete($$tree{$new_root}{'blength'});
			}
			delete($$tree{'root'});
			$$tree{'root'} = $$tree{$new_root};
			$$tree{$new_root}{'name'} = 'root';
		}
	}
	return;
}

# Eliminate multifurcations by adding zero-length branches
sub bifurcate_node {
	my ($tree, $recurse, $node) = @_;
	$recurse = 0 unless $recurse;
	$node = 'root' unless $node;
	
	if (exists($$tree{$node}{'internal'})) {
		my $children = $$tree{$node}{'children'};
		while (scalar(@{$children}) > 2) {
			my $child2 = pop(@$children);
			my $child1 = pop(@$children);
			
			my $new_node = "int_node_bifurc_$child1\_$child2";
			push(@$children, $new_node);
			$$tree{$child1}{'parent'} = $new_node;
			$$tree{$child2}{'parent'} = $new_node;
			$$tree{$new_node}{'internal'} = 1;
			$$tree{$new_node}{'parent'} = $node;
			$$tree{$new_node}{'children'} = [$child1, $child2];
			if (exists($$tree{$child1}{'blength'})) {
				$$tree{$new_node}{'blength'} = 0;
			}
		}
		
		if ($recurse) {
			bifurcate_node($tree, $recurse, $$children[0]);
			bifurcate_node($tree, $recurse, $$children[1]);
		}
	}
	return;
}

sub bifurcate_tree {
	my ($tree) = @_;
	return bifurcate_node($tree, 1);
}

# Delete a node from a parsed tree
sub prune_node {
	my ($tree, $node) = @_;
	
	if (! exists($$tree{$node})) {
		return;
	} elsif (! exists($$tree{$node}{'parent'})) {
		carp "Cannot delete the root node";
		return;
	}
	
	my $parent = $$tree{$node}{'parent'};
	my @siblings;
	foreach my $child (@{$$tree{$parent}{'children'}}) {
		push(@siblings, $child) unless $child eq $node;
	}
	
	if (scalar(@siblings) > 1) {
		$$tree{$parent}{'children'} = \@siblings;
	} else {
		my $sibling = $siblings[0];
		if (exists($$tree{$parent}{'parent'})) {
			if (exists($$tree{$parent}{'blength'})) {
				$$tree{$sibling}{'blength'} += $$tree{$parent}{'blength'};
			}
			
			my $grandparent = $$tree{$parent}{'parent'};
			$$tree{$sibling}{'parent'} = $grandparent;
			
			my $children = join(",", @{$$tree{$grandparent}{'children'}});
			$children =~ s/$parent/$sibling/;
			my @new_children = split(/,/, $children);
			$$tree{$grandparent}{'children'} = \@new_children;
		} else {
			delete($$tree{$sibling}{'parent'});
			delete($$tree{$sibling}{'blength'}) if exists $$tree{$sibling}{'blength'};
			$$tree{'root'} = $$tree{$sibling};
			$$tree{$sibling}{'name'} = $sibling;
		}
		delete($$tree{$parent});
	}
	
	if (exists($$tree{$node}{'internal'})) {
		_prune_node($tree, $node);
	}
	delete($$tree{$node});
	
	return;
}

sub prune_tree {
	my ($tree, $nodes) = @_;
	foreach my $node (@$nodes) {
		prune_node($tree, $node);
	}
}

sub tree_length {
	my ($tree_string) = @_;
	$tree_string =~ s/\n//g;
	my @branches = $tree_string =~ /:\s*([\d\.]+)/gms;
	return sum(@branches);
}

sub leaf_nodes {
	my ($tree_string) = @_;
	$tree_string =~ s/\n//g;
	my @species = $tree_string =~ /[\(,]\s*([\w\.]+|[\'\"][^\'\"]+[\'\"])/gms;
	return wantarray ? @species : \@species;
}

sub relabel {
	my ($tree_string, $mapping) = @_;
	foreach my $id (keys %$mapping) {
		$tree_string =~ s/([\(,\n])$id([^\w\.]+)/$1$$mapping{$id}$2/gms;
	}
	return $tree_string;
}

# Recursive functions that do the grunt-work for exported functions
# have the same name as the relevant function with an underscore prefix.
sub _parse_tree {
	my ($tree_string, $tree, $nodes, $level) = @_;
	
	# Looking for at least two leaves (or processed internal nodes),
	# that are siblings.
	my ($children) = $tree_string =~ /\(([\w,:\.]+,[\w,:\.]+)\)/;
	if ($children) {
		my $name;
		if ($tree_string =~ /\($children\)(\w+)/) {
			$name = $$nodes{$1};
			$tree_string =~ s/\($children\)//;
		} else {
			$name = "int_node_$level";
			$tree_string =~ s/\($children\)/$name/;
		}
		
		my @children = split(/,/, $children);
		foreach my $child (@children) {
			if ($child =~ /:/) {
				my ($blength) = $child =~ /:(\d+\.?\d*)$/;
				$child =~ s/:$blength//;
				$child = $$nodes{$child} if exists $$nodes{$child};
				$$tree{$child}{'blength'} = $blength;
			} else {
				$child = $$nodes{$child} if exists $$nodes{$child};
			}
			$$tree{$child}{'parent'} = $name;
		}
		$$tree{$name}{'children'} = \@children;
		$$tree{$name}{'internal'} = 1;
		
		_parse_tree($tree_string, $tree, $nodes, ++$level);
		
		unless (exists($$tree{'root'})) {
			$$tree{$name}{'name'} = $name;
			$$tree{'root'} = $$tree{$name};
		}
	}
	
	return;
}

sub _print_tree {
	my ($tree, $node, $separator1, $separator2, $level) = @_;
	$node = $$tree{'root'}{'name'} unless $node;
	$separator1 = "" unless $separator1;
	$separator2 = "" unless $separator2;
	$level = 1 unless $level;
	
	my $string = "(".$separator1.($separator2 x $level);
	my $length = 0;
	if (exists($$tree{$node}{'children'})) {
		$length = scalar(@{$$tree{$node}{'children'}});
	}
	for (my $i=0; $i<$length; $i++) {
		my $child = $$tree{$node}{'children'}[$i];
		if (exists($$tree{$child}{'internal'})) {
			$string .= _print_tree($tree, $child, $separator1, $separator2, $level+1);
		} else {
			$string .= $child;
			if (exists($$tree{$child}{'blength'})) {
				$string .= ":".$$tree{$child}{'blength'};
			}
		}
		if ($i != ($length-1)) {
			$string .= ",".$separator1.($separator2 x $level);
		}
	}
	$string .= $separator1.($separator2 x ($level-1)).")";
	$string .= $node unless $node =~ /^(int_node|root)/;
	if (exists($$tree{$node}{'blength'}) && $level > 1) {
		$string .= ":".$$tree{$node}{'blength'};
	}
	return ($string);
}

sub _prune_node {
	my ($tree, $node) = @_;
	
	foreach my $child (@{$$tree{$node}{'children'}}) {
		if (exists($$tree{$child}{'internal'})) {
			_prune_node($tree, $child);
		} else {
			delete($$tree{$child});
		}
	}
	return;
}

# Store children as an array of arrays, rather than
# as text. Which sounds nice, but is hard to work with,
# since the internal nodes aren't labelled. Not exported,
# it's here for future reference.
sub nest_nodes {
	my ($tree, $node) = @_;
	foreach my $child (@{$$tree{$node}{'children'}}) {
		if (exists($$tree{$child}{'internal'})) {
			$child = nest_nodes($tree, $child);
		}
	}
	return($$tree{$node}{'children'});
}

################################################################################

1;

