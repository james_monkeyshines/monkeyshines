package MonkeyShines::Utils;

use warnings;
use strict;
use Carp;

our $VERSION = '0.1';

use POSIX qw (strftime);

use Exporter;
our @ISA = qw(Exporter);
our @EXPORT_OK = qw(
	timestamp_id random_seed
	read_from_file read_from_delim read_from_delim_column save_to_file save_to_delim
	min max sum mean median median_hash mode
	factorial binomial_coeff ln_choose log_parse
	coord_overlap coord_union coord_intersection coord_union_all coord_intersection_all
	random_select random_select_and_remove fisher_yates_shuffle uniquify_list
	simple_union simple_intersect simple_diff list_exists
	flip_hash print_hash
	execute_check
);

=pod

=head1 Name

MonkeyShines::Utils - General purpose utilities.

=head1 Description

MonkeyShines::Utils is a collection of general purpose functions. A lot
probably exist eleswhere, but often it's easier to write a short
function than it is to get them from a bunch of different places. And
it's much easier to tweak/extend the functionality later on.

In many functions I don't bother with lots of checks to see if enough
parameters, of the correct type, have been provided. This is to keep
the functions succinct and readable. You should still get generic error
messages from Perl when you do something wrong, but it's up to you to
work out why it's whingeing about treating a scalar as an arrayref...

=head1 Functions

=head2 Generate Timestamp

=over

=item B<$timestamp_id = timestamp_id([$max_random])>

Return a string consisting of a timestamp and a random number between 0 and
1000 (by default - specify $max_random to use a different upper limit). This
function is useful for generating names for temporary files.

=back

=head2 Generate Random Number

=over

=item B<$seed = random_seed([$source])>

This function uses $source (/dev/random by default) to generate a truly
random number, more specifically a 10-digit integer. The main reason for
doing this is to provide a seed number for a pseudo-random generator,
which is why the function is not called 'random_number'.
This will only work on Unix/Linux/Cygwin.

=back

=head2 File IO

=over

=item B<$data = read_from_file($file)>

Read data from the given file location into a string.

=item B<($data, $cols) = read_from_delim($file[, $delim][, $header][, $cols])>

Read data from a delimited text file and output an arrayref of arrayrefs,
$data, of rows and columns. $delim is tab by default. If $header is true,
then the first row of the file is assumed to have column headings, which are
used as the keys of the hashref $cols, with a value indicating the column
number (zero-based, to allow easy indexing of $data). Alternatively, an
arrayref of column names, $cols, can be provided.

=item B<([$%]data) = read_from_delim_column($file[, $delim][, $header][, $cols][, $col_name])>

Read data from a delimited text file and output a hash(ref) of hashrefs, indexed
first by the entries in a specific column, $col_name (which defaults to the first
column), then by the names of all columns (i.e. including $col_name). $delim is
tab by default. If $header is true, then the first row of the file is assumed to
have column headings, otherwise an arrayref of column names, $cols, is required.

=item B<C<undef> = save_to_file($data, $file[, $append])>

Save $data in the location given by $file; if $append is true, then
append to, rather than overwrite, any existing data in that file.

=item B<C<undef> = save_to_delim($data, $file[, $append][, $delim][, $cols])>

Save an arrayref of arrayrefs, $data, in a delimited format ($delim is tab
by default) in the location given by $file; if $append is true, then
append to, rather than overwrite, any existing data in that file. If an
arrayref of column names, $cols, is given, save them as a header row.

=back

=head2 Numeric Calculations

=over

=item B<$min = min($value1, $value2[, $value3[, ...]])>

=item B<$min = min(@values)>

Find the minimum of two or more values, or the minimum value in an array.

=item B<$max = max($value1, $value2[, $value3[, ...]])>

=item B<$max = max(@values)>

Find the maximum of two or more values, or the maximum value in an array.

=item B<$sum = sum($value1, $value2[, $value3[, ...]])>

=item B<$sum = sum(@values)>

Find the sum of two or more values, or the sum of all values in an array.

=item B<$mean = mean($value1, $value2[, $value3[, ...]])>

=item B<$mean = mean(@values)>

Find the mean of two or more values, or the mean of all values in an
array, to six decimal places.

=item B<$median = median($value1, $value2[, $value3[, ...]])>

=item B<$median = median(@values)>

Find the median of two or more values, or the median of all values in an
array, to six decimal places.

=item B<($median_id, $median_value) = median_hash(%ids)>

Find the median of the values of the hash %ids, and return the
corresponding key and the value itself as $median_id and $median_value,
respectively. If more than one key has the median value, one is randomly
selected as $median_id. If there are an even number of keys, those
corresponding to the two middle values are pooled, and one is randomly
chosen as $median_id, and its associated value is returned as $median_value.

=item B<$mode = mode($value1, $value2[, $value3[, ...]])>

=item B<$mode = mode(@values)>

Find the mode of two or more values, or the mode of all values in an array.
The return value, $mode, is an arrayref, with more than one element if different
values occur equally often.

=item B<$factorial = factorial($num)>

Calculate the factorial of $num. This function uses a very simple recursion
rather than any clever tricks, so don't give it a big number unless you're
in a patient mood.

=item B<$binomial_coeff = binomial_coeff($n, $k)>

Calculate the binomial coefficient for n 'choose' k. This function uses the
above factorial function, so the caveat about big numbers applies here too.

=item B<$ln_choose = ln_choose($n, $k[, $bin_dir])>

Calculate the binomial coefficient for n 'choose' k. This is far better than
the previous function, as it uses the GSL C library to do the calculations (via
a very short C function, stored in the 'MonkeyShines/c' directory).
Since big numbers are often involved, the natural log of the value is returned.
To use this function, you need to have compiled the C code by executing
'MonkeyShines/c/make_choose', which will place the executable file in
'$HOME/bin'. If you edit the makefile to put the executable somewhere that's
not in your path, provide the location with $bin_dir.

=item B<$log = log_parse($num[, $base])>

Calculate the logarithm of a number, to base 10 unless $base is
given. The function parses $num as text, expecting it to be given in
scientific notation, to deal with numbers that are so small that they are
zero as far as Perl and/or a specific machine is concerned. If a regular
number is given, the function just passes it to the Perl log function.

=item B<$union = coord_overlap($v, $w)>

Given two start-stop coordinates, $v and $w, as arrayrefs, work out if they
overlap. Return a number the indicates the nature of the overlap, as follows:
 0 = no overlap
 1 = v entirely within w
 2 = w entirely within v
 3 = end of v overlaps start of w
 4 = start of v overlap end of w

=item B<$union = coord_union($v, $w)>

Given two start-stop coordinates, $v and $w, as arrayrefs, work out if they
overlap. If they do, then return the minimum and maximum values (as an
arrayref). If they don't overlap, return an empty arrayref.

=item B<$intersection = coord_intersection($v, $w)>

Given two start-stop coordinates, $v and $w, as arrayrefs, work out if they
overlap. If they do, then return the coordiantes of the overlap (as an
arrayref). If they don't overlap, return an empty arrayref.

=item B<C<undef> = coord_union_all($coords)>

Given an arrayref of start-stop coordinates, $coords, each element of which is
an arrayref, work out which subsets overlap. The function operates recursively
on the original $coords variable, merging the overlapping coordinates until
all that's left are arrayrefs that describe non-overlapping coordinates.
So the number of elements of $coords will be unchanged if none of the
elements overlap, and will be somewhere between that number and 1 otherwise,
depending on the degree of overlap.

=item B<C<undef> = coord_intersection_all($coords)>

Given an arrayref of start-stop coordinates, $coords, each element of which is
an arrayref, work out the coordinates at which _all_ elements overlap. The
function operates recursively on the original $coords variable, until it
contains either one value (ie the intersection) or an empty arrayref.

=back

=head2 Array Manipulation

=over

=item B<$element = random_select($array)>

Return a randomly selected element of an arrayref, $array.

=item B<$element = random_select_and_remove($array)>

Return a randomly selected element of an arrayref, $array,
while also removing that element from $array.

=item B<[$@]shuffled = fisher_yates_shuffle($array)>

Randomise an arrayref, $array, using the Fisher-Yates method; $array is
unchanged, and a new array(ref) is returned.

=item B<[$@]unique = uniquify_list($array)>

Select the unique elements from an arrayref, $array, and return them as
a new array(ref).

=item B<[$@]union = simple_union($array1, $array2)>

Return a de-duplicated list of the elements of $array1 and $array2.

=item B<[$@]intersect = simple_intersect($array1, $array2)>

Return a de-duplicated list of the elements that $array1 and $array2
have in common. The returned array(ref) will be empty rather than
undefined if there is no intersection.

=item B<[$@]diff = simple_diff($array1, $array2)>

Return the elements of the arrayref $array1 that are not present in $array2.
The returned array(ref) will be empty rather than undefined if there are no
differences. Note that elements that are in $array2 but not in $array1 are
_not_ returned.

=item B<$exists = list_exists($element, $array)>

Test whether the string $element is contained in the arrayref $array.

=back

=head2 Hash Manipulation

=over

=item B<[$%]flip = flip_hash($hash[, $arrays])>

Switch the keys and values in a hash, returning a hash(ref). If
multiple keys have the same value in the original hash, an arrayref of the
keys will be created as the new value in the flipped hash. The function
is forced to always create arrayref values if $arrays is true.

=item B<$hash_contents = print_hash($hash[, $delim_1][, $delim_2)>

Return the key-value pairs of a hashref as a string, with the key and value
separately by $delim_1 (default ':'), and pairs separated by $delim_2
(default ', ').

=back

=head2 Script Execution

=over

=item B<$out = execute_check($execute, $check[, $check_success][, $program][, $fatal])>

Execute shell scripts/programs, and check for errors. $execute should
consist of a program name and any parameters, but should not redirect either
the standard error or output streams, as the function will do this and place
both in the $out return value. To test whether a program executed without
errors, the $check parameter should contain text that appears in the program's
error message; if this is found in the output, the error is passed on to the
user. Alternatively, text that appears in the valid output of the program can
be provided as the $check value, if $check_success is true. The $program parameter
is used to report which program failed to the user. If the $fatal parameter is
true then execution ceases after the error is reported, otherwise it will
continue after displaying a warning (default is the latter).

=back

=head1 Author

James Allen
(james@monkeyshines.co.uk)

=head1 Copyright

Copyright 2009-2012 James Allen; University of Manchester

=head1 Licence

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program. If not, see <http://www.gnu.org/licenses/>.

=cut

################################################################################
# GENERATE TIMESTAMP
################################################################################
sub timestamp_id {
	my ($max_random) = @_;
	$max_random = 1000 unless $max_random;
	
	my $timestamp = strftime("%m%d%H%M%S", localtime);
	return "$timestamp\_".int(rand($max_random));
}
################################################################################

################################################################################
# RANDOM NUMBERS
################################################################################
# Generate a 10-digit random number.
sub random_seed {
	my ($source) = @_;
	$source = "/dev/random" unless $source;
	my $seed = "";
	# Sometimes the '10' digit number is shorter, because there are
	# effectively leading zeroes; we ignore those until we get 10-digits,
	# so that the script returns numbers of consistent length.
	until (length($seed) == 10) {
		# Random bytes are returned, but we want integers, so
		# convert them from octal with od.
		$seed = `od -An -N4 -tu4 $source`;
		$seed =~ s/\s//g;
	}
	return $seed;
}
################################################################################

################################################################################
# FILE IO
################################################################################
sub read_from_file {
	my ($file) = @_;
	my $data;
	open(FILE, $file) || croak "Cannot open file '$file'";
	while (my $line = <FILE>) {
		$data .= $line;
	}
	close(FILE);
	return $data;
}

sub read_from_delim {
	my ($file, $delim, $header, $cols) = @_;
	my (@data, %cols);
	$delim = "\t" unless $delim;
	open(FILE, $file) || croak "Cannot open file '$file'";
	if ($header) {
		my $line = <FILE>;
		chomp($line);
		my @cols = split(/$delim/, $line);
		$cols{$cols[$_]} = $_ for (0..(scalar(@cols)-1));
	}
	if ($cols) {
		$cols{$$cols[$_]} = $_ for (0..(scalar(@$cols)-1));
	}
	while (my $line = <FILE>) {
		chomp($line);
		my @row = split(/$delim/, $line);
		next unless @row;
		push @data, \@row;
	}
	close(FILE);
	return \@data, \%cols;
}

sub read_from_delim_column {
	my ($file, $delim, $header, $cols, $col_name) = @_;
	unless ($header || $cols) {
		croak "The file must have a header row, if column names are not given";
	}
	
	my (%data);
	my ($data, $columns) = read_from_delim($file, $delim, $header, $cols);
	unless ($col_name) {
		my $flipped_cols = flip_hash($columns);
		$col_name = $$flipped_cols{0};
	}
	foreach my $row (@$data) {
		my $row_name = $$row[$$columns{$col_name}];
		foreach my $col (keys %$columns) {
			next if $col eq $row_name;
			croak "Non-unique row name '$row_name'" if exists($data{$row_name}{$col});
			if (defined $$row[$$columns{$col}]) {
				$data{$row_name}{$col} = $$row[$$columns{$col}];
			} else {
				$data{$row_name}{$col} = '';
			}
		}
	}
	return wantarray ? %data : \%data;
}

sub save_to_file {
	my ($data, $file, $append) = @_;
	$append = $append ? ">" : "";
	open(FILE, ">$append$file") || croak "Cannot open file '$file'";
	print FILE $data;
	close(FILE);
	return;
}

sub save_to_delim {
	my ($data, $file, $append, $delim, $cols) = @_;
	$append = $append ? ">" : "";
	$delim = "\t" unless $delim;
	open(FILE, ">$append$file") || croak "Cannot open file '$file'";
	if ($cols) {
		print FILE join($delim, @$cols)."\n";
	}
	foreach my $row (@$data) {
		print FILE join($delim, @$row)."\n";
	}
	close(FILE);
	return;
}
################################################################################

################################################################################
# NUMERIC CALCULATIONS
################################################################################
sub min {
	my @values = @_;
	my @sorted = sort {$a <=> $b} @values;
	return $sorted[0];
}

sub max {
	my @values = @_;
	my @sorted = sort {$b <=> $a} @values;
	return $sorted[0];
}

sub sum {
	my @values = @_;
	my $sum;
	$sum += $_ for @values;
	return $sum;
}

sub mean {
	my @values = @_;
	return sprintf("%.6f", sum(@values)/scalar(@values));
}

sub median {
	my @values = @_;
	my @sorted = sort {$a <=> $b} @values;
	if ((@sorted % 2) == 1) {
		return sprintf("%.6f", $sorted[(@sorted-1)/2]);
    } else {
        return sprintf("%.6f", ( $sorted[(@sorted/2)-1] + $sorted[@sorted/2] )/2);
    }
}

sub median_hash {
	my %values = @_;
	my @sorted = sort {$a <=> $b} values(%values);
	my %flipped = flip_hash(\%values, 1);
	my ($median_id, $median_value);
	if ((@sorted % 2) == 1) {
		$median_value = $sorted[(@sorted-1)/2];
		$median_id = random_select($flipped{$median_value});
    } else {
		my @median_ids = (	@{$flipped{$sorted[(@sorted/2)-1]}},
							@{$flipped{$sorted[@sorted/2]}});
		$median_id = random_select(\@median_ids);
		$median_value = $values{$median_id};
    }
	return ($median_id, $median_value);
}

sub mode {
	my @values = @_;
	my %mode;
	$mode{$_}++ for @values;
	my %flipped = flip_hash(\%mode, 1);
	return $flipped{max(keys(%flipped))};
}

sub factorial {
	my ($num) = @_;
	return $num > 1 ? $num*factorial($num-1) : 1;
}

sub binomial_coeff {
	my ($n, $k) = @_;
	return factorial($n)/(factorial($n-$k)*factorial($k));
}

sub ln_choose {
	my ($n, $k, $bin_dir) = @_;
	$bin_dir = "" unless $bin_dir;
	my $lnchoose = "$bin_dir"."lnchoose";
	return `$lnchoose $n $k`;
}

sub log_parse {
	my ($num, $base) = @_;
	$base = 10 unless $base;
	my $log;
	if ($num =~ /e/i) {
		my ($sig, $exp) = $num =~ /^(.+)e(.+)$/i;
		$log = (log($sig)/log($base))+$exp;
	} else {
		$log = log($num)/log($base);
	}
	return $log;
}

sub coord_overlap {
	my ($v, $w) = @_;
	my $overlap = 0;
	my ($v_start, $v_stop) = sort @$v;
	my ($w_start, $w_stop) = sort @$w;
	if ($v_start >= $w_start && $v_stop <= $w_stop) {
		$overlap = 1;
	} elsif ($v_start <= $w_start && $v_stop >= $w_stop) {
		$overlap = 2;
	} elsif ($v_stop >= $w_start && $v_stop <= $w_stop) {
		$overlap = 3;
	} elsif ($v_start >= $w_start && $v_start <= $w_stop) {
		$overlap = 4;
	}
	return $overlap;
}

sub coord_union {
	my ($v, $w) = @_;
	my $union;
	
	unless (scalar(@$v) == 2 && scalar(@$w) == 2) {
		croak "Two-dimensional co-ordinates only, please.";
	}
	# Sort now to make later calcs simpler.
	my (@v, @w);
	if ($$v[0] <= $$w[0]) {
		@v = sort {$a <=> $b} @$v;
		@w = sort {$a <=> $b} @$w;
	} else {
		@v = sort {$a <=> $b} @$w;
		@w = sort {$a <=> $b} @$v;
	}
	
	# As we've set it up so that the first element of @v is less
	# than that of @w, we only have three possible scenarios.
	# 1) No overlap, with @v 'to the left of' @w.
	# 2) A partial overlap, with @v straddling the first element of @w.
	# 3) Complete overlap, with @w sitting wholly within @v.
	if ($v[1] < $w[0]) {
		$union = undef;
	} elsif ($v[0] <= $w[0] && $v[1] <= $w[1]) {
		$union = [$v[0], $w[1]];
	} elsif ($v[0] <= $w[0] && $v[1] >= $w[1]) {
		$union = \@v;
	} else {
		croak "Something's gone wrong...";
	}
	
	return $union;
}

sub coord_intersection {
	my ($v, $w) = @_;
	my $intersection;
	
	unless (scalar(@$v) == 2 && scalar(@$w) == 2) {
		croak "Two-dimensional co-ordinates only, please.";
	}
	# Sort now to make later calcs simpler.
	my (@v, @w);
	if ($$v[0] <= $$w[0]) {
		@v = sort {$a <=> $b} @$v;
		@w = sort {$a <=> $b} @$w;
	} else {
		@v = sort {$a <=> $b} @$w;
		@w = sort {$a <=> $b} @$v;
	}
	
	# As we've set it up so that the first element of @v is less
	# than that of @w, we only have three possible scenarios.
	# 1) No overlap, with @v 'to the left of' @w.
	# 2) A partial overlap, with @v straddling the first element of @w.
	# 3) Complete overlap, with @w sitting wholly within @v.
	if ($v[1] < $w[0]) {
		$intersection = [];
	} elsif ($v[0] <= $w[0] && $v[1] <= $w[1]) {
		$intersection = [$w[0], $v[1]];
	} elsif ($v[0] <= $w[0] && $v[1] >= $w[1]) {
		$intersection = \@w;
	} else {
		croak "Something's gone wrong...";
	}
	
	return $intersection;
}

sub coord_union_all {
	my ($union_all, $disjoint) = @_;
	$disjoint = 0 unless defined $disjoint;
	# The disjoint parameter is used to avoid redundant recursions.
	
	if (scalar(@$union_all) > $disjoint) {
		# Take the first element and start comparing it to each of
		# the others in turn. We stop if we find an overlap.
		my $v = shift @$union_all;
		my $v_disjoint = 1;
		my $stop = scalar(@$union_all) - 1 - $disjoint;
		foreach my $w (@$union_all[0..$stop]) {
			my $union = coord_union($v, $w);
			if (defined($union)) {
				$w = $union;
				$v_disjoint = 0;
				last;
			}
		}
		
		# If something has no overlaps at all, stick it on
		# the end of the list and increment a counter so we
		# know to ignore it on subsequent recursions.
		if ($v_disjoint == 1) {
			push @$union_all, $v;
			$disjoint++;
		}
		
		coord_union_all($union_all, $disjoint);
	}
}

sub coord_intersection_all {
	my ($intersection_all) = @_;
	
	if (scalar(@$intersection_all) > 1) {
		# Take the first element and start comparing it to each of
		# the others in turn. Then take those intersections...
		my $v = shift @$intersection_all;
		my $v_disjoint = 0;
		foreach my $w (@$intersection_all) {
			my $intersection = coord_intersection($v, $w);
			if (scalar(@$intersection) > 0) {
				$w = $intersection;
			} else {
				@$intersection_all = ();
				$v_disjoint = 1;
				last;
			}
		}
		
		unless ($v_disjoint) {
			coord_intersection_all($intersection_all);
		}
	}
}
################################################################################

################################################################################
# ARRAY MANIPULATION
################################################################################
sub random_select {
	my ($array) = @_;
	return $$array[int(rand(scalar(@$array)))];
}

sub random_select_and_remove {
	my ($array) = @_;
	my $i = int(rand(scalar(@$array)));
	my $element = $$array[$i];
	splice(@$array, $i, 1);
	return $element;
}

sub fisher_yates_shuffle {
    my ($array) = @_;
	my @shuffled = @$array;
    for (my $i = @shuffled; --$i;) {
        my $j = int rand ($i+1);
        next if $i == $j;
        @shuffled[$i,$j] = @shuffled[$j,$i];
    }
	return wantarray ? @shuffled : \@shuffled;
}

sub uniquify_list {
	my ($array) = @_;
	my %seen;
	my @unique = ();
	foreach my $i (@$array) {
		push(@unique, $i) unless exists $seen{$i};
		$seen{$i}++;
	}
	return wantarray ? @unique : \@unique;
}

sub simple_union {
	my ($array1, $array2) = @_;
	return uniquify_list([@$array1, @$array2]);
}

sub simple_intersect {
	my ($array1, $array2) = @_;
	my %array2;
	@array2{@$array2} = 0;
	my @intersect = ();
	foreach my $i (@$array1) {
		push(@intersect, $i) if exists $array2{$i};
	}
	return uniquify_list(\@intersect);
}

sub simple_diff {
	my ($array1, $array2) = @_;
	my %seen;
	my @diff = ();
	@seen{@$array2} = ();
	foreach my $i (@$array1) {
    	push(@diff, $i) unless exists $seen{$i};
	}
	return wantarray ? @diff : \@diff;
}

sub list_exists {
	my ($element, $array) = @_;
	my %hash;
	@hash{@$array} = 0;
	return exists($hash{$element});
}
################################################################################

################################################################################
# HASH MANIPULATION
################################################################################
sub flip_hash {
	my ($hash, $arrays) = @_;
	my (%flip);
	foreach my $key (sort keys %$hash) {
		my $value = $$hash{$key};
		if (exists($flip{$value})) {
			if (!$arrays) {
				foreach my $flip_key (sort keys %flip) {
					$flip{$flip_key} = [$flip{$flip_key}];
				}
				$arrays = 1;
			}
			push(@{$flip{$value}}, $key);
		} else {
			$flip{$value} = $arrays ? [$key] : $key;
		}
	}
	return wantarray ? %flip : \%flip;
}

sub print_hash {
	my ($hash, $delim_1, $delim_2) = @_;
	$delim_1 = ":" unless $delim_1;
	$delim_2 = ", " unless $delim_2;
	my @pairs;
	foreach my $k (sort keys %$hash) {
		push @pairs, "$k$delim_1".$$hash{$k};
	}
	return join($delim_2, @pairs);
}
################################################################################

################################################################################
# SCRIPT EXECUTION
################################################################################
sub execute_check {
	my ($execute, $check, $check_success, $program, $fatal) = @_;
	$check_success = 0 unless $check_success;
	($program) = $execute =~ /^(\S)/ unless $program;
	$fatal = 0 unless $fatal;
	
	my $out = `$execute 2>&1`;
	if (!defined($out)) {
		if ($fatal) {
			croak "Aborting due to $program errors: failed to execute '$execute'";
		} else {
			carp "Continuing execution despite $program errors: failed to execute '$execute'";
		}
	}
	my $error_condition;
	if ($check_success) {
		$error_condition = $out !~ /$check/gms;
	} else {
		$error_condition = $out =~ /$check/gms;
	}
	if ($error_condition) {
		print STDERR "\n$program Output - Start\n";
		print STDERR "$out\n";
		print STDERR "$program Output - End\n\n";
		if ($fatal) {
			croak "Aborting due to $program errors: tried to execute '$execute'";
		} else {
			carp "Continuing execution despite $program errors: tried to execute '$execute'";
		}
	}
	return $out;
}
################################################################################

1;

