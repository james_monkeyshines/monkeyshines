#!/usr/bin/perl
use strict;
use warnings;

# tree.t
# James Allen
# University of Manchester
# $Date: 2012-02-21 17:12:37 +0000 (Tue, 21 Feb 2012) $
# SVN: $Id: tree.t 855 2012-02-21 17:12:37Z Julio $

use Test::More tests => 8;

BEGIN { use_ok('MonkeyShines::Tree', qw(parse_tree print_tree unroot_tree bifurcate_tree tree_length leaf_nodes)); }

my $tree_string = '((((Human:0.01,Chimp:0.01):0.005,Gorilla:0.01):0.02,Orangutan:0.1)Apes:0.3,(Macaque:0.2,Baboon:0.25)Monkeys:0.5);';
is(print_tree(parse_tree($tree_string)), $tree_string, 'Parsing then printing gives original string');
is(tree_length(print_tree(parse_tree($tree_string), 0, 'Apes')), 0.155, 'Length of a subtree');
my @leaf_nodes = leaf_nodes($tree_string);
is(join(",", sort(@leaf_nodes)), 'Baboon,Chimp,Gorilla,Human,Macaque,Orangutan', 'Leaf nodes');

my $pruned_tree_string = '((Human:0.01,Chimp:0.01):0.025,Orangutan:0.1)Apes;';
is(print_tree(parse_tree($tree_string, ['Gorilla', 'Monkeys'])), $pruned_tree_string, 'Prune leaf and internal nodes');

my $unrooted_tree_string = '(((Human:0.01,Chimp:0.01):0.005,Gorilla:0.01):0.02,Orangutan:0.1,(Macaque:0.2,Baboon:0.25)Monkeys:0.8);';
my $tree = parse_tree($tree_string);
unroot_tree($tree);
is(print_tree($tree), $unrooted_tree_string, 'Unroot bifurcating tree');

my $multifurcating = '(((Human:0.01,Chimp:0.01,Gorilla:0.01):0.02,Orangutan:0.1)Apes:0.3,(Macaque:0.2,Baboon:0.25,Marmoset:0.4)Monkeys:0.5)';
my $bifurcating = '(((Human:0.01,(Chimp:0.01,Gorilla:0.01):0):0.02,Orangutan:0.1)Apes:0.3,(Macaque:0.2,(Baboon:0.25,Marmoset:0.4):0)Monkeys:0.5);';
$tree = parse_tree($multifurcating);
bifurcate_tree($tree);
is(print_tree($tree), $bifurcating, 'Add zero-length branches');

my $big_tree_string = "(Edentata:55, (((Orycteropus:12, Trichechus:43):1, (Procavia:29, (Elephas:18, Loxodonta:5):55):10):15, (((Chiroptera:27, (Tupaia:43, ((Talpa:24, (Suncus:24, Erinaceus:58):6):4, (Manis:5, ((Felis:13, Leo:7):32, ((Canis:37, Ursidae:12):4, ((Phocidae:19, Zalophus:17):7, (Procyonidae:12, Mustelidae:22):9):5):17):13):3):10):6):8, (((Lemuridae:46, (Galago:16, Nycticebus:27):10):8, (Tarsius:15, ((Cebus:10, (Atelinae:5, (Aotus:6, Callithrix:9):3):3):18, ((Hylobates:6, (Pongo:1, (Gorilla:2, (Homo:22, Pan:1):2):6):2):5, (Presbytis:1, (Cercopithecus:1, (Erythrocebus:0, ('Macaca fascicularis':2, (\"Macaca mulatta\":5, 'Macaca fuscata':0):1, (Theropithecus:2, Papio:4):15):2):1):8):6):9):22):10):13, ((Ochotona:7, Oryctolagus:54):4, (Caviomorpha:107, (Spermophilus:29, (Spalax:23, ((Rattus:71, Mus:19):15, (Ondatra:27, Mesocricetus:32):27):20):8):12):15):11):9):12, ((Sus:50, ((Lama:10, Camelus:24):31, (Hippopotamus:31, (((Ovis:9, Capra:8):19, ((Antilocapra:13, Giraffa:14):7, (Cervus:8, Alces:9):11):2):9, (Tragelaphinae:6, ('Bos grunniens':6, (Bison:5, 'Bos taurus':15):7):11):6):47):10):19):18, ((('Equus (Asinus)':6, 'Equus caballus':31):24, (Tapirus:19, Rhinocerotidae:23):11):22, ((Phocoena:4, Tursiops:17):5, (Balaenoptera:36, Eschrichtius:2):8):29):12):12):16):55);";
(my $pruned_big_tree_string = $big_tree_string) =~ s/,\s+/,/g;
$pruned_big_tree_string =~ s/\Q(Elephas:18,Loxodonta:5):55\E/Elephas:73/;
my $big_tree = parse_tree($big_tree_string, ['Loxodonta']);
is(print_tree($big_tree), $pruned_big_tree_string, 'Parsing then printing gives original string, minus extra spaces and pruned species');

