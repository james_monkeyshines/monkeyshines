#!/usr/bin/perl
use strict;
use warnings;

# utils.t
# James Allen
# University of Manchester
# $Date: 2012-02-21 17:12:37 +0000 (Tue, 21 Feb 2012) $
# SVN: $Id: utils.t 855 2012-02-21 17:12:37Z Julio $

use Test::More tests => 14;

BEGIN { use_ok('MonkeyShines::Utils',
qw( timestamp_id random_seed
	read_from_file read_from_delim read_from_delim_column save_to_file save_to_delim
	min max sum mean median median_hash mode
	factorial binomial_coeff ln_choose log_parse
	coord_overlap coord_union coord_intersection coord_union_all coord_intersection_all
	random_select random_select_and_remove fisher_yates_shuffle uniquify_list
	simple_union simple_intersect simple_diff list_exists
	flip_hash print_hash
	execute_check )); }

# Timestamp and Random Seed
my $timestamp = timestamp_id();
like($timestamp, '/^\d{10}_\d+$/', 'timestamp_id: correct format');

my $random_seed = random_seed();
like($random_seed, '/^\d{10}$/', 'random_seed: correct format');

# File IO
my $sample_text_file = 'utils_sample_text_file.txt';
my @sample_data = (
	['Monkey', 'Diet', 'Cute Factor'],
	['Baboon', 'Omnivore', 2],
	['Marmoset', 'Insectivore', 6],
	['Siamang', 'Herbivore', 4],
	['Tarsier', 'Insectivore', 8]
);
my $sample_string;
foreach my $row (@sample_data) {
	$sample_string .= join("\t", @{$row})."\n";
}

# File IO: Reading
is(read_from_file($sample_text_file), $sample_string, 'read_from_file: file read');

my ($data, $cols);
($data, undef) = read_from_delim($sample_text_file);
is_deeply($data, \@sample_data, 'read_from_file_delim: file read, with defaults');

($data, $cols) = read_from_delim($sample_text_file, undef, undef, ['first', 'second', 'third']);
is($$cols{'first'}, 0, 'read_from_file_delim: column read, with column list');
is_deeply($data, \@sample_data, 'read_from_file_delim: data read, with column list');

my $sample_cols = shift @sample_data;
my %sample_cols = ('Monkey' => 0, 'Diet' => 1, 'Cute Factor' => 2);
($data, $cols) = read_from_delim($sample_text_file, "\t", 1);
is_deeply($cols, \%sample_cols, 'read_from_file_delim: columns read, with header flag');
is_deeply($data, \@sample_data, 'read_from_file_delim: data read, with header flag');

my %sample_data_by_col = (
	'Baboon' => {'Monkey' => 'Baboon', 'Diet' => 'Omnivore', 'Cute Factor' => 2},
	'Marmoset' => {'Monkey' => 'Marmoset', 'Diet' => 'Insectivore', 'Cute Factor' => 6},
	'Siamang' => {'Monkey' => 'Siamang', 'Diet' => 'Herbivore', 'Cute Factor' => 4},
	'Tarsier' => {'Monkey' => 'Tarsier', 'Diet' => 'Insectivore', 'Cute Factor' => 8},
);
$data = read_from_delim_column($sample_text_file, undef, 1);
is_deeply($data, \%sample_data_by_col, 'read_from_file_delim_column: data read, with header flag');

# File IO: Writing
SKIP: {
	skip 'Test directory not writable, cannot test "save" functions', 4 unless -w '.';
	
	my $sample_save_file = 'utils_sample_save_file.txt';
	save_to_file($sample_string, $sample_save_file);
	is(read_from_file($sample_save_file), $sample_string, 'save_to_file: file saved');
	save_to_file("Human\tOmnivore\tVariable", $sample_save_file, 1);
	is(read_from_file($sample_save_file), $sample_string."Human\tOmnivore\tVariable", 'save_to_file: file appended');
	
	save_to_delim(\@sample_data, $sample_save_file, 0, ",", $sample_cols);
	my ($saved_data, undef) = read_from_delim($sample_save_file, ",", 1);
	is_deeply($saved_data, \@sample_data, 'save_to_delim: file saved with column names');
	
	save_to_delim(\@sample_data, $sample_save_file);
	($saved_data, undef) = read_from_delim($sample_save_file);
	is_deeply($saved_data, \@sample_data, 'save_to_delim: file saved without column names');
	
	unlink($sample_save_file);
}

# Numerical Calculations


