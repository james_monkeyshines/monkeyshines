#!/usr/bin/perl
use strict;
use warnings;
use Carp;

=pod

=head1 Name

rna_gene_results.pl dir data_file

=head1 Description

Take the results of RNA gene prediction programs and combine them into a single file.

=head1 Author

James Allen
(james@monkeyshines.co.uk)

=head1 Copyright

Copyright 2012 James Allen; University of Manchester

=head1 Licence

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program. If not, see <http://www.gnu.org/licenses/>.

=cut

use File::Basename qw(basename);

use MonkeyShines::Tree qw (tree_length);
use MonkeyShines::Utils qw (read_from_delim read_from_file save_to_file);

################################################################################
# Initialise Parameters - Start
my $dir = $ARGV[0];
croak "Directory must be specified" unless $dir;
croak "Directory '$dir' does not exist" unless -e $dir;

my $data_file = $ARGV[1];
croak "Data file must be specified" unless $data_file;
croak "File '$data_file' does not exist" unless -e $data_file;

my $pred_dir = "$dir/prediction";
my $output_dir = "$dir/output";

my $bionj = 1;
my $bionj_txt = $bionj ? '.bionj' : '';
# Initialise Parameters - End
################################################################################

################################################################################
# Output Files - Start
my $output_file = "$output_dir/prediction$bionj_txt\_results.txt";
my @output_cols = (
	'ID', 'Chr', 'Location', 'Dataset', 'Flanking', 'Randomised', 'Genic',
	'Software', 'Executed', 'RNA Gene',
	'TP', 'TN', 'FP', 'FN', 'Sensitivity', 'Specificity', 'MCC',
#	'Pattern', 'Inferred'
);
save_to_file(join("\t", @output_cols), $output_file);

my $boundaries_file = "$output_dir/prediction$bionj_txt\_boundaries.txt";
my @boundaries_cols = (
	'Flanking', 'Dataset', 'Randomised', 'Genic', 'Software',
	'Direction', 'Overlaps RNA', 'Position', 'Genic', 'Inferred', 'Total'
);
save_to_file(join("\t", @boundaries_cols), $boundaries_file);
# Output Files - End
################################################################################

################################################################################
# Collate Prediction Results - Start
my %boundaries;
my ($data, $cols) = read_from_delim($data_file, "\t", 1);
foreach my $line (@$data) {
	my $id = $$line[$$cols{'ID'}];
	my $chr = $$line[$$cols{'Chr'}];
	my $location = $$line[$$cols{'Location'}];
	my $dataset = $$line[$$cols{'Dataset'}];
	my $genic = $$line[$$cols{'Gene.Position'}] ? 1 : 0;
	
	my $results_dir = "$pred_dir$bionj_txt/$id\_$chr\_$location\_$dataset/results";
	if (-e $results_dir) {
		my $results_file = "$results_dir/results.txt";
		if (-e $results_file) {
			my $output;
			my $prefix = "$id\t$chr\t$location\t$dataset";
			my ($results, $col) = read_from_delim($results_file, "\t", 1);
			foreach my $line (@$results) {
				$output .= "\n$prefix";
				$output .= "\t".$$line[$$col{'Flanking'}];
				$output .= "\t".$$line[$$col{'Randomised'}];
				$output .= "\t".$genic;
				$output .= "\t".$$line[$$col{'Software'}];
				$output .= "\t".$$line[$$col{'Executed'}];
				$output .= "\t".$$line[$$col{'RNA Gene'}];
				$output .= "\t".$$line[$$col{'TP'}];
				$output .= "\t".$$line[$$col{'TN'}];
				$output .= "\t".$$line[$$col{'FP'}];
				$output .= "\t".$$line[$$col{'FN'}];
				$output .= "\t".$$line[$$col{'Sensitivity'}];
				$output .= "\t".$$line[$$col{'Specificity'}];
				$output .= "\t".$$line[$$col{'MCC'}];
#				$output .= "\t".$$line[$$col{'Pattern'}];
#				$output .= "\t".$$line[$$col{'Inferred'}];
			}
			save_to_file($output, $output_file, 'append');
		} else {
			carp "Results file '$results_file' does not exist";
		}
		next;
		my $boundaries_file = "$results_dir/boundaries.txt";
		if (-e $boundaries_file) {
			my ($results, $col) = read_from_delim($boundaries_file, "\t", 1);
			foreach my $line (@$results) {
				my $fl = $$line[$$col{'Flanking'}];
				my $rand = $$line[$$col{'Randomised'}];
				my $sw = $$line[$$col{'Software'}];
				my $dir = $$line[$$col{'Direction'}];
				my $ol = $$line[$$col{'Overlaps RNA'}];
				my $pos = $$line[$$col{'Position'}];
				my $inf = $$line[$$col{'Inferred'}];
				
				$boundaries{$fl}{$dataset}{$rand}{$sw}{$dir}{$ol}{$pos}{$genic}{'Inferred'} += $inf;
				$boundaries{$fl}{$dataset}{$rand}{$sw}{$dir}{$ol}{$pos}{$genic}{'Total'}++;
			}
		} else {
			carp "Boundaries file '$boundaries_file' does not exist";
		}
	} else {
		carp "Results directory '$results_dir' does not exist";
	}
}
exit;
my $boundaries;
foreach my $fl (sort keys %boundaries) {
	foreach my $dataset (sort keys %{$boundaries{$fl}}) {
		foreach my $rand (sort keys %{$boundaries{$fl}{$dataset}}) {
			foreach my $sw (sort keys %{$boundaries{$fl}{$dataset}{$rand}}) {
				foreach my $dir (sort keys %{$boundaries{$fl}{$dataset}{$rand}{$sw}}) {
					foreach my $ol (sort keys %{$boundaries{$fl}{$dataset}{$rand}{$sw}{$dir}}) {
						my %hash = %{$boundaries{$fl}{$dataset}{$rand}{$sw}{$dir}{$ol}};
						foreach my $pos (sort { $hash{$a} <=> $hash{$b} } keys %hash) {
							foreach my $genic (sort keys %{$hash{$pos}}) {
								$boundaries .= "\n$fl\t$dataset\t$rand\t$sw\t$dir\t$ol\t$pos\t$genic";
								$boundaries .= "\t".$boundaries{$fl}{$dataset}{$rand}{$sw}{$dir}{$ol}{$pos}{$genic}{'Inferred'};
								$boundaries .= "\t".$boundaries{$fl}{$dataset}{$rand}{$sw}{$dir}{$ol}{$pos}{$genic}{'Total'};
							}
						}
					}
				}
			}
		}
	}
}
save_to_file($boundaries, $boundaries_file, 'append');
# Collate Prediction Results - End
################################################################################

