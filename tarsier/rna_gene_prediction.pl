#!/usr/bin/perl
use strict;
use warnings;
use Carp;

=pod

=head1 Name

rna_gene_prediction.pl

=head1 Description

Execute RNA gene scans for an alignment, using EvoFold and RNAz.
The complete results are saved for future reference, in an output
sub-directory, but the results returned by this script
are those that deemed most reliable, according to each program's
definition of 'reliable'.

=head1 Author

James Allen
(james@monkeyshines.co.uk)

=head1 Copyright

Copyright 2012 James Allen; University of Manchester

=head1 Licence

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program. If not, see <http://www.gnu.org/licenses/>.

=cut

use File::Basename qw(basename);
use File::Path qw(make_path);
use Getopt::Long;

use MonkeyShines::Sequence qw (from_fasta from_maf flanking_lengths);
use MonkeyShines::Tarsier qw (
	evofold rnaz2 evofold_inferred rnaz_inferred inferred_accuracy
);
use MonkeyShines::Utils qw (read_from_file read_from_delim save_to_file);

################################################################################
# Arguments - Start
my(	$help, $verbose, $alignment_file, $tree_file, $out_dir);

GetOptions(
	'help' => \$help,
	'verbose' => \$verbose,
	'alignment_file=s' => \$alignment_file,
	'tree_file=s' => \$tree_file,
	'out_dir:s' => \$out_dir
);

sub usage {
  print qq~

rna_gene_prediction.pl: Finding RNA genes in a genomic alignment

This script executes EvoFold and RNAz for a genomic alignment.

* Mandatory parameters:
  --a[lignment_file] <file>
  --t[ree_file] <file>
  Two input files are required, an alignment in MAF format, and a tree
  in Newick format.

* Optional parameters:
  --o[ut_dir] <dir>
  If an output directory is not specified, results are saved in the
  current directory. The full output and 'reliable' results are saved
  in subdirectories, 'output' and 'results', respectively.

* Optional flags:
  --v[erbose]
  Display informational messages about what the script is doing.

~;
  print 'Author: James Allen (james@monkeyshines.co.uk)'."\n\n";
  print "Usage: $0 --align <file> -tree <file> [options]\n";
  print qq~
  Please provide values for:
    --a[lignment_file]  Fasta-format alignment
    --t[ree_file]       Newick-format tree
  
  Optional parameters:
    --o[ut_dir]  default '.'

  Optional flags:
    --h[elp]     View this help page
    --v[erbose]  Display progress on screen
  
  ~;
  print "\n";
  exit;
}

usage() if (defined $help);

unless ( $alignment_file && $tree_file ) {
	usage;
}

$verbose = 0 unless $verbose;

my %software = (
	'rnaz2' => 'RNAz',
	'rnaz2_max_sp' => 'RNAz (Max. Species)',
	'evofold' => 'EvoFold',
);
# Arguments - End
################################################################################

################################################################################
# Sort Out Files and Directories - Start
if (!-e $alignment_file) {
	error_msg("Sequence file '$alignment_file' does not exist.");
}
if (!-e $tree_file) {
	error_msg("Tree file '$tree_file' does not exist.");
}

$out_dir = '.' unless $out_dir;
if (!-e $out_dir) {
	make_path($out_dir);
	print "Writing output to new directory, '$out_dir'\n" if $verbose;
} else {
	print "Writing output to existing directory, '$out_dir'\n" if $verbose;
}
my $results_dir = "$out_dir/results";
make_path($results_dir) unless -e $results_dir;

my $seq_base = basename($alignment_file);
$seq_base =~ s/\.\w+$//;
my ($flanking) = $seq_base =~ /^(\d+)/;
my $randomised = ($seq_base =~ /random/) ? 1 : 0;
my $ref_species = 'Homo_sapiens';
# Sort Out Files and Directories - End
################################################################################

################################################################################
# Execute RNA Gene Detection Software - Start
my @results_columns = (
	'Alignment', 'Flanking', 'Randomised', 'Software', 'Executed', 'RNA Gene',
	'TP', 'TN', 'FP', 'FN', 'Sensitivity', 'Specificity', 'MCC',
	'Pattern', 'Inferred'
);
my @boundaries_columns = (
	'Flanking', 'Randomised', 'Software',
	'Direction', 'Overlaps RNA', 'Position', 'Inferred'
);
my $results_file = "$results_dir/results.txt";
my $boundaries_file = "$results_dir/boundaries.txt";
save_to_file(join("\t", @results_columns), $results_file) unless (-e $results_file);
save_to_file(join("\t", @boundaries_columns), $boundaries_file) unless (-e $boundaries_file);
my ($results, $boundaries);

foreach my $software (keys %software) {
	my $sw_results_dir = "$results_dir/$software";
	make_path($sw_results_dir) unless -e $sw_results_dir;
	
	my $out_file = "$sw_results_dir/$seq_base.out";
	my ($rna_gene, $executed) = (0, 0);
	if (!-e $out_file) {
		print "Executing ".$software{$software}." on $alignment_file.\n" if $verbose;
		
		# The functions that call the software all return a single
		# 0/1 value, according to whether RNA was found in the region.
		# Very occasionally, one of the programs may report that
		# they are unable to process an alignment, returning 'undef'.
		if ($software eq "evofold") {
			$rna_gene = evofold($alignment_file, $tree_file, $sw_results_dir);
		} elsif ($software eq "rnaz2") {
			$rna_gene = rnaz2($alignment_file, $sw_results_dir);
		} elsif ($software eq "rnaz2_max_sp") {
			$rna_gene = rnaz2($alignment_file, $sw_results_dir, 35);
		} else {
			croak "Unknown software, '$software'";
		}
		$executed = defined($rna_gene) ? 1 : 0;
		$rna_gene = 0 unless defined($rna_gene);
	} else {
		print "Using existing ".$software{$software}." results for $alignment_file.\n" if $verbose;
		
		if ($software eq "evofold") {
			if (-s $out_file) {
				$executed = 1;
				my ($data, $cols) = read_from_delim($out_file, "\t", 1);
				if (scalar(@$data) > 0) {
					$rna_gene = 1;
				}
			}
		} elsif ($software =~ /^rnaz2/) {
			if (-e $out_file) {
				$executed = 1;
				if (-s $out_file) {
					$rna_gene = 1;
				}
			}
		} else {
			croak "Unknown software, '$software'";
		}
	}
	$results .= "\n$alignment_file\t$flanking\t$randomised\t$software\t$executed\t$rna_gene";
	
	if ($executed) {
		my $out = read_from_file($out_file);
		my ($seqs, $order) = from_maf(read_from_file("$alignment_file"));
		my $ref_seq = $$seqs{$ref_species};
		my $alignment_length = length($ref_seq);
		my $rna_length = $alignment_length - (2 * $flanking);
		my $pattern;
		if ($randomised) {
			$pattern = '0' x $alignment_length;
		} else {
			$pattern = '0' x $flanking . '1' x $rna_length . '0' x $flanking;
		}
		
		my $inferred;
		if ($software eq "evofold") {
			$inferred = evofold_inferred($out, $ref_seq);
		} elsif ($software =~ /^rnaz2/) {
			$inferred = rnaz_inferred($out, $ref_seq);
		} else {
			croak "Unknown software, '$software'";
		}
		croak "Failed to calculate inferred pattern" unless defined($inferred);
		my %accuracy = inferred_accuracy($pattern, $inferred);
		
		$results .= "\t".$accuracy{'TP'}.
					"\t".$accuracy{'TN'}.
					"\t".$accuracy{'FP'}.
					"\t".$accuracy{'FN'}.
					"\t".$accuracy{'Sensitivity'}.
					"\t".$accuracy{'Specificity'}.
					"\t".$accuracy{'MCC'}.
					"\t$pattern".
					"\t$inferred";
		
		my @inferred = split(//, $inferred);
		
		# Calculate left and right (ie up- and down-stream) separately.
		# The boundary in both cases is between positions 0 and 1.
		# We note whether the inferred gene overlaps the RNA position.
		my $overlaps_rna = $accuracy{'Sensitivity'} > 0 ? 1 : 0;
		for (my $i=0; $i < ($flanking+$rna_length); $i++) {
			my $position = $i - $flanking + 1;
			$boundaries .= "\n$flanking\t$randomised\t$software\tU\t$overlaps_rna\t$position\t".$inferred[$i];
		}
		
		for (my $i=($alignment_length-1); $i >= $flanking; $i--) {
			my $position = $i - $rna_length - $flanking + 1;
			$boundaries .= "\n$flanking\t$randomised\t$software\tD\t$overlaps_rna\t$position\t".$inferred[$i];
		}
	} else {
		$results .= "\t"x9;
	}
}

save_to_file($results, $results_file, 'append') if $results;
save_to_file($boundaries, $boundaries_file, 'append') if $boundaries;
# Execute RNA Gene Detection Software - End
################################################################################

