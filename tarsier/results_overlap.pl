#!/usr/bin/perl
use strict;
use warnings;
use Carp;

=pod

=head1 Name

results_overlap.pl

=head1 Description

Calculate overlap between EvoFold and RNAz results; and between the
different types of alignment.

=head1 Author

James Allen
(james@monkeyshines.co.uk)

=head1 Copyright

Copyright 2012 James Allen; University of Manchester

=head1 Licence

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program. If not, see <http://www.gnu.org/licenses/>.

=cut

use MonkeyShines::Overlap qw (two_lists);
use MonkeyShines::Utils qw (read_from_delim save_to_file min);

################################################################################
# Initialise Parameters - Start
my $dir = $ARGV[0];
croak "Directory must be specified" unless $dir;
croak "Directory '$dir' does not exist" unless -e $dir;

my $data_file = $ARGV[1];
croak "Data file must be specified" unless $data_file;
croak "File '$data_file' does not exist" unless -e $data_file;

my $data_dir = "$dir/data";
my $output_dir = "$dir/output";

my $bionj = 1;
my $bionj_txt = $bionj ? '.bionj' : '';

my @software = ('evofold', 'rnaz2', 'rnaz2_max_sp');
my @datasets = ('epo', 'epo_low_coverage', 'multiz');
my $flanking = 400;
# Initialise Parameters - End
################################################################################

my @columns = ('Software 1', 'Dataset 1', 'Software 2', 'Dataset 2',
				'Result Type', 'Size 1', 'Size 2', 'Overlap', 'N',
				'Expected', 'Factor', 'Probability');
my $overlap_file = "$output_dir/overlap_$flanking.txt";
save_to_file(join("\t", @columns), $overlap_file);

my $results_file = "$output_dir/prediction$bionj_txt\_results.txt";

my %ok;
my ($data, $cols1) = read_from_delim($data_file, "\t", 1);
foreach my $line (@$data) {
	my $id = $$line[$$cols1{'ID'}];
	my $chr = $$line[$$cols1{'Chr'}];
	my $location = $$line[$$cols1{'Location'}];
	my $dataset = $$line[$$cols1{'Dataset'}];
	
	$ok{"$id$chr$location$dataset"} = 1;
}

my (%tp, %fp, %tp_N, %fp_N);
foreach my $software (@software) {
	my ($results, $cols) = read_from_delim($results_file, "\t", 1);
	
	my $counter = 0;
	foreach my $row (@$results) {
		my $id = $$row[$$cols{'ID'}];
		my $chr = $$row[$$cols{'Chr'}];
		my $location = $$row[$$cols{'Location'}];
		my $dataset = $$row[$$cols{'Dataset'}];
		
		next unless exists $ok{"$id$chr$location$dataset"};
		
		if ($$row[$$cols{'Flanking'}] == $flanking && $$row[$$cols{'Software'}] eq $software) {
			my $randomised = $$row[$$cols{'Randomised'}];
			if ($randomised) {
				$counter++;
			} else {
				$counter = 0;
			}
			
			if ($$row[$$cols{'Executed'}]) {
				my $rna_gene = $$row[$$cols{'RNA Gene'}];
				my $sensitivity = $$row[$$cols{'Sensitivity'}];
				$rna_gene = 0 if ($randomised == 0 && $sensitivity == 0);
				
				if ($rna_gene) {
					my $id = "$id$chr$location";
					
					if ($randomised) {
						push @{$fp{$software}{$dataset}}, "$id$counter";
					} else {
						push @{$tp{$software}{$dataset}}, $id;
					}
				}
				if ($randomised) {
					$fp_N{$software}{$dataset}++;
				} else {
					$tp_N{$software}{$dataset}++;
				}
			}
		}
	}
}

foreach my $dataset (@datasets) {
	my $evofold_tp = $tp{'evofold'}{$dataset};
	my $rnaz2_tp = $tp{'rnaz2'}{$dataset};
	my $rnaz2_max_sp_tp = $tp{'rnaz2_max_sp'}{$dataset};
	my $tp_N = min($tp_N{'evofold'}{$dataset}, $tp_N{'rnaz2'}{$dataset}, $tp_N{'rnaz2_max_sp'}{$dataset});
	
	my ($expected, $factor, $p, $rnaz2, $evofold, $overlap, undef)
		= two_lists($rnaz2_tp, $evofold_tp, $tp_N);
	my $results = "\nevofold\t$dataset\trnaz2\t$dataset\tTP".
					"\t$evofold\t$rnaz2\t$overlap\t$tp_N\t$expected".
					"\t$factor\t$p";
	save_to_file($results, $overlap_file, 'append');
	
	my $rnaz2_max_sp;
	($expected, $factor, $p, $rnaz2_max_sp, $evofold, $overlap, undef)
		= two_lists($rnaz2_max_sp_tp, $evofold_tp, $tp_N);
	$results = "\nevofold\t$dataset\trnaz2_max_sp\t$dataset\tTP".
					"\t$evofold\t$rnaz2_max_sp\t$overlap\t$tp_N\t$expected".
					"\t$factor\t$p";
	save_to_file($results, $overlap_file, 'append');
	
	($expected, $factor, $p, $rnaz2, $rnaz2_max_sp, $overlap, undef)
		= two_lists($rnaz2_tp, $rnaz2_max_sp_tp, $tp_N);
	$results = "\nrnaz2_max_sp\t$dataset\trnaz2\t$dataset\tTP".
					"\t$rnaz2_max_sp\t$rnaz2\t$overlap\t$tp_N\t$expected".
					"\t$factor\t$p";
	save_to_file($results, $overlap_file, 'append');
	
	my $evofold_fp = $fp{'evofold'}{$dataset};
	my $rnaz2_fp = $fp{'rnaz2'}{$dataset};
	my $rnaz2_max_sp_fp = $fp{'rnaz2_max_sp'}{$dataset};
	my $fp_N = min($fp_N{'evofold'}{$dataset}, $fp_N{'rnaz2'}{$dataset}, $fp_N{'rnaz2_max_sp'}{$dataset});
	
	($expected, $factor, $p, $rnaz2, $evofold, $overlap, undef)
		= two_lists($rnaz2_fp, $evofold_fp, $fp_N);
	$results = "\nevofold\t$dataset\trnaz2\t$dataset\tFP".
					"\t$evofold\t$rnaz2\t$overlap\t$fp_N\t$expected".
					"\t$factor\t$p";
	save_to_file($results, $overlap_file, 'append');
	
	($expected, $factor, $p, $rnaz2_max_sp, $evofold, $overlap, undef)
		= two_lists($rnaz2_max_sp_fp, $evofold_fp, $fp_N);
	$results = "\nevofold\t$dataset\trnaz2_max_sp\t$dataset\tFP".
					"\t$evofold\t$rnaz2_max_sp\t$overlap\t$fp_N\t$expected".
					"\t$factor\t$p";
	save_to_file($results, $overlap_file, 'append');
	
	($expected, $factor, $p, $rnaz2, $rnaz2_max_sp, $overlap, undef)
		= two_lists($rnaz2_fp, $rnaz2_max_sp_fp, $fp_N);
	$results = "\nrnaz2_max_sp\t$dataset\trnaz2\t$dataset\tFP".
					"\t$rnaz2_max_sp\t$rnaz2\t$overlap\t$fp_N\t$expected".
					"\t$factor\t$p";
	save_to_file($results, $overlap_file, 'append');
}

foreach my $software (@software) {
	my $epo_tp = $tp{$software}{'epo'};
	my $epo_lc_tp = $tp{$software}{'epo_low_coverage'};
	my $multiz_tp = $tp{$software}{'multiz'};
	my $tp_N = min($tp_N{$software}{'epo'}, $tp_N{$software}{'epo_low_coverage'}, $tp_N{$software}{'multiz'});
	
	my ($expected, $factor, $p, $set1, $set2, $overlap, undef)
		= two_lists($epo_tp, $epo_lc_tp, $tp_N);
	my $results = "\n$software\tepo\t$software\tepo_low_coverage\tTP".
					"\t$set1\t$set2\t$overlap\t$tp_N\t$expected".
					"\t$factor\t$p";
	save_to_file($results, $overlap_file, 'append');
	
	($expected, $factor, $p, $set1, $set2, $overlap, undef)
		= two_lists($epo_tp, $multiz_tp, $tp_N);
	$results = "\n$software\tepo\t$software\tmultiz\tTP".
					"\t$set1\t$set2\t$overlap\t$tp_N\t$expected".
					"\t$factor\t$p";
	save_to_file($results, $overlap_file, 'append');
	
	($expected, $factor, $p, $set1, $set2, $overlap, undef)
		= two_lists($epo_lc_tp, $multiz_tp, $tp_N);
	$results = "\n$software\tepo_low_coverage\t$software\tmultiz\tTP".
					"\t$set1\t$set2\t$overlap\t$tp_N\t$expected".
					"\t$factor\t$p";
	save_to_file($results, $overlap_file, 'append');
	
	my $epo_fp = $fp{$software}{'epo'};
	my $epo_lc_fp = $fp{$software}{'epo_low_coverage'};
	my $multiz_fp = $fp{$software}{'multiz'};
	my $fp_N = min($fp_N{$software}{'epo'}, $fp_N{$software}{'epo_low_coverage'}, $fp_N{$software}{'multiz'});
	
	($expected, $factor, $p, $set1, $set2, $overlap, undef)
		= two_lists($epo_fp, $epo_lc_fp, $fp_N);
	$results = "\n$software\tepo\t$software\tepo_low_coverage\tFP".
					"\t$set1\t$set2\t$overlap\t$fp_N\t$expected".
					"\t$factor\t$p";
	save_to_file($results, $overlap_file, 'append');
	
	($expected, $factor, $p, $set1, $set2, $overlap, undef)
		= two_lists($epo_fp, $multiz_fp, $fp_N);
	$results = "\n$software\tepo\t$software\tmultiz\tFP".
					"\t$set1\t$set2\t$overlap\t$fp_N\t$expected".
					"\t$factor\t$p";
	save_to_file($results, $overlap_file, 'append');
	
	($expected, $factor, $p, $set1, $set2, $overlap, undef)
		= two_lists($epo_lc_fp, $multiz_fp, $fp_N);
	$results = "\n$software\tepo_low_coverage\t$software\tmultiz\tFP".
					"\t$set1\t$set2\t$overlap\t$fp_N\t$expected".
					"\t$factor\t$p";
	save_to_file($results, $overlap_file, 'append');
}

