library(colorspace)

boundaries_file = "S:/cygwin/tmp/mammals/output/prediction.bionj_boundaries.txt"
plot_dir = "S:/cygwin/tmp/mammals/output/prediction"

basename = 'detection_boundaries'
flankings = c(0, 400)

software = c('evofold', 'rnaz2', 'rnaz2_max_sp')
software_display = c('EvoFold', 'RNAz', 'RNAz (Max Sp.)')
names(software_display) = software
software_colours = rainbow_hcl(length(software), c=80, l=50)
names(software_colours) = software

software = c('evofold', 'rnaz2_max_sp')
software_display = c('EvoFold', 'RNAz (Max Sp.)')
names(software_display) = software
software_colours = c(software_colours[1], software_colours[3])
names(software_colours) = software

datasets = c('epo', 'epo_low_coverage', 'multiz')
datasets_display = c('EPO-12', 'EPO-35', 'MultiZ')
names(datasets_display) = datasets

# If this is 1, then the data is filtered on the 'Overlaps.RNA' column.
overlaps_rna = 0
if (overlaps_rna) {
  basename = paste(basename, 'overlap', sep='_')
}

boundaries = read.delim(boundaries_file)

for (flanking in flankings) {
  for (dataset in datasets) {
    #evofold_dataset = subset(boundaries, boundaries$Flanking == flanking & boundaries$Dataset == dataset & boundaries$Software == 'evofold')
    #rnaz_dataset = subset(boundaries, boundaries$Flanking == flanking & boundaries$Dataset == dataset & boundaries$Software == 'rnaz2')
    #rnaz_max_sp_dataset = subset(boundaries, boundaries$Flanking == flanking & boundaries$Dataset == dataset & boundaries$Software == 'rnaz2_max_sp')
    #boundaries = rbind(evofold_dataset, rnaz_dataset)
    filtered = subset(boundaries, boundaries$Flanking == flanking & boundaries$Dataset == dataset)
    if (overlaps_rna) {
      filtered = subset(filtered, filtered$Overlaps.RNA == 1)
    }
    #filtered = subset(boundaries, boundaries$Software == software[1])
    #if (length(software) > 1) {
    #    for (counter in c(2:length(software))) {
    #        filtered = rbind(filtered, subset(boundaries, boundaries$Software == software[counter]))
    #    }
    #}
    
    attach(filtered)
    up = subset(filtered, Direction == 'U' & Position <= 200 & Position > -400)
    down = subset(filtered, Direction == 'D' & Position < 400 & Position >= -200)
    merged = transform(down, Position = (Position * -1) + 1)
    merged = rbind(merged, up)
    merged$Direction = 'Merged'
    detach(filtered)
    rm(filtered)
    
    attach(up)
    positives = aggregate(up['Inferred'], list(Software=Software, Randomised=Randomised, Position=Position), sum)
    totals = aggregate(up['Total'], list(Software=Software, Randomised=Randomised, Position=Position), sum)
    up_freqs = merge(positives, totals)
    up_freqs = cbind(up_freqs, PPR=up_freqs$Inferred/up_freqs$Total)
    up_freqs = up_freqs[order(up_freqs$Position),]
    detach(up)
    
    attach(down)
    positives = aggregate(down['Inferred'], list(Software=Software, Randomised=Randomised, Position=Position), sum)
    totals = aggregate(down['Total'], list(Software=Software, Randomised=Randomised, Position=Position), sum)
    down_freqs = merge(positives, totals)
    down_freqs = cbind(down_freqs, PPR=down_freqs$Inferred/down_freqs$Total)
    down_freqs = down_freqs[order(down_freqs$Position),]
    detach(down)
    
    attach(merged)
    positives = aggregate(merged['Inferred'], list(Software=Software, Randomised=Randomised, Position=Position), sum)
    totals = aggregate(merged['Total'], list(Software=Software, Randomised=Randomised, Position=Position), sum)
    merged_freqs = merge(positives, totals)
    merged_freqs = cbind(merged_freqs, PPR=merged_freqs$Inferred/merged_freqs$Total)
    merged_freqs = merged_freqs[order(merged_freqs$Position),]
    detach(merged)
    
    plot_file1 = paste(basename, 'updown', flanking, dataset, sep='_')
    plot_file1 = paste(plot_file1, 'pdf', sep='.')
    plot_file1 = paste(plot_dir, plot_file1, sep='/')
    plot_file2 = paste(basename, 'merged', flanking, dataset, sep='_')
    plot_file2 = paste(plot_file2, 'pdf', sep='.')
    plot_file2 = paste(plot_dir, plot_file2, sep='/')
    
    pdf(plot_file1, width=10, height=4)
    dev.control(displaylist="enable")
    par(mai=c(1, 1, 0.25, 0.25), omi=c(0, 0, 0, 0), mfrow=c(1,2), las=1)
    
    plot(
        x = 0,
        y = 0,
        type = 'n',
        xlim = c(-1 * as.numeric(flanking), 200),
        ylim = c(0, 1),
        xlab = 'Sequence Sites (Upstream | RNA)',
        ylab = 'Prediction Rate',
        cex.lab = 1.25,
        cex.axis = 1.25
    )
    abline(v=0.5, lty="dashed")
    legend('topleft', software_display, col=software_colours, pch=16, cex=1.25, pt.cex=1.5)
    
    attach(up_freqs)
    for (sw in software) {
      lines(
          Position[Software == sw & Randomised == 0],
          PPR[Software == sw & Randomised == 0],
          col = software_colours[[sw]],
          lwd = 2
      )
      lines(
          Position[Software == sw & Randomised == 1],
          PPR[Software == sw & Randomised == 1],
          col = software_colours[[sw]],
          lwd = 2, lty = 2
      )
    }
    detach(up_freqs)
    
    plot(
        x = 0,
        y = 0,
        type = 'n',
        xlim = c(-200, as.numeric(flanking)),
        ylim = c(0, 1),
        xlab = 'Sequence Sites (RNA | Downstream)',
        ylab = 'Prediction Rate',
        cex.lab = 1.25,
        cex.axis = 1.25
    )
    abline(v=0.5, lty="dashed")
    
    attach(down_freqs)
    for (sw in software) {
      lines(
          Position[Software == sw & Randomised == 0],
          PPR[Software == sw & Randomised == 0],
          col = software_colours[[sw]],
          lwd = 2
      )
      lines(
          Position[Software == sw & Randomised == 1],
          PPR[Software == sw & Randomised == 1],
          col = software_colours[[sw]],
          lwd = 2, lty = 2
      )
    }
    detach(down_freqs)
    
    dev.off()
    
    pdf(plot_file2, width=6, height=4)
    dev.control(displaylist="enable")
    par(mai=c(1, 1, 0.25, 0.25), las=1)
    
    plot(
        x = 0,
        y = 0,
        type = 'n',
        xlim = c(-1 * as.numeric(flanking), 200),
        ylim = c(0, 1),
        xlab = 'Sequence Sites (Flanking | RNA)',
        ylab = 'Prediction Rate',
        cex.lab = 1.25,
        cex.axis = 1.25
    )
    abline(v=0.5, lty="dashed")
    
    legend('topleft', software_display, col=software_colours, pch=16, cex=1.25, pt.cex=1.5)
    
    attach(merged_freqs)
    for (sw in software) {
      lines(
          Position[Software == sw & Randomised == 0],
          PPR[Software == sw & Randomised == 0],
          col = software_colours[[sw]],
          lwd = 2
      )
      lines(
          Position[Software == sw & Randomised == 1],
          PPR[Software == sw & Randomised == 1],
          col = software_colours[[sw]],
          lwd = 2, lty = 2
      )
    }
    detach(merged_freqs)
    
    dev.off()
  }
}

