library(colorspace)
source('MonkeyShines/tarsier/r_scripts/wilson_ci.r')

results_file = "S:/cygwin/tmp/mammals/output/prediction.bionj_results.txt"
stats_file = "S:/cygwin/tmp/mammals/output/flanking_rna.filtered.txt"
plot_dir = "S:/cygwin/tmp/mammals/output/prediction"

basename = 'prediction_tpr_fpr'
flankings = c(0, 400)

software = c('evofold', 'rnaz2', 'rnaz2_max_sp')
software_display = c('EvoFold', 'RNAz', 'RNAz (Max. Sp.)')
names(software_display) = software
software_colours = rainbow_hcl(length(software), c=80, l=50)
names(software_colours) = software

datasets = c('epo', 'epo_low_coverage', 'multiz')
datasets_display = c('EPO', 'EPO: Low Coverage', 'MultiZ')
names(datasets_display) = datasets

results = read.delim(results_file)
stats = read.delim(stats_file)
results = merge(results, stats)
results$Genic = nchar(results$Gene.Position) > 0

for (flanking in flankings) {
  for (dataset in datasets) {
    results_dataset = subset(results, results$Flanking == flanking & results$Dataset == dataset & results$Executed == 1)
    
    # If an RNA gene is predicted that does not overlap the RNA region at all
    # we ignore it, since we've already removed sequences that appeared to
    # have RNA in the flanking regions.
    results_dataset[results_dataset$Randomised == 0 & results_dataset$Sensitivity == 0, "RNA.Gene"] = 0;
    
    filtered = subset(results_dataset, results_dataset$Software == software[1])
    if (length(software) > 1) {
        for (counter in c(2:length(software))) {
            filtered = rbind(filtered, subset(results_dataset, results_dataset$Software == software[counter]))
        }
    }
    
    pos_dataset = subset(filtered, filtered$Randomised == 0)
    neg_dataset = subset(filtered, filtered$Randomised == 1)
    
    if (nrow(pos_dataset) == 0 || nrow(neg_dataset) == 0) {
        stop('Insufficient data for analysis.')
    }
    
    true_pos = aggregate(pos_dataset['RNA.Gene'], list(Software=pos_dataset$Software), sum)
    pos_total = aggregate(pos_dataset['RNA.Gene'], list(Software=pos_dataset$Software), length)
    false_pos = aggregate(neg_dataset['RNA.Gene'], list(Software=neg_dataset$Software), sum)
    neg_total = aggregate(neg_dataset['RNA.Gene'], list(Software=neg_dataset$Software), length)
    
    true_pos_con = aggregate(pos_dataset['RNA.Gene'], list(Software=pos_dataset$Software, Genic=pos_dataset$Genic), sum)
    pos_total_con = aggregate(pos_dataset['RNA.Gene'], list(Software=pos_dataset$Software, Genic=pos_dataset$Genic), length)
    false_pos_con = aggregate(neg_dataset['RNA.Gene'], list(Software=neg_dataset$Software, Genic=neg_dataset$Genic), sum)
    neg_total_con = aggregate(neg_dataset['RNA.Gene'], list(Software=neg_dataset$Software, Genic=neg_dataset$Genic), length)
    true_pos = rbind(true_pos_con, cbind(true_pos, Genic=NA))
    pos_total = rbind(pos_total_con, cbind(pos_total, Genic=NA))
    false_pos = rbind(false_pos_con, cbind(false_pos, Genic=NA))
    neg_total = rbind(neg_total_con, cbind(neg_total, Genic=NA))
    
    false_neg = pos_total['RNA.Gene'] - true_pos['RNA.Gene']
    true_neg = neg_total['RNA.Gene'] - false_pos['RNA.Gene']
    
    pos_summary = cbind(true_pos, false_neg)
    neg_summary = cbind(false_pos, true_neg)
    names(pos_summary) = c('Software', 'Genic', 'TP', 'FN')
    names(neg_summary) = c('Software', 'Genic', 'FP', 'TN')
    summary = merge(pos_summary, neg_summary)
    attach(summary)
    
    # There's redundancy here, but it makes later analysis easier.
    # The sapply bit replaces any NaNs with zeroes
    sensitivity = sapply(TP/(TP+FN), function(x) {x[is.na(x)] <- 0; x})
    specificity = sapply(TN/(TN+FP), function(x) {x[is.na(x)] <- 0; x})
    tp_rate = sensitivity
    fp_rate = 1-specificity
    recall = sensitivity
    precision = sapply(TP/(TP+FP), function(x) {x[is.na(x)] <- 0; x})
    f_measure = sapply(2*((precision * recall)/(precision + recall)), function(x) {x[is.na(x)] <- 0; x})
    
    tpr_ci = wilson_ci(TP, TP+FN)
    names(tpr_ci) = c("tpr_ci_lower", "tpr_ci_upper")
    fpr_ci = wilson_ci(FP, TN+FP)
    names(fpr_ci) = c("fpr_ci_lower", "fpr_ci_upper")
    prec_ci = wilson_ci(TP, TP+FP)
    names(prec_ci) = c("prec_ci_lower", "prec_ci_upper")
    
    summary = cbind(summary, sensitivity, specificity, tp_rate, fp_rate, recall, precision, f_measure, tpr_ci, fpr_ci, prec_ci)
    remove(sensitivity, specificity, tp_rate, fp_rate, recall, precision, f_measure, tpr_ci, fpr_ci, prec_ci)
    detach(summary)
    attach(summary)
    
    text_file = paste(basename, flanking, dataset, sep='_')
    text_file = paste(text_file, 'txt', sep='.')
    text_file = paste(plot_dir, text_file, sep='/')
    plot_file = paste(basename, flanking, dataset, sep='_')
    plot_file = paste(plot_file, 'pdf', sep='.')
    plot_file = paste(plot_dir, plot_file, sep='/')
    
    f = file(text_file, open='wb')
    write.table(summary, file=f, quote=FALSE, sep='\t', eol='\n', row.names=FALSE)
    close(f)
    
    pdf(plot_file, width=6, height=6)
    dev.control(displaylist="enable")
    par(mai=c(1, 1, 0.25, 0.25), las=1)
    
    plot(
        x = 0,
        y = 0,
        type = 'n',
        xlim = c(0, 1),
        ylim = c(0, 1),
        xlab = 'False Positive Rate',
        ylab = 'True Positive Rate',
        cex.lab = 1.25,
        cex.axis = 1.25
    )
    abline(a=0, b=1, lty="dashed")
    
    legend_text = c(software_display, '', 'All', 'Genic', 'Non-Genic')
    legend_colours = c(software_colours, 'white', 'black', 'black', 'black')
    legend_symbols = c(rep(16, length(software)), 16, 21, 22, 23)
    legend('bottomright', legend_text, col=legend_colours, pch=legend_symbols, cex=1.25, pt.cex=1.5)
    
    for (sw in software) {
      swcol = software_colours[[sw]]
      
      # All data
      points(
          x = fp_rate[Software == sw & is.na(Genic)],
          y = tp_rate[Software == sw & is.na(Genic)],
          col = swcol, bg = swcol,
          pch = 21, cex = 1.5
      )
      
      # Error bars are two double-ended arrows, with flat arrow-heads...
      arrows(
          x0 = fp_rate[Software == sw & is.na(Genic)],
          y0 = tpr_ci_upper[Software == sw & is.na(Genic)],
          y1 = tpr_ci_lower[Software == sw & is.na(Genic)],
          length = 0.05, angle = 90, code = 3, col=swcol
      )
      arrows(
          x0 = fpr_ci_upper[Software == sw & is.na(Genic)],
          x1 = fpr_ci_lower[Software == sw & is.na(Genic)],
          y0 = tp_rate[Software == sw & is.na(Genic)],
          length = 0.05, angle = 90, code = 3, col=swcol
      )
      
      # 'Genic'
      points(
          x = fp_rate[Software == sw & Genic],
          y = tp_rate[Software == sw & Genic],
          col = swcol, bg = swcol,
          pch = 22, cex = 1.5
      )
      
      arrows(
          x0 = fp_rate[Software == sw & Genic],
          y0 = tpr_ci_upper[Software == sw & Genic],
          y1 = tpr_ci_lower[Software == sw & Genic],
          length = 0.05, angle = 90, code = 3, col=swcol
      )
      arrows(
          x0 = fpr_ci_upper[Software == sw & Genic],
          x1 = fpr_ci_lower[Software == sw & Genic],
          y0 = tp_rate[Software == sw & Genic],
          length = 0.05, angle = 90, code = 3, col=swcol
      )
      
      # 'Non-Genic'
      points(
          x = fp_rate[Software == sw & !Genic],
          y = tp_rate[Software == sw & !Genic],
          col = swcol, bg = swcol,
          pch = 23, cex = 1.5
      )
      arrows(
          x0 = fp_rate[Software == sw & !Genic],
          y0 = tpr_ci_upper[Software == sw & !Genic],
          y1 = tpr_ci_lower[Software == sw & !Genic],
          length = 0.05, angle = 90, code = 3, col=swcol)
      arrows(
          x0 = fpr_ci_upper[Software == sw & !Genic],
          x1 = fpr_ci_lower[Software == sw & !Genic],
          y0 = tp_rate[Software == sw & !Genic],
          length = 0.05, angle = 90, code = 3, col=swcol)
    }
    
    dev.off()
    detach(summary)
  }
}

