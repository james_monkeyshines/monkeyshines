library(TeachingDemos)

gap_dist_file = "S:/cygwin/tmp/mammals/output/prediction.bionj_gap_dist.txt"
plot_dir = "S:/cygwin/tmp/mammals/output/prediction"

basename = 'gap_distribution'
flankings = c(0, 400)

datasets = c('epo', 'epo_low_coverage', 'multiz')
datasets_display = c('EPO-12', 'EPO-35', 'MultiZ')
names(datasets_display) = datasets

y_axis_zoom = 0.1

library(colorspace)
dataset_colours = rainbow_hcl(length(datasets), c=70, l=70, start=30, end=300)
names(dataset_colours) = datasets

gap_dist = read.delim(gap_dist_file)

for (flanking in flankings) {
  gap_dist_flanking = subset(gap_dist, gap_dist$Flanking == flanking & gap_dist$Gap.Length <= 400)
  gap_dist_flanking$Gap.Length = log2(gap_dist_flanking$Gap.Length)
  
  plot_file = paste(basename, flanking, sep='_')
  plot_file = paste(plot_file, 'pdf', sep='.')
  plot_file = paste(plot_dir, plot_file, sep='/')
  
  pdf(plot_file, width=6, height=4)
  dev.control(displaylist="enable")
  par(mai=c(1, 1, 0.25, 0.25), lwd=2, las=1)
  
  hist_breaks=c(0, 1, 2, 4, 8, 16, 32, 64, 128, 256)
  
  #gap_dist_flanking_hist = hist(gap_dist_flanking[,'Gap.Length'], breaks=20, plot=FALSE)
  gap_dist_flanking_hist = hist(gap_dist_flanking[,'Gap.Length'], breaks=hist_breaks, plot=FALSE)
  hist_bins = gap_dist_flanking_hist$breaks
  hist_bin_mids = gap_dist_flanking_hist$mids
  
  freqs = data.frame(row.names=hist_bin_mids)
  for (dataset in datasets) {
    gap_dist_dataset = subset(gap_dist_flanking, gap_dist_flanking$Dataset == dataset)
    gap_dist_dataset_hist = hist(gap_dist_dataset[,'Gap.Length'], breaks=hist_bins, plot=FALSE)
    
    freqs[,dataset] = gap_dist_dataset_hist$counts/sum(gap_dist_dataset_hist$counts)
  }
  max_freq = ceiling(max(freqs)*10)/10
  #max_freq = 0.5
  
  plot(
      x = min(hist_bins),
      y = 0,
      type = 'n',
      xlim = c(min(hist_bins), max(hist_bins)),
      ylim = c(0, max_freq),
      xlab = 'Gap Length',
      ylab = 'Frequency',
      cex.axis = 1.25,
      cex.lab = 1.5
  )
  
  for (dataset in datasets) {
    lines(
        x = hist_bin_mids,
        y = freqs[,dataset],
        col = dataset_colours[[dataset]]
    )
  }
  
  legend('topleft', datasets_display, col=dataset_colours, pch=15, cex=1, inset = c(0.10, 0.05),)
  
  sp = subplot(
    plot(
        x = min(hist_bins),
        y = 0,
        type = 'n',
        xlim = c(0, 150),
        ylim = c(0, y_axis_zoom),
        xlab = '',
        ylab = '',
        xaxt = 'n',
        yaxt = 'n'
     ),
     x = 'topright',
     inset = c(0.05, 0.05),
     size = c(1.5, 1.5),
     pars = list(cex.axis=0.9)
  )
  par(sp)
  axis(1, at=c(0, 50, 100, 150), labels=c(0, 50, 100, 150), tck=-0.05, mgp=c(3, 0.3, 0))
  axis(2, at=c(0, 0.05, 0.1), labels=c(0.0, 0.05, 0.1), las=1, hadj=1, tck=-0.05, mgp=c(3, 0.4, 0))
  
  for (dataset in datasets) {
    lines(
        x = hist_bin_mids,
        y = freqs[,dataset],
        col = dataset_colours[[dataset]]
    )
  }
  
  dev.off()
}

