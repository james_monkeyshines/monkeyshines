#!/usr/bin/perl
use strict;
use warnings;
use Carp;

=pod

=head1 Name

gap_distribution.pl

=head1 Description

Work out the gap distribution. For cases where the flanking length
is zero, the gap lengths are calculated on the RNA; for all positive
flanking lengths, the gap lengths are calculated for the flanking _only_,
excluding the RNA.

=head1 Author

James Allen
(james@monkeyshines.co.uk)

=head1 Copyright

Copyright 2012 James Allen; University of Manchester

=head1 Licence

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program. If not, see <http://www.gnu.org/licenses/>.

=cut

use MonkeyShines::Sequence qw (from_fasta);
use MonkeyShines::Utils qw (read_from_file read_from_delim save_to_file);

################################################################################
# Initialise Parameters - Start
my $dir = $ARGV[0];
croak "Directory must be specified" unless $dir;
croak "Directory '$dir' does not exist" unless -e $dir;

my $data_file = $ARGV[1];
croak "Data file must be specified" unless $data_file;
croak "File '$data_file' does not exist" unless -e $data_file;

my $data_dir = "$dir/data";
my $output_dir = "$dir/output";

my $bionj = 1;
my $bionj_txt = $bionj ? '.bionj' : '';

my @flanking = (0, 400);
# Initialise Parameters - End
################################################################################

################################################################################
# Count Gaps - Start
my @columns = ('ID', 'Chr', 'Location', 'Dataset', 'Flanking',
				'Species Name', 'Gap Length');

my $gap_dist_file = "$output_dir/prediction$bionj_txt\_gap_dist.txt";
save_to_file(join("\t", @columns), $gap_dist_file);

my ($data, $cols) = read_from_delim($data_file, "\t", 1);
foreach my $line (@$data) {
	my $id = $$line[$$cols{'ID'}];
	my $chr = $$line[$$cols{'Chr'}];
	my $location = $$line[$$cols{'Location'}];
	my $dataset = $$line[$$cols{'Dataset'}];
	
	my $aln_dir = "$data_dir/$id/$chr/$location/$dataset";
	next unless -d $aln_dir;
	
	my $gap_data = "";
	foreach my $flanking (@flanking) {
		my $aln_file = "$aln_dir/$flanking.fa";
		my ($seqs, undef) = from_fasta(read_from_file($aln_file));
		
		foreach my $id (keys %$seqs) {
			my $seq = $$seqs{$id};
			
			if ($flanking > 0) {
				my ($fl1, $fl2) = $seq =~ /^(.{$flanking}).+(.{$flanking})$/;
				$seq = "$fl1 $fl2";
			}
			while ($seq =~ /[\-\.]/) {
				$seq =~ s/([\-\.]+)//;
				$gap_data .= "\n$id\t$chr\t$location\t$dataset\t$flanking\t$id\t".length($1);
			}
		}
	}
	save_to_file($gap_data, $gap_dist_file, 'append');
}
# Count Gaps - End
################################################################################

