library(ggplot2)

gap_dist_file = "S:/cygwin/tmp/mammals/output/prediction.bionj_gap_dist.txt"
plot_dir = "S:/cygwin/tmp/mammals/output/prediction"

basename = 'gap_distribution'
flankings = c(0, 400)

datasets = c('epo', 'epo_low_coverage', 'multiz')
datasets_display = c('EPO-12', 'EPO-35', 'MultiZ')
names(datasets_display) = datasets

library(colorspace)
dataset_colours = rainbow_hcl(length(datasets), c=40, l=40, start=80, end=300)
names(dataset_colours) = datasets

gap_dist = read.delim(gap_dist_file)
gap_dist$Dataset = datasets_display[gap_dist$Dataset]

for (flanking in flankings) {
  gap_dist_flanking = subset(gap_dist, gap_dist$Flanking == flanking & Gap.Length <= 128)
  
  plot_file = paste(basename, flanking, sep='_')
  plot_file = paste(plot_file, 'pdf', sep='.')
  plot_file = paste(plot_dir, plot_file, sep='/')
  
  pdf(plot_file, width=4, height=4)
  dev.control(displaylist="enable")
  
  m = ggplot(gap_dist_flanking, aes(x=Gap.Length, y=..density..))
  m = m + scale_y_continuous('Density\n')
  m = m + scale_x_log2('\nGap Length (log2 scale)', breaks=c(1,2,4,8,16,32,64,128), labels=c('1','2','4','8','16','32','64','128'))
  m + geom_histogram(binwidth=1)  + facet_grid(Dataset ~ .) + theme_bw()
  
  dev.off()
}

