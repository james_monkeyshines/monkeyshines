library(colorspace)

results_file = "S:/cygwin/tmp/mammals/output/prediction.bionj_results.txt"
plot_dir = "S:/cygwin/tmp/mammals/output/prediction"

basename = 'detection_sens_spec'
flankings = c(0, 400)

software = c('evofold', 'rnaz2', 'rnaz2_max_sp')
software_display = c('EvoFold', 'RNAz', 'RNAz (Max Sp.)')
names(software_display) = software
software_colours = rainbow_hcl(length(software), c=80, l=50)
names(software_colours) = software

datasets = c('epo', 'epo_low_coverage', 'multiz')
datasets_display = c('EPO-12', 'EPO-35', 'MultiZ')
names(datasets_display) = datasets

results = read.delim(results_file)

for (flanking in flankings) {
  for (dataset in datasets) {
    filtered = subset(results, results$Flanking == flanking & results$Dataset == dataset & results$Executed == 1)
    #filtered = subset(results_dataset, results_dataset$Software == software[1])
    #if (length(software) > 1) {
    #    for (counter in c(2:length(software))) {
    #        filtered = rbind(filtered, subset(results_dataset, results_dataset$Software == software[counter]))
    #    }
    #}
    attach(filtered)
    
    plot_file = paste(basename, flanking, dataset, sep='_')
    plot_file = paste(plot_file, 'pdf', sep='.')
    plot_file = paste(plot_dir, plot_file, sep='/')
    
    pdf(plot_file, width=9, height=5)
    dev.control(displaylist="enable")
    par(mai=c(1, 1, 0.25, 0.25), pch=43, mfrow=c(1, 2), las=1)
    
    for (sw in software) {
      plot(
          x = 0,
          y = 0,
          type = 'n',
          main = software_display[[sw]],
          xlab = 'Specificity',
          ylab = 'Sensitivity',
          xlim = c(0, 1),
          ylim = c(0, 1),
          cex.main = 1.25,
          cex.lab = 1.25,
          cex.axis = 1.25
      )
      abline(a=1, b=-1, lty="dashed")
      
      points(
          x = Specificity[Software == sw],
          y = Sensitivity[Software == sw],
          col = software_colours[[sw]],
          pch=43, cex=1.5
      )
    }
    
    dev.off()
    detach(filtered)
  }
}

