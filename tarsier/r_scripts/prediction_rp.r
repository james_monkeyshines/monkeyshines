library(colorspace)
source('MonkeyShines/tarsier/r_scripts/wilson_ci.r')

results_file = "S:/cygwin/tmp/mammals/output/prediction.bionj_results.txt"
stats_file = "S:/cygwin/tmp/mammals/output/results.filtered.txt"
plot_dir = "S:/cygwin/tmp/mammals/output/prediction"

basename = 'prediction_rp'
flankings = c(0, 400)

software = c('evofold', 'rnaz2', 'rnaz2_max_sp')
software_display = c('EvoFold', 'RNAz', 'RNAz (Max Sp.)')
names(software_display) = software
software_colours = rainbow_hcl(length(software), c=80, l=50)
names(software_colours) = software

software = c('evofold', 'rnaz2_max_sp')
software_display = c('EvoFold', 'RNAz (Max Sp.)')
names(software_display) = software
software_colours = c(software_colours[1], software_colours[3])
names(software_colours) = software

datasets = c('epo', 'epo_low_coverage', 'multiz')
datasets_display = c('EPO-12', 'EPO-35', 'MultiZ')
names(datasets_display) = datasets

properties = c('MPI', 'GC.Content', 'Gaps', 'Effective.Species..Flanking.', 'Adjusted.Tree.Length', 'RNA.Length', 'Paired.Bases.PC')
properties_display = c('MPI (Mean Pairwise Identity)', 'GC Content', 'Gap Percentage', 'Number of Species', 'Tree Length', 'RNA Length', 'Proportion of Paired Bases')
legend_pos = c('topleft', 'topleft', 'topleft', 'topleft', 'topleft', 'topleft', 'topleft')
ticks = list(
    c(70, 75, 80, 85, 90, 95),
    c(0.25, 0.4, 0.5, 0.6, 0.75),
    c(0, 2.5, 5, 7.5, 10, 12.5, 15),
    c(0, 5, 10, 15, 20, 25, 31),
    c(0, 1, 2, 3, 4, 5),
    c(25, 50, 100, 150, 200, 225),
    c(0.2, 0.4, 0.6, 0.8)
)
labels = list(
    c('< 70', 75, 80, 85, 90, '> 95'),
    c('< 0.3', 0.4, 0.5, 0.6, '> 0.7'),
    c(0, 2.5, 5, 7.5, 10, 12.5, '> 15'),
    c(0, 5, 10, 15, 20, 25, '> 27'),
    c(0, 1, 2, 3, 4, 5),
    c('< 50', 50, 100, 150, 200, '> 200'),
    c('< 0.2', 0.4, 0.6, '> 0.8')
)
names(properties_display) = properties
names(legend_pos) = properties
names(ticks) = properties
names(labels) = properties

results = read.delim(results_file)
stats = read.delim(stats_file)
stats$Flanking = NULL
results = merge(results, stats)

results$Genic = results$Gene.Position != ''
results$Paired.Bases.PC = (results$Paired.Bases*2)/(results$Aligned.RNA.Length*results$Effective.Species..Flanking.)

# Convert the properties to a limited range of binned values,
# whereby the property becomes the mid-point of the range.
attach(results)

results$MPI = ((floor(MPI/20))*20)+2.5
results$MPI[MPI < 20] = 10
results$MPI[MPI >= 90] = 95

results$GC.Content = ((floor(GC.Content*20))/20)+0.025
results$GC.Content[GC.Content < 0.3] = 0.25
results$GC.Content[GC.Content >= 0.7] = 0.75

results$Gaps = ((floor(Gaps/10))*10)+5
results$Gaps[Gaps >= 50] = 60

results$Effective.Species..Flanking.[Effective.Species..Flanking. >= 5 & Effective.Species..Flanking. < 8] = 5
results$Effective.Species..Flanking.[Effective.Species..Flanking. >= 8 & Effective.Species..Flanking. < 13] = 10
results$Effective.Species..Flanking.[Effective.Species..Flanking. >= 13 & Effective.Species..Flanking. < 18] = 15
results$Effective.Species..Flanking.[Effective.Species..Flanking. >= 18 & Effective.Species..Flanking. < 23] = 20
results$Effective.Species..Flanking.[Effective.Species..Flanking. >= 23 & Effective.Species..Flanking. < 28] = 25
results$Effective.Species..Flanking.[Effective.Species..Flanking. >= 28] = 31

results$Adjusted.Tree.Length = ((floor(Adjusted.Tree.Length*2))/2)+0.25

results$RNA.Length[RNA.Length < 50] = 25
results$RNA.Length[RNA.Length >= 50 & RNA.Length < 75] = 62.5
results$RNA.Length[RNA.Length >= 75 & RNA.Length < 100] = 87.5
results$RNA.Length[RNA.Length >= 100 & RNA.Length < 125] = 112.5
results$RNA.Length[RNA.Length >= 125 & RNA.Length < 150] = 137.5
results$RNA.Length[RNA.Length >= 150 & RNA.Length < 175] = 162.5
results$RNA.Length[RNA.Length >= 175 & RNA.Length < 200] = 187.5
results$RNA.Length[RNA.Length >= 200] = 225

results$Paired.Bases.PC = ((floor(Paired.Bases.PC*10))/10)+0.05
results$Paired.Bases.PC[Paired.Bases.PC < 0.2] = 0.1
results$Paired.Bases.PC[Paired.Bases.PC >= 0.8] = 0.8

detach(results)

for (flanking in flankings) {
  for (dataset in datasets) {
    filtered = subset(results, results$Flanking == flanking & results$Dataset == dataset & results$Executed == 1)
    
    # If an RNA gene is predicted that does not overlap the RNA region at all
    # we ignore it, since we've already removed sequences that appeared to
    # have RNA in the flanking regions.
    filtered[filtered$Randomised == 0 & filtered$Sensitivity == 0, "RNA.Gene"] = 0;
    
    #filtered = subset(results_dataset, results_dataset$Software == software[1])
    #if (length(software) > 1) {
    #    for (counter in c(2:length(software))) {
    #        filtered = rbind(filtered, subset(results_dataset, results_dataset$Software == software[counter]))
    #    }
    #}
    
    pos_dataset = subset(filtered, filtered$Randomised == 0)
    neg_dataset = subset(filtered, filtered$Randomised == 1)
    
    if (nrow(pos_dataset) == 0 || nrow(neg_dataset) == 0) {
        stop('Insufficient data for analysis.')
    }
    
    for (property in properties) {
      true_pos = aggregate(pos_dataset['RNA.Gene'], list(Software=pos_dataset$Software, Property=pos_dataset[,property]), sum)
      pos_total = aggregate(pos_dataset['RNA.Gene'], list(Software=pos_dataset$Software, Property=pos_dataset[,property]), length)
      false_pos = aggregate(neg_dataset['RNA.Gene'], list(Software=neg_dataset$Software, Property=neg_dataset[,property]), sum)
      neg_total = aggregate(neg_dataset['RNA.Gene'], list(Software=neg_dataset$Software, Property=neg_dataset[,property]), length)
      
      # It's handy to have the stats broken down by conservation, but
      # presenting the data is a bit tricky, because the plots get
      # confusing. So plot all the data, and save the break-down in the
      # accompanying text file.
      true_pos_con = aggregate(pos_dataset['RNA.Gene'], list(Software=pos_dataset$Software, Property=pos_dataset[,property], Genic=pos_dataset$Genic), sum)
      pos_total_con = aggregate(pos_dataset['RNA.Gene'], list(Software=pos_dataset$Software, Property=pos_dataset[,property], Genic=pos_dataset$Genic), length)
      false_pos_con = aggregate(neg_dataset['RNA.Gene'], list(Software=neg_dataset$Software, Property=neg_dataset[,property], Genic=neg_dataset$Genic), sum)
      neg_total_con = aggregate(neg_dataset['RNA.Gene'], list(Software=neg_dataset$Software, Property=neg_dataset[,property], Genic=neg_dataset$Genic), length)
      true_pos = rbind(true_pos_con, cbind(true_pos, Genic=NA))
      pos_total = rbind(pos_total_con, cbind(pos_total, Genic=NA))
      false_pos = rbind(false_pos_con, cbind(false_pos, Genic=NA))
      neg_total = rbind(neg_total_con, cbind(neg_total, Genic=NA))
      
      false_neg = pos_total['RNA.Gene'] - true_pos['RNA.Gene']
      true_neg = neg_total['RNA.Gene'] - false_pos['RNA.Gene']
      
      pos_summary = cbind(true_pos, false_neg)
      neg_summary = cbind(false_pos, true_neg)
      names(pos_summary) = c('Software', property, 'Genic', 'TP', 'FN')
      names(neg_summary) = c('Software', property, 'Genic', 'FP', 'TN')
      summary = merge(pos_summary, neg_summary)
      attach(summary)
      
      # There's redundancy here, but it makes later analysis easier.
      # The sapply bit replaces any NaNs with zeroes
      sensitivity = sapply(TP/(TP+FN), function(x) {x[is.na(x)] <- 0; x})
      specificity = sapply(TN/(TN+FP), function(x) {x[is.na(x)] <- 0; x})
      tp_rate = sensitivity
      fp_rate = 1-specificity
      recall = sensitivity
      precision = sapply(TP/(TP+FP), function(x) {x[is.na(x)] <- 0; x})
      f_measure = sapply(2*((precision * recall)/(precision + recall)), function(x) {x[is.na(x)] <- 0; x})
      
      tpr_ci = wilson_ci(TP, TP+FN)
      names(tpr_ci) = c("tpr_ci_lower", "tpr_ci_upper")
      fpr_ci = wilson_ci(FP, TN+FP)
      names(fpr_ci) = c("fpr_ci_lower", "fpr_ci_upper")
      prec_ci = wilson_ci(TP, TP+FP)
      names(prec_ci) = c("prec_ci_lower", "prec_ci_upper")
      
      # For plotting, we scale the points proportionally, according to
      # the size of each group.
      radii_scaling = (max(ticks[[property]]) - min(ticks[[property]]))/20
      radii = sqrt((TP+FN)/pi)
      radii = (radii/max(radii)) * radii_scaling
      
      summary = cbind(summary, sensitivity, specificity, tp_rate, fp_rate, recall, precision, f_measure, tpr_ci, fpr_ci, prec_ci)
      remove(sensitivity, specificity, tp_rate, fp_rate, recall, precision, f_measure, tpr_ci, fpr_ci, prec_ci)
      detach(summary)
      attach(summary)
      
      text_file = paste(basename, property, flanking, dataset, sep='_')
      text_file = paste(text_file, 'txt', sep='.')
      text_file = paste(plot_dir, text_file, sep='/')
      plot_file = paste(basename, property, flanking, dataset, sep='_')
      plot_file = paste(plot_file, 'pdf', sep='.')
      plot_file = paste(plot_dir, plot_file, sep='/')
      
      f = file(text_file, open='wb')
      write.table(summary, file=f, quote=FALSE, sep='\t', eol='\n', row.names=FALSE)
      close(f)
      
      pdf(plot_file, width=9, height=5)
      dev.control(displaylist="enable")
      par(mai=c(1, 1, 0.25, 0.25), mfrow=c(1,2), las=1)
      
      plot(
          x = 0,
          y = 0,
          type = 'n',
          xlim = c(min(ticks[[property]]), max(ticks[[property]])),
          ylim = c(0, 1),
          xlab = properties_display[[property]],
          xaxt = 'n',
          ylab = 'Recall',
          cex.lab = 1.25,
          cex.axis = 1.25
      )
      axis(
          side = 1,
          at = ticks[[property]],
          labels = labels[[property]],
          cex.lab = 1.25,
          cex.axis = 1.25
      )
      
      for (sw in software) {
        symbols(
            x = summary[,property][Software == sw & is.na(Genic)],
            y = recall[Software == sw & is.na(Genic)],
            circles = radii[Software == sw & is.na(Genic)], add=TRUE, inches=FALSE,
            fg = software_colours[[sw]], bg = software_colours[[sw]]
        )
      }
      
      plot(
          x = 0,
          y = 0,
          type = 'n',
          xlim = c(min(ticks[[property]]), max(ticks[[property]])),
          ylim = c(0, 1),
          xlab = properties_display[[property]],
          xaxt = 'n',
          ylab = 'Precision',
          cex.lab = 1.25,
          cex.axis = 1.25
      )
      axis(
          side = 1,
          at = ticks[[property]],
          labels = labels[[property]],
          cex.lab = 1.25,
          cex.axis = 1.25
      )
      
      legend('topright', software_display, col=software_colours, bg='white', pch=15, cex=1.25, pt.cex=1.5)
      
      for (sw in software) {
        symbols(
            x = summary[,property][Software == sw & is.na(Genic)],
            y = precision[Software == sw & is.na(Genic)],
            circles = radii[Software == sw & is.na(Genic)], add=TRUE, inches=FALSE,
            fg = software_colours[[sw]], bg = software_colours[[sw]]
        )
      }
      
      dev.off()
      detach(summary)
    }
  }
}


