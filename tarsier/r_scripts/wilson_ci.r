# Calculate the 95% confidence interval of a proportion.
# I only needed a simple function, but if you want a choice
# of confidence levels, or some alternative methods for
# calculating confidence intervals, there's an R package
# called PropCIs (which I haven't tried):
# http://cran.r-project.org/web/packages/PropCIs

wilson_ci = function(p, n) {
    q = n - p
    lambda = 1.96
    
    midpoint = (p + (lambda^2 / 2)) / (n + lambda^2)
    ci_bound = lambda * ((sqrt((p * q / n) + (lambda^2 / 4)))/(n + lambda^2))
    ci_lower = midpoint - ci_bound
    ci_upper = midpoint + ci_bound
    ci = data.frame(ci_lower, ci_upper)
    ci
}

