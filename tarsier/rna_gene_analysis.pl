#!/usr/bin/perl
use strict;
use warnings;
use Carp;

=pod

=head1 Name

rna_gene_analysis.pl dir data_file

=head1 Description

Get the files required for RNA gene prediction in one place; then
either run the programs, or defer execution to a cluster. This keeps
the genomic alignments and the analyses separate, which may be useful
when processing the results, although there is a degree of duplicated data.

=head1 Author

James Allen
(james@monkeyshines.co.uk)

=head1 Copyright

Copyright 2012 James Allen; University of Manchester

=head1 Licence

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program. If not, see <http://www.gnu.org/licenses/>.

=cut

use File::Path qw(make_path);

use MonkeyShines::Utils qw (read_from_delim save_to_file);

################################################################################
# Initialise Parameters - Start
my $dir = $ARGV[0];
croak "Directory must be specified" unless $dir;
croak "Directory '$dir' does not exist" unless -e $dir;

my $data_file = $ARGV[1];
croak "Data file must be specified" unless $data_file;
croak "File '$data_file' does not exist" unless -e $data_file;

my $bionj = 1;
my $bionj_txt = $bionj ? '.bionj' : '';

my $data_dir = "$dir/data";
my $pred_dir = "$dir/prediction$bionj_txt";
mkdir($pred_dir) unless -e $pred_dir;

my @flanking = (0, 400);
my $cluster = 0;
my $pred_script_dir = $ENV{'HOME'}."/MonkeyShines/tarsier";
# Initialise Parameters - End
################################################################################

################################################################################
# Collate PHASE Input - Start
my $ids_file = "$pred_dir/ids.txt";
my $ids;

my ($data, $cols) = read_from_delim($data_file, "\t", 1);
foreach my $line (@$data) {
	my $id = $$line[$$cols{'ID'}];
	my $chr = $$line[$$cols{'Chr'}];
	my $location = $$line[$$cols{'Location'}];
	my $dataset = $$line[$$cols{'Dataset'}];
	
	$ids .= "$id\_$chr\_$location\_$dataset\n";
	foreach my $flanking (@flanking) {
		my $old_dir = "$data_dir/$id/$chr/$location/$dataset";
		my $new_dir = "$pred_dir/$id\_$chr\_$location\_$dataset";
		make_path($new_dir) unless -e $new_dir;
		
		`cp $old_dir/$flanking.* $new_dir/.`;
		`cp $old_dir/$flanking\_random*.* $new_dir/.`;
	}
}
save_to_file($ids, $ids_file);
# Collate PHASE Input - End
################################################################################

################################################################################
# Execute Prediction, or Setup for Cluster - Start
if ($cluster) {
	my $alns = () = $ids =~ /\n/gm;
	my $sh_file = "$pred_dir/rna_gene_prediction.sh";
	my $sh =
		'#!/bin/bash'."\n".
		'#$ -cwd'."\n".
		'#$ -S /bin/bash'."\n".
		'#$ -V'."\n".
		'#$ -e $HOME/scratch'."\n".
		'#$ -o $HOME/scratch'."\n".
		'#$ -t 1-'."$alns\n\n".
		'ID=$(awk "NR==$SGE_TASK_ID" '."$ids_file)\n";
	foreach my $flanking (@flanking) {
		$sh .= "$pred_script_dir/rna_gene_prediction.pl --alignment '$pred_dir/\$ID/$flanking.maf' --tree '$pred_dir/\$ID/$flanking$bionj_txt.nh' --out '$pred_dir/\$ID' --verbose\n";
		$sh .= "$pred_script_dir/rna_gene_prediction.pl --alignment '$pred_dir/\$ID/$flanking\_random_1.maf' --tree '$pred_dir/\$ID/$flanking$bionj_txt.nh' --out '$pred_dir/\$ID' --verbose\n";
		$sh .= "$pred_script_dir/rna_gene_prediction.pl --alignment '$pred_dir/\$ID/$flanking\_random_2.maf' --tree '$pred_dir/\$ID/$flanking$bionj_txt.nh' --out '$pred_dir/\$ID' --verbose\n";
		$sh .= "$pred_script_dir/rna_gene_prediction.pl --alignment '$pred_dir/\$ID/$flanking\_random_3.maf' --tree '$pred_dir/\$ID/$flanking$bionj_txt.nh' --out '$pred_dir/\$ID' --verbose\n";
		$sh .= "$pred_script_dir/rna_gene_prediction.pl --alignment '$pred_dir/\$ID/$flanking\_random_4.maf' --tree '$pred_dir/\$ID/$flanking$bionj_txt.nh' --out '$pred_dir/\$ID' --verbose\n";
		$sh .= "$pred_script_dir/rna_gene_prediction.pl --alignment '$pred_dir/\$ID/$flanking\_random_5.maf' --tree '$pred_dir/\$ID/$flanking$bionj_txt.nh' --out '$pred_dir/\$ID' --verbose\n";
		$sh .= "$pred_script_dir/rna_gene_prediction.pl --alignment '$pred_dir/\$ID/$flanking\_random_6.maf' --tree '$pred_dir/\$ID/$flanking$bionj_txt.nh' --out '$pred_dir/\$ID' --verbose\n";
		$sh .= "$pred_script_dir/rna_gene_prediction.pl --alignment '$pred_dir/\$ID/$flanking\_random_7.maf' --tree '$pred_dir/\$ID/$flanking$bionj_txt.nh' --out '$pred_dir/\$ID' --verbose\n";
		$sh .= "$pred_script_dir/rna_gene_prediction.pl --alignment '$pred_dir/\$ID/$flanking\_random_8.maf' --tree '$pred_dir/\$ID/$flanking$bionj_txt.nh' --out '$pred_dir/\$ID' --verbose\n";
		$sh .= "$pred_script_dir/rna_gene_prediction.pl --alignment '$pred_dir/\$ID/$flanking\_random_9.maf' --tree '$pred_dir/\$ID/$flanking$bionj_txt.nh' --out '$pred_dir/\$ID' --verbose\n";
		$sh .= "$pred_script_dir/rna_gene_prediction.pl --alignment '$pred_dir/\$ID/$flanking\_random_10.maf' --tree '$pred_dir/\$ID/$flanking$bionj_txt.nh' --out '$pred_dir/\$ID' --verbose\n";
	}
	
	save_to_file($sh, $sh_file);
	print "Template cluster (array) executable saved in $sh_file.\n".
		"Edit the directory locations and options as required.\n";
} else {
	opendir(PHASE_DIR, $pred_dir);
	foreach my $aln (sort grep { /^[^\.]/ } readdir(PHASE_DIR)) {
		next unless -d "$pred_dir/$aln";
		foreach my $flanking (@flanking) {
			print `perl $pred_script_dir/rna_gene_prediction.pl --alignment $pred_dir/$aln/$flanking.maf --tree $pred_dir/$aln/$flanking$bionj_txt.nh --out $pred_dir/$aln --verbose`;
			print `perl $pred_script_dir/rna_gene_prediction.pl --alignment $pred_dir/$aln/$flanking\_random_1.maf --tree $pred_dir/$aln/$flanking$bionj_txt.nh --out $pred_dir/$aln --verbose`;
			print `perl $pred_script_dir/rna_gene_prediction.pl --alignment $pred_dir/$aln/$flanking\_random_2.maf --tree $pred_dir/$aln/$flanking$bionj_txt.nh --out $pred_dir/$aln --verbose`;
			print `perl $pred_script_dir/rna_gene_prediction.pl --alignment $pred_dir/$aln/$flanking\_random_3.maf --tree $pred_dir/$aln/$flanking$bionj_txt.nh --out $pred_dir/$aln --verbose`;
			print `perl $pred_script_dir/rna_gene_prediction.pl --alignment $pred_dir/$aln/$flanking\_random_4.maf --tree $pred_dir/$aln/$flanking$bionj_txt.nh --out $pred_dir/$aln --verbose`;
			print `perl $pred_script_dir/rna_gene_prediction.pl --alignment $pred_dir/$aln/$flanking\_random_5.maf --tree $pred_dir/$aln/$flanking$bionj_txt.nh --out $pred_dir/$aln --verbose`;
			print `perl $pred_script_dir/rna_gene_prediction.pl --alignment $pred_dir/$aln/$flanking\_random_6.maf --tree $pred_dir/$aln/$flanking$bionj_txt.nh --out $pred_dir/$aln --verbose`;
			print `perl $pred_script_dir/rna_gene_prediction.pl --alignment $pred_dir/$aln/$flanking\_random_7.maf --tree $pred_dir/$aln/$flanking$bionj_txt.nh --out $pred_dir/$aln --verbose`;
			print `perl $pred_script_dir/rna_gene_prediction.pl --alignment $pred_dir/$aln/$flanking\_random_8.maf --tree $pred_dir/$aln/$flanking$bionj_txt.nh --out $pred_dir/$aln --verbose`;
			print `perl $pred_script_dir/rna_gene_prediction.pl --alignment $pred_dir/$aln/$flanking\_random_9.maf --tree $pred_dir/$aln/$flanking$bionj_txt.nh --out $pred_dir/$aln --verbose`;
			print `perl $pred_script_dir/rna_gene_prediction.pl --alignment $pred_dir/$aln/$flanking\_random_10.maf --tree $pred_dir/$aln/$flanking$bionj_txt.nh --out $pred_dir/$aln --verbose`;
		}
	}
	closedir(PHASE_DIR);
}
# Execute Prediction, or Setup for Cluster - End
################################################################################

