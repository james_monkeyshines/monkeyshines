Perl modules and scripts for bioinformatics, including modules for RNA gene
analysis and alignment visualisation.

There's no installation procedure as such, you just need to tell Perl where
the MonkeyShines libraries are. If <dir> is the directory where you pulled
from git (or unzipped the tar.gz file), then add
'<dir>/MonkeyShines/lib' to your Perl library path. In bash, this is done
with the following (replace $HOME with <dir> if necessary):
  export PERL5LIB=$PERL5LIB:$HOME/MonkeyShines/lib
Save this command in $HOME/.bashrc to load it automatically when you log on.

The modules are documented in 'MonkeyShines/doc/Monkeyshines', and there are
some tests in 'MonkeyShines/t' which may serve as usage examples. The main
purpose of the modules for me is their use in the Marmoset and Tarsier
pipelines, which gather genomic alignments of RNA genes with a view to
better understanding their evolution and improving prediction of novel genes.
These pipelines are complex and are described in full in my PhD thesis, but
the directories for each pipeline ('MonkeyShines/marmoset' and
'MonkeyShines/tarsier') contain basic information, and the scripts that
constitute the pipeline include some pod and comments.

The MonkeyShines code relies heavily on software and libraries developed by
others; in particular the Marmoset pipeline gathers data from Ensembl, so
you'll probably want to start by installing the Ensembl Perl API, which is
mercifully easy: http://www.ensembl.org/info/docs/api/api_installation.html.
Marmoset uses the 'ensembl' and 'ensembl-compara' packages. BioPerl is also
required; instructions for installation are given on the Ensembl API page.
I found a bug in a BioPerl module, but the fix might not have made it to
your version of BioPerl; you can either install the latest bioperl-live
(https://github.com/bioperl/bioperl-live), or ensure that your copy of
Bio::SimpleAlign has this fix: https://redmine.open-bio.org/issues/3269.

