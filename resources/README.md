This directory contains scripts written by others that are used in the
Marmoset and Tarsier pipelines. The software is all open source, but
might not be released under the same GPL licence that I use, so please
refer to the original authors' documentation for details.

