#!/usr/bin/perl
use strict;
use warnings;
use Carp;

=pod

=head1 Name

flanking_rna.pl dir data_file [mode]

=head1 Description

If <mode> is "input" (the default), generate a set of fasta files
containing the human flanking sequences, which need to be submitted
to Rfam to search for RNA (http://rfam.sanger.ac.uk/search/batch).
The Rfam search requires short IDs, so to prevent inadvertently
created duplicates, assign a number to each location, and store
the mapping in a file. When the results are returned, they need to
be saved in a file with the same name as the submitted fasta file,
but with the suffix ".out" rather than ".fa".

If <mode> is "output", the assumption is that the Rfam search results
have been saved, and these will be processed to integrate the
information with that in <data_file>.

=head1 Author

James Allen
(james@monkeyshines.co.uk)

=head1 Copyright

Copyright 2012 James Allen; University of Manchester

=head1 Licence

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program. If not, see <http://www.gnu.org/licenses/>.

=cut

use File::Path qw(make_path);

use MonkeyShines::Sequence qw (from_fasta);
use MonkeyShines::Utils qw (read_from_file read_from_delim read_from_delim_column save_to_file);

################################################################################
# Initialise Parameters - Start
my $dir = $ARGV[0];
croak "Directory must be specified" unless $dir;
croak "Directory '$dir' does not exist" unless -e $dir;

my $data_file = $ARGV[1];
croak "Data file must be specified" unless $data_file;
croak "File '$data_file' does not exist" unless -e $data_file;

my $mode = $ARGV[2];
$mode = 'input' unless $mode;

my $flanking_rna_dir = "$dir/output/flanking_rna";
make_path($flanking_rna_dir) unless -e $flanking_rna_dir;

my @datasets = ('epo', 'epo_low_coverage', 'multiz');
my $flanking = 400;
my $ref_species = 'Homo_sapiens';

my $seqs_per_file = 150;
# Initialise Parameters - End
################################################################################

################################################################################
# Search for RNA in Human Flanking Sequences - Start
my @columns = ('Unique ID', 'ID', 'Chr', 'Location');

my ($header) = read_from_file($data_file) =~ /^(.*)$/m;
my $col_count;
my $output_file = "$dir/output/flanking_rna.txt";
save_to_file("$header\tDownstream RNA\tUpstream RNA", $output_file);

foreach my $dataset (@datasets) {
	my $id_map_file = "$flanking_rna_dir/flanking_id_mapping_$dataset.txt";
	my $id_map;
	my $parsed;
	
	if ($mode eq 'input') {
		save_to_file(join("\t", @columns), $id_map_file);
	} else {
		$id_map = read_from_delim_column($id_map_file, "\t", 1);
		foreach my $unique_id (keys %$id_map) {
			my $id = $$id_map{$unique_id}{'ID'}.'_'
					.$$id_map{$unique_id}{'Chr'}.'_'
					.$$id_map{$unique_id}{'Location'};
			$$parsed{$id}{'down'} = 0;
			$$parsed{$id}{'up'} = 0;
		}
	}
	
	my $seq_counter = 1;
	my $file_chunk = 1;
	my $basename = "$flanking_rna_dir/flanking_$dataset";
	my $flanking_file = "$basename\_$file_chunk.fa";
	my $flanking_data = '';
	
	if ($mode ne 'input') {
		(my $results_file = $flanking_file) =~ s/fa$/out/;
		parse_results($id_map, $results_file, $parsed);
	}
	
	my ($data, $cols) = read_from_delim($data_file, "\t", 1);
	$col_count = scalar(keys %$cols);
	my %current_data;
	foreach my $line (@$data) {
		my $id = $$line[$$cols{'ID'}];
		my $chr = $$line[$$cols{'Chr'}];
		my $location = $$line[$$cols{'Location'}];
		my $file_dataset = $$line[$$cols{'Dataset'}];
		next unless $dataset eq $file_dataset;
		$current_data{"$id\_$chr\_$location"} = $line;
		my $aln_file = "$dir/data/$id/$chr/$location/$dataset/$flanking.fa";
		
		if ($seq_counter > 1 && $seq_counter % $seqs_per_file == 1) {
			if ($mode eq 'input') {
				save_to_file($flanking_data, $flanking_file);
				$flanking_data = '';
			}
			$file_chunk++;
			$flanking_file = "$basename\_$file_chunk.fa";
			if ($mode ne 'input') {
				(my $results_file = $flanking_file) =~ s/fa$/out/;
				parse_results($id_map, $results_file, $parsed);
			}
		}
		
		if ($mode eq 'input') {
			$id_map .= "\n$seq_counter\t$id\t$chr\t$location";
			my ($seqs, undef) = from_fasta(read_from_file($aln_file));
			my $ref_seq = $$seqs{$ref_species};
			my ($up, $down) = $ref_seq =~ /^(.{400}).+(.{400})$/;
			$up =~ s/\-//gm;
			$down =~ s/\-//gm;
			
			if (length($up) > 1) {
				$flanking_data .= ">up_$seq_counter\n$up\n";
			} else {
				warn "Zero- or single-length upstream flanking for $id, $chr:$location.\n";
			}
			if (length($down) > 1) {
				$flanking_data .= ">down_$seq_counter\n$down\n";
			} else {
				warn "Zero- or single-length downstream flanking for $id, $chr:$location.\n";
			}
		}
		$seq_counter++;
	}
	if ($mode eq 'input') {
		save_to_file($flanking_data, $flanking_file);
	}
	$file_chunk++;
	$flanking_file = "$basename\_$file_chunk.fa";
	
	if ($mode eq 'input') {
		save_to_file($id_map, $id_map_file, 'append');
	} else {
		my $output;
		foreach my $id (sort keys %$parsed) {
			my $trailing_tabs = "\t" x ($col_count - scalar(@{$current_data{$id}}));
			$output .= "\n".join("\t", @{$current_data{$id}})."$trailing_tabs\t".$$parsed{$id}{'down'}."\t".$$parsed{$id}{'up'};
		}
		save_to_file($output, $output_file, 'append');
	}
}

sub parse_results {
	my ($id_map, $results_file, $parsed) = @_;
	my $results = read_from_file($results_file);
	my @results = $results =~ /^\S+\s+\S+\s+(\w+_\d+)\s/gm;
	foreach my $result (@results) {
		my ($stream, $unique_id) = split(/_/, $result);
		my $id = $$id_map{$unique_id}{'ID'}.'_'
				.$$id_map{$unique_id}{'Chr'}.'_'
				.$$id_map{$unique_id}{'Location'};
		$$parsed{$id}{$stream}++;
	}
}
# Search for RNA in  Human Flanking Sequences - End
################################################################################

