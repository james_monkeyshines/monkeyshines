#!/usr/bin/perl
use strict;
use warnings;
use Carp;

=pod

=head1 Name

epo_multiz_comparison.pl dir data_file

=head1 Description

Calculate the difference between EPO alignments and MultiZ alignments.
This is hard to do directly, since the species may not match; but if we
do pairwise analysis of the common species it will tell us whether the
alignments are at least of similar sequences.
Requires 'needle', from the EMBOSS suite, to be in your path
(http://emboss.sourceforge.net/).

=head1 Author

James Allen
(james@monkeyshines.co.uk)

=head1 Copyright

Copyright 2012 James Allen; University of Manchester

=head1 Licence

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program. If not, see <http://www.gnu.org/licenses/>.

=cut

use MonkeyShines::Sequence qw (from_fasta);
use MonkeyShines::Utils qw (read_from_delim read_from_file save_to_file);

################################################################################
# Initialise Parameters - Start
my $dir = $ARGV[0];
croak "Directory must be specified" unless $dir;
croak "Directory '$dir' does not exist" unless -e $dir;

my $data_file = $ARGV[1];
croak "Data file must be specified" unless $data_file;
croak "File '$data_file' does not exist" unless -e $data_file;

my $output_dir = "$dir/output";

my $overwrite = 0;
my @flanking = (0, 400);
my $dataset = 'epo';
# Initialise Parameters - End
################################################################################

################################################################################
# Output File - Start
my $output_file = "$output_dir/epo_multiz_comparison.txt";
if (!-e $output_file || $overwrite) {
	my @columns = ('ID', 'Chr', 'Location', 'Flanking',
		'Dataset 1', 'Dataset 2', 'Species', 'NW Score', 'Identity',
		'External Gaps', 'Internal Gaps', 'Gaps',
		'Different', 'Total', 'Different %age');
	save_to_file(join("\t", @columns), $output_file);
}
my $existing_output = read_from_file($output_file);
# Output File - End
################################################################################

################################################################################
# Compare EPO and MultiZ Sequences - Start
my ($tmp_file1, $tmp_file2) = ('1.tmp', '2.tmp');
my ($data, $cols) = read_from_delim($data_file, "\t", 1);
foreach my $line (@$data) {
	my $id = $$line[$$cols{'ID'}];
	my $chr = $$line[$$cols{'Chr'}];
	my $location = $$line[$$cols{'Location'}];
	my $file_dataset = $$line[$$cols{'Dataset'}];
	next unless $file_dataset eq $dataset;
	
	foreach my $flanking (@flanking) {
		my $epo_file = "$dir/data/$id/$chr/$location/epo/$flanking.fa";
		croak "Missing EPO file: '$epo_file'" unless -e $epo_file;
		
		my $processed = $existing_output =~ /^$id\t$chr\t$location\t$flanking\tepo\tmultiz/m;
		if (!$processed || $overwrite) {
			my $multiz_file = "$dir/data/$id/$chr/$location/multiz/$flanking.fa";
			if (-e $multiz_file) {
				my ($epo_seqs, undef) = from_fasta(read_from_file($epo_file));
				my ($multiz_seqs, undef) = from_fasta(read_from_file($multiz_file));
				
				foreach my $species (keys %$epo_seqs) {
					next unless exists($$multiz_seqs{$species});
					my $seq1 = $$epo_seqs{$species};
					my $seq2 = $$multiz_seqs{$species};
					$seq1 =~ s/\-//g;
					$seq2 =~ s/\-//g;
					save_to_file($seq1, $tmp_file1);
					save_to_file($seq2, $tmp_file2);
					my $needle = `needle -auto -stdout -aformat markx10 $tmp_file1 $tmp_file2`;
					
					my ($score) = $needle =~ /^#\s+Score:\s+(\S+)/m;
					my ($identity, $total) = $needle =~ /^#\s+Identity:\s+(\d+)\/(\d+)/m;
					my ($gaps) = $needle =~ /^#\s+Gaps:\s+(\d+)/m;
					
					my @seqs = $needle =~ /^([ACGT\-][ACGT\-\n]+)/gm;
					map { $_ =~ s/\n//gm } @seqs;
					my $external_gaps = 0;
					foreach my $seq (@seqs) {
						my ($external) = $seq =~ /^(\-+)/;
						$external_gaps += length($external) if $external;
						($external) = $seq =~ /(\-+)$/;
						$external_gaps += length($external) if $external;
					}
					
					my $output = "\n$id\t$chr\t$location\t$flanking".
						"\tepo\tmultiz\t$species\t$score\t$identity".
						"\t$external_gaps\t".($gaps-$external_gaps)."\t$gaps".
						"\t".($total-$identity-$external_gaps).
						"\t$total\t".(($total-$identity-$external_gaps)/$total);
					save_to_file($output, $output_file, 'append');
				}
			} else {
				carp "EPO file '$epo_file' has no MultiZ counterpart";
				my $output = "\n$id\t$chr\t$location\t$flanking".
					"\tepo\tmultiz\t\t\t\t\t\t\t\t\t";
				save_to_file($output, $output_file, 'append');
			}
		}
	}
}
unlink($tmp_file1, $tmp_file2);
# Compare EPO and MultiZ Sequences - End
################################################################################

