#!/usr/bin/perl
use strict;
use warnings;
use Carp;

=pod

=head1 Name

tree_evaluation.pl dir data_file

=head1 Description

Perform SH/AU tests on a set of results from PHASE. If a BIONJ tree was
created with 'bionj.pl', that'll be included as well as the tree associated
with the genomic alignment. 

=head1 Author

James Allen
(james@monkeyshines.co.uk)

=head1 Copyright

Copyright 2012 James Allen; University of Manchester

=head1 Licence

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program. If not, see <http://www.gnu.org/licenses/>.

=cut

use File::Path qw(make_path);

use MonkeyShines::Utils qw(read_from_file read_from_delim save_to_file);

################################################################################
# Initialise Parameters - Start
my $dir = $ARGV[0];
croak "Directory must be specified" unless $dir;
croak "Directory '$dir' does not exist" unless -e $dir;

my $data_file = $ARGV[1];
croak "Data file must be specified" unless $data_file;
croak "File '$data_file' does not exist" unless -e $data_file;

my $bionj = 1;
my $bionj_txt = $bionj ? '.bionj' : '';

my $phase_dir = "$dir/phase$bionj_txt";
my $output_dir = "$dir/output";

my $flanking = 0;
my $phase_script_dir = $ENV{'HOME'}."/PHASE-2.1/scripts";
my $p_value = 0.05;
# Initialise Parameters - End
################################################################################

################################################################################
# Output File - Start
my $output_file = "$output_dir/tree_evaluation.txt";
#if (!-e $output_file || $overwrite) {
	my @output_cols = (
		'ID', 'Chr', 'Location', 'Dataset', 'Flanking',
		'RNA/HKY+G', 'RNA/Reference', 'RNA/BIONJ', 'RNA/Gene', 'RNA/ML',
		'Gene/Reference', 'ML/Reference'
	);
	save_to_file(join("\t", @output_cols), $output_file);
#}
#my $existing_output = read_from_file($output_file);
# Output File - End
################################################################################

################################################################################
# Perform SH/AU Tests - Start
my ($data, $cols) = read_from_delim($data_file, "\t", 1);
foreach my $line (@$data) {
	my $id = $$line[$$cols{'ID'}];
	my $chr = $$line[$$cols{'Chr'}];
	my $location = $$line[$$cols{'Location'}];
	my $dataset = $$line[$$cols{'Dataset'}];
	my $aln_dir = "$phase_dir/$id\_$chr\_$location\_$dataset";
	my $ids_file = "$aln_dir/$flanking.ids";
	my $trees_file = "$aln_dir/$flanking.trees";
	
	my ($ids, $trees);
	opendir(ALN_DIR, $aln_dir);
	foreach my $tree_file (sort grep { /\.nh$/ } readdir(ALN_DIR)) {
		my $tree = read_from_file("$aln_dir/$tree_file");
		$ids .= "$tree_file\n";
		chomp($tree);
		$tree =~ s/:\d+\.\d+//g;
		$trees .= "$tree\n";
	}
	closedir(ALN_DIR);
	save_to_file($trees, $trees_file);
	save_to_file($ids, $ids_file);
	print `perl $phase_script_dir/tree_evaluation.pl --alignment $aln_dir/$flanking.fa --tree $trees_file -ids $ids_file --out $aln_dir --verbose`;
}

foreach my $line (@$data) {
	my $id = $$line[$$cols{'ID'}];
	my $chr = $$line[$$cols{'Chr'}];
	my $location = $$line[$$cols{'Location'}];
	my $dataset = $$line[$$cols{'Dataset'}];
	my $aln_dir = "$phase_dir/$id\_$chr\_$location\_$dataset";
	my $results_file = "$aln_dir/tree_evaluation.txt";
	
	my $results = read_from_file($results_file);
	my %au = $results =~ /^(0\S+).+?(\S+)$/gm;
	my ($best) = $results =~ /^(0\.consensus_\S+_\S+\.nh)/m;
	$best = "$flanking.consensus_HKY85+G.nh" unless $best;
	next unless exists $au{$best};
	
	my $best_au = $au{$best};
	my ($hky, $ref, $bionj, $gene, $ml, $gene_ref, $ml_ref) = (0, 0, 0, '', 0, '', 0);
	if ($best_au >= $p_value) {
		$hky = 1 if $au{"$flanking.consensus_HKY85+G.nh"} < $p_value;
		$ref = 1 if $au{"$flanking.nh"} < $p_value;
		$bionj = 1 if $au{"$flanking.bionj.nh"} < $p_value;
		if (exists $au{"$flanking.gene.nh"}) {
			$gene = $au{"$flanking.gene.nh"} < $p_value ? 1 : 0;
		}
		$ml = 1 if $au{"$flanking.raxml.nh"} < $p_value;
	} else {
		$hky = 1 if $au{"$flanking.consensus_HKY85+G.nh"} > $p_value;
		$ref = 1 if $au{"$flanking.nh"} > $p_value;
		$bionj = 1 if $au{"$flanking.bionj.nh"} > $p_value;
		if (exists $au{"$flanking.gene.nh"}) {
			$gene = $au{"$flanking.gene.nh"} > $p_value ? 1 : 0;
		}
		$ml = 1 if $au{"$flanking.raxml.nh"} > $p_value;
	}
	if ($au{"$flanking.nh"} >= $p_value) {
		if (exists $au{"$flanking.gene.nh"}) {
			$gene_ref = $au{"$flanking.gene.nh"} < $p_value ? 1 : 0;
		}
		$ml_ref = 1 if $au{"$flanking.raxml.nh"} < $p_value;
	} else {
		if (exists $au{"$flanking.gene.nh"}) {
			$gene_ref = $au{"$flanking.gene.nh"} > $p_value ? 1 : 0;
		}
		$ml_ref = 1 if $au{"$flanking.raxml.nh"} > $p_value;
	}
	
	my $output = "\n$id\t$chr\t$location\t$dataset\t$flanking";
	$output .= "\t$hky\t$ref\t$bionj\t$gene\t$ml\t$gene_ref\t$ml_ref";
	save_to_file($output, $output_file, 'append');
}
# Perform SH/AU Tests - End
################################################################################

