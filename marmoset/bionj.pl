#!/usr/bin/perl
use strict;
use warnings;
use Carp;

=pod

=head1 Name

bionj.pl dir data_file

=head1 Description

For each line in <data_file>, generate a BioNJ tree and save it
in the appropriate subdirectory of <dir>. The <data file> must have
columns headed 'ID', 'Chr', 'Location', and 'Dataset', since the
script is assuming that it will find the following directory structure:
<dir>/data/<id>/<chr>/<location>/<dataset>, which will be generated
by calls to 'genomic_rna.pl' and 'epo_alignments.pl'. The output file
of the latter script (possibly filtered to remove rows which don't
pass given QC criteria) is expected to be the <data_file>.

PhyML is used to generate the BioNJ tree, since it's interface is
easier to use than the actual BioNJ executable. To run this script
the executable 'phyml' must exist in your path; PhyML is freely
available at http://www.atgc-montpellier.fr/phyml/binaries.php.

BioNJ paper: http://www.ncbi.nlm.nih.gov/pubmed/9254330
PhyML 3.0 paper: http://www.ncbi.nlm.nih.gov/pubmed/20525638

=head1 Author

James Allen
(james@monkeyshines.co.uk)

=head1 Copyright

Copyright 2012 James Allen; University of Manchester

=head1 Licence

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program. If not, see <http://www.gnu.org/licenses/>.

=cut

use File::Path qw(make_path);

use MonkeyShines::Sequence qw (from_fasta to_phylip_s);
use MonkeyShines::Utils qw (read_from_file read_from_delim save_to_file);

################################################################################
# Initialise Parameters - Start
my $dir = $ARGV[0];
croak "Directory must be specified" unless $dir;
croak "Directory '$dir' does not exist" unless -e $dir;

my $data_file = $ARGV[1];
croak "Data file must be specified" unless $data_file;
croak "Data file '$data_file' does not exist" unless -e $data_file;

my $data_dir = "$dir/data";
my $overwrite = 0;
my @flanking = (0, 400);
# Initialise Parameters - End
################################################################################

################################################################################
# Create BioNJ Trees - Start
my ($data, $cols) = read_from_delim($data_file, "\t", 1);
foreach my $line (@$data) {
	my $id = $$line[$$cols{'ID'}];
	my $chr = $$line[$$cols{'Chr'}];
	my $location = $$line[$$cols{'Location'}];
	my $dataset = $$line[$$cols{'Dataset'}];
	
	foreach my $flanking (@flanking) {
		my $basename = "$data_dir/$id/$chr/$location/$dataset/$flanking";
		my $fasta_file = "$basename.fa";
		next unless -e $fasta_file;
		
		my $phyml_file = "$basename.phy";
		my $bionj_file = "$basename.bionj.nh";
		if (!-e $bionj_file || $overwrite) {
			my ($seqs, $order) = from_fasta(read_from_file($fasta_file));
			save_to_file(to_phylip_s($seqs, $order), $phyml_file);
			
			# Easiest way to create a BioNJ tree is to run Phyml without optimisation.
			`phyml -i $phyml_file -o n -b 0`;
			my $tree = read_from_file("$phyml_file\_phyml_tree.txt");
			# Make sure that there are no negative branch lengths.
			if ($tree =~ /\-\d+\.\d+/) {
				print "  Fixing negative branch lengths for $id\n  $tree\n";
				$tree =~ s/(\-\d+\.\d+)/0.0/g;
			}
			if ($tree =~ /e\-/) {
				print "  Check branch-length format for $id\n  $tree\n";
			}
			save_to_file($tree, $bionj_file);
			unlink($phyml_file);
			unlink("$phyml_file\_phyml_stats.txt");
			unlink("$phyml_file\_phyml_tree.txt");
		}
	}
}
# Create BioNJ Trees - End
################################################################################

