#!/usr/bin/perl
use strict;
use warnings;
use Carp;

=pod

=head1 Name

multiz_alignments.pl dir data_file

=head1 Description

Retrieve the genomic alignment for the regions specified in <data_file>,
from the MultiZ/TBA maf dataset (trimmed to match the species in the EPO
12 dataset, apart from pig which is not in the MultiZ alignment).

=head1 Author

James Allen
(james@monkeyshines.co.uk)

=head1 Copyright

Copyright 2012 James Allen; University of Manchester

=head1 Licence

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program. If not, see <http://www.gnu.org/licenses/>.

=cut

use File::Path qw(make_path);

use MonkeyShines::Marmoset qw (
	multiz_alignment
	remove_long_inserts
	remove_gappy
	crop_alignment
	gene_gain_loss
	effective_species_count
	structure_files
	phase_class_files
	randomisations
);

use MonkeyShines::Sequence qw (
	download_ucsc maf_index
	to_fasta to_maf from_maf
	flanking_lengths
);
use MonkeyShines::Tree qw (parse_tree bifurcate_tree unroot_tree print_tree leaf_nodes relabel);
use MonkeyShines::Utils	qw (read_from_file read_from_delim save_to_file simple_diff);

################################################################################
# Initialise Parameters - Start
my $dir = $ARGV[0];
croak "Directory must be specified" unless $dir;
croak "Directory '$dir' does not exist" unless -e $dir;

my $data_file = $ARGV[1];
croak "Data file must be specified" unless $data_file;
croak "Data file '$data_file' does not exist" unless -e $data_file;

my $data_dir = "$dir/data";
my $output_dir = "$dir/output";
my $maf_dir = "$data_dir/maf";
my $two_bit_dir = "$data_dir/fasta/two_bit";
my $overwrite = 0;
my $flanking = 400;
my $long_insert = 0.1;
my $max_gap = 0.25;
my $randomisations = 10;

my $ref_id = 9606;
my $ref_species = 'Homo_sapiens';
my $skip_download = 1;

# Note that Sus scrofa is not in the MultiZ 46-way alignment.
my %epo = (
	'hg19'		=>	'Homo_sapiens',
	'panTro2'	=>	'Pan_troglodytes',
	'gorGor1'	=>	'Gorilla_gorilla',
	'ponAbe2'	=>	'Pongo_abelii',
	'rheMac2'	=>	'Macaca_mulatta',
	'calJac1'	=>	'Callithrix_jacchus',
	'mm9'		=>	'Mus_musculus',
	'rn4'		=>	'Rattus_norvegicus',
	'bosTau4'	=>	'Bos_taurus',
	'equCab2'	=>	'Equus_caballus',
	'canFam2'	=>	'Canis_familiaris',
);
my $dataset = 'multiz';
# Initialise Parameters - End
################################################################################

################################################################################
# Output Files - Start
my $output_file = "$output_dir/genomic_alignments.txt";
if (!-e $output_file || $overwrite) {
	my @output_cols = (
		'ID', 'Chr', 'Location', 'Strand', 'Start', 'End', 'Dataset',
		'Genomic Blocks', 'Ambiguous Bases (Ref)', 'Multiple Sections (Ref)',
		'Ambiguous Bases', 'Multiple Sections',
		"Long Insert (Ref: $long_insert)", "Too Many Gaps (Ref: $max_gap)",
		"Long Insert ($long_insert)", "Too Many Gaps ($max_gap)",
		'Species', 'Effective Species (Gene)', 'Effective Species (Flanking)',
		'Insufficient Flanking', 'Gene Gain/Loss');
	save_to_file(join("\t", @output_cols), $output_file);
}
my $existing_output = read_from_file($output_file);
# Output Files - End
################################################################################

################################################################################
# Download MultiZ MAF Data and Tree - Start
unless ($skip_download) {
	my @chrs = qw (1 2 3 4 5 6 7 8 9 10 11 12 13 14 15 16 17 18 19 20 21 22 X Y M);
	my $multiz_version = "46way";
	download_ucsc("maf", $dir, $ref_species, \@chrs, $multiz_version, undef, undef, 1);
	
	my $tree_file = "$dir/maf/$multiz_version.corrected.nh";
	my $tree_string = relabel(read_from_file($tree_file), \%epo);
	my $tree = parse_tree($tree_string);
	bifurcate_tree($tree);
	save_to_file(print_tree($tree), "$dir/settings/multiz_rooted.nh");
	unroot_tree($tree);
	save_to_file(print_tree($tree), "$dir/settings/multiz_unrooted.nh");
}

# Create MAF indexes, for quick querying (won't overwrite existing indexes).
maf_index("$dir/maf");
# Download MultiZ MAF Data and Tree - End
################################################################################

################################################################################
# Fetch MultiZ Data - Start
my $tree_string = read_from_file("$dir/settings/multiz_unrooted.nh");
my @tree_species = leaf_nodes($tree_string);

my ($data, $cols) = read_from_delim($data_file, "\t", 1);
foreach my $line (@$data) {
	my $id = $$line[$$cols{'ID'}];
	my $chr = $$line[$$cols{'Chr'}];
	my $location = $$line[$$cols{'Location'}];
	my $strand = $$line[$$cols{'Strand'}];
	next unless $id && $chr && $location && $strand;
	
	my ($start, $end) = $location =~ /(\d+)\-(\d+)/;
	if ($strand ne '+') {
		($start, $end) = ($end, $start);
	}
	
	my $aln_dir = "$data_dir/$id/$chr/$location/$dataset";
	make_path($aln_dir) unless -e $aln_dir;
	
	my $processed = $existing_output =~ /^$id\t$chr\t$location[^\n]+$dataset/m;
	
	if (!$processed || $overwrite) {
		my ($blocks, $ambiguous_ref, $multiple_ref, $ambiguous, $multiple,
			$long_insert_ref, $max_gap_ref, $long_insert_species,
			$max_gap_species, $species_count, $eff_species_gene,
			$eff_species_flanking, $no_flanking, $gain_loss) = 
		(0, 0, 0, '', '', 0, 0, '', '', 0, 0, 0, 0, 0);
		
		my $maf_file = "$aln_dir/$flanking.maf";
		($blocks, $ambiguous_ref, $multiple_ref, $ambiguous, $multiple, $species_count) = 
			multiz_alignment($chr, $start-$flanking, $end+$flanking, $strand, $ref_species, \%epo, $maf_dir, $two_bit_dir, $maf_file);
		
		# Note that we don't filter on $blocks == 1, otherwise we'd get no data at all...
		if ($ambiguous_ref == 0 && $multiple_ref == 0 && $species_count > 2) {
			my ($seqs, $order) = from_maf(read_from_file($maf_file));
			($long_insert_ref, $long_insert_species, undef, $seqs) = 
				remove_long_inserts($seqs, $order, {}, $start, $end, $ref_species, $long_insert);
			
			($max_gap_ref, $max_gap_species, $species_count, $seqs) = 
				remove_gappy($seqs, $order, {}, $ref_species, $max_gap);
			
			if ($long_insert_ref == 1 || $max_gap_ref == 1 || $species_count < 2) {
				unlink($maf_file);
				unlink("$maf_file.orig");
				rmdir $aln_dir;
			} else {
				my $ref_file = "$data_dir/$id/$chr/$location/reference.fa";
				my $aligned_rna_length;
				($no_flanking, $aligned_rna_length) = crop_alignment($seqs, $order, $flanking, $ref_species, $ref_file);
				
				if (!$no_flanking) {
					# Check for all-gap sequences in trimmed alignment.
					my ($all_gap_ref, $all_gap_species);
					($all_gap_ref, $all_gap_species, $species_count, $seqs) = 
					remove_gappy($seqs, $order, {}, $ref_species, 1);
					
					if ($all_gap_ref == 1 || $species_count < 2) {
						$max_gap_ref = 1;
						$max_gap_species = '';
						unlink($maf_file);
						unlink("$maf_file.orig");
						rmdir $aln_dir;
					} else {
						$max_gap_species .= ",$all_gap_species" if $all_gap_species;
						
						# Finally ready to save the alignment...
						my $fasta_file = "$aln_dir/$flanking.fa";
						save_to_file(to_fasta($seqs, $order), $fasta_file);
						save_to_file(to_maf($seqs, $order), $maf_file);
						
						# Save an appropriately-trimmed tree.
						my @aln_species = keys %$seqs;
						my @prune = simple_diff(\@tree_species, \@aln_species);
						my $tree = parse_tree($tree_string, \@prune);
						unroot($tree);
						save_to_file(print_tree($tree), "$aln_dir/$flanking.nh");
						
						# Save the structure with gaps inserted.
						my $structure = read_from_file("$data_dir/$id/$chr/$location/rfam_structure.txt");
						my $aligned_structure = structure_files($structure, $$seqs{$ref_species}, $flanking, $aln_dir);
						phase_class_files($aligned_structure, $flanking, $aln_dir);
						
						# Check for identical sequences, which effectively reduce
						# the number of species in the alignment.
						$eff_species_flanking = effective_species_count($seqs);
						
						# Save the RNA, sans flanking.
						my $fasta_0_file = "$aln_dir/0.fa";
						my $maf_0_file = "$aln_dir/0.maf";
						foreach my $id (sort {$$order{$a} <=> $$order{$b}} keys %$order) {
							$$seqs{$id} = substr($$seqs{$id}, $flanking, $aligned_rna_length);
						}
						save_to_file(to_fasta($seqs, $order), $fasta_0_file);
						save_to_file(to_maf($seqs, $order), $maf_0_file);
						save_to_file(print_tree($tree), "$aln_dir/0.nh");
						$aligned_structure = structure_files($structure, $$seqs{$ref_species}, 0, $aln_dir);
						phase_class_files($aligned_structure, 0, $aln_dir);
						$eff_species_gene = effective_species_count($seqs);
						
						# Check if the RNA is entirely missing in any species.
						$gain_loss = gene_gain_loss($seqs);
						
						if (!-e "$aln_dir/$flanking\_random_1.maf" || $overwrite) {
							# Randomise alignment: MultiPerm must be in the execution path.
							randomisations($maf_file, $randomisations);
							randomisations($maf_0_file, $randomisations);
						}
					}
				}
			}
		} else {
			unlink($maf_file);
			unlink("$maf_file.orig");
			rmdir($aln_dir);
		}
		
		my $output = "\n$id\t$chr\t$location\t$strand\t$start\t$end\t$dataset".
			"\t$blocks\t$ambiguous_ref\t$multiple_ref\t$ambiguous\t$multiple".
			"\t$long_insert_ref\t$max_gap_ref".
			"\t$long_insert_species\t$max_gap_species".
			"\t$species_count\t$eff_species_gene\t$eff_species_flanking".
			"\t$no_flanking\t$gain_loss";
		save_to_file($output, $output_file, 'append');
	}
}

