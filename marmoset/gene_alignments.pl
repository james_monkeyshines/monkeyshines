#!/usr/bin/perl
use strict;
use warnings;
use Carp;

=pod

=head1 Name

gene_alignments.pl dir data_file

=head1 Description

Retrieve the regions for the exons of overlapping genes, then get
the corresponding sequences in the EPO 12 or 35 species datasets.

=head1 Author

James Allen
(james@monkeyshines.co.uk)

=head1 Copyright

Copyright 2012 James Allen; University of Manchester

=head1 Licence

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program. If not, see <http://www.gnu.org/licenses/>.

=cut

use File::Path qw(make_path);

use Bio::AlignIO;
use Bio::EnsEMBL::Registry;
use MonkeyShines::Sequence qw (from_fasta to_fasta to_phylip_s complement_rev excise_columns);
use MonkeyShines::Tree qw (parse_tree unroot_tree print_tree);
use MonkeyShines::Utils qw (read_from_file read_from_delim save_to_file execute_check);

################################################################################
# Initialise Parameters - Start
my $dir = $ARGV[0];
croak "Directory must be specified" unless $dir;
croak "Directory '$dir' does not exist" unless -e $dir;

my $data_file = $ARGV[1];
croak "Data file must be specified" unless $data_file;
croak "Data file '$data_file' does not exist" unless -e $data_file;

my $bionj = 1;
my $bionj_txt = $bionj ? '.bionj' : '';

my $overwrite = 0;
my $phase_dir = "$dir/phase$bionj_txt";
my $output_dir = "$dir/output";
my $ref_species = 'Homo_sapiens';
# Initialise Parameters - End
################################################################################

################################################################################
# Output File - Start
my $output_file = "$output_dir/gene_alignments.txt";
if (!-e $output_file || $overwrite) {
	my @output_cols = (
		'ID', 'Chr', 'Location', 'Dataset', 'Ensembl Gene ID',
		'RF Dist (Best)', 'RF Dist Norm (Best)',
		'RF Dist (Ref)', 'RF Dist Norm (Ref)'
	);
	save_to_file(join("\t", @output_cols), $output_file);
}
my $existing_output = read_from_file($output_file);
# Output File - End
################################################################################

################################################################################
# Load Ensembl Registry - Start
Bio::EnsEMBL::Registry->load_registry_from_db(
	-host => 'ensembldb.ensembl.org',
	-user => 'anonymous'
);
# Load Ensembl Registry - End
################################################################################

my ($data, $cols) = read_from_delim($data_file, "\t", 1);
foreach my $line (@$data) {
	my $id = $$line[$$cols{'ID'}];
	my $chr = $$line[$$cols{'Chr'}];
	my $start = $$line[$$cols{'Start'}];
	my $end = $$line[$$cols{'End'}];
	my $location = $$line[$$cols{'Location'}];
	my $dataset = $$line[$$cols{'Dataset'}];
	
	my $processed = $existing_output =~ /^$id\t$chr\t$location\t$dataset/m;
	
	if (!$processed || $overwrite) {
		my $results_dir = "$phase_dir/$id\_$chr\_$location\_$dataset";
		my (undef, $order) = from_fasta(read_from_file("$results_dir/0.fa"));
		
		my $ref_tree = read_from_file("$results_dir/0.nh");
		my $ref_tree_obj = parse_tree($ref_tree);
		unroot_tree($ref_tree_obj);
		$ref_tree = print_tree($ref_tree_obj);
		
		my $best_tree_file = "0.consensus_HKY85+G.nh";
		opendir(DIR, $results_dir);
		foreach my $tree_file (sort grep { /\.nh$/ } readdir(DIR)) {
			if ($tree_file =~ /^(0\.consensus_\S+_\S+\.nh)/) {
				$best_tree_file = $tree_file;
				last;
			}
		}
		closedir(DIR);
		next unless -e "$results_dir/$best_tree_file";
		my $best_tree = read_from_file("$results_dir/$best_tree_file");
		
		$chr =~ s/chr//;
		$start += 1;
		
		my $mlss_adaptor = Bio::EnsEMBL::Registry->get_adaptor('Multi', 'Compara', 'MethodLinkSpeciesSet');
		my $mlss = $mlss_adaptor->fetch_by_method_link_type_species_set_name(uc($dataset), 'mammals');
		
		my $slice_adaptor = Bio::EnsEMBL::Registry->get_adaptor($ref_species, 'Core', 'Slice');
		my $slice = $slice_adaptor->fetch_by_region('chromosome', $chr, $start, $end);
		
		my $genes = $slice->get_all_Genes_by_type('protein_coding');
		GENE: foreach my $gene (@{$genes}) {
			my $stable_id = $gene->stable_id();
			my $transcript = $gene->canonical_transcript();
			
			my $exons = $transcript->get_all_Exons();
			my %exon_seqs;
			my $exon_count = 1;
			foreach my $exon (@{$exons}) {
				next unless defined $exon->coding_region_start($transcript);
				my $exon_start = $exon->coding_region_start($transcript);
				my $exon_end = $exon->coding_region_end($transcript);
				my $exon_slice = $slice_adaptor->fetch_by_region('chromosome', $chr, $exon_start, $exon_end);
				my $gab_adaptor = Bio::EnsEMBL::Registry->get_adaptor('Multi', 'compara', 'GenomicAlignBlock');
				my $genomic_align_blocks = $gab_adaptor->fetch_all_by_MethodLinkSpeciesSet_Slice($mlss, $exon_slice, undef, undef, 1);
				
				if (defined $genomic_align_blocks) {
					foreach my $gab (@$genomic_align_blocks) {
						my $alignment = $gab->get_SimpleAlign("uc");
						my $fasta_file = "/tmp/gene_$stable_id\_$exon_count.fa";
						my $alignIO = Bio::AlignIO->new(
							-file=> ">$fasta_file",
							-format => 'fasta');
						$alignIO->write_aln($alignment);
						my ($seqs, undef) = from_fasta(read_from_file("/tmp/gene_$stable_id\_$exon_count.fa"));
						foreach my $id (keys %$seqs) {
							my ($sp) = $id =~ /^(\w+)\/.+$/;
							$sp = ucfirst($sp);
							$$seqs{$sp} = delete $$seqs{$id};
						}
						foreach my $sp (keys %$order) {
							if (exists $$seqs{$sp}) {
								$exon_seqs{$sp} .= $$seqs{$sp};
							} else {
								$exon_seqs{$sp} .= '-' x $alignment->length();
							}
						}
					}
				} else {
					print STDERR "Retrieving blocks for $stable_id slice chr$chr:$exon_start-$exon_end failed.\n";
				}
				$exon_count++;
			}
			
			if ($gene->strand() < 0) {
				foreach my $id (keys %exon_seqs) {
					$exon_seqs{$id} = complement_rev($exon_seqs{$id});
				}
			}
			%exon_seqs = excise_columns(\%exon_seqs, $order);
			my $phy_file = "C:/cygwin/tmp/gene_$stable_id.phy";
			save_to_file(to_phylip_s(\%exon_seqs, $order), $phy_file);
			
			`phyml -i $phy_file -m GTR -b 0 -o tlr`;
			my $gene_tree = read_from_file("$phy_file\_phyml_tree.txt");
			save_to_file($gene_tree, "$results_dir/0.gene.nh");
			
			my ($rf_dist_best, $rf_dist_norm_best) = rf_calc("/tmp", $best_tree, $gene_tree);
			my ($rf_dist_ref, $rf_dist_norm_ref) = rf_calc("/tmp", $ref_tree, $gene_tree);
			my $output = "\n$id\tchr$chr\t$location\t$dataset\t$stable_id".
						 "\t$rf_dist_best\t$rf_dist_norm_best".
						"\t$rf_dist_ref\t$rf_dist_norm_ref";
			save_to_file($output, $output_file, 'append');
		}
	}
}

sub rf_calc {
	my ($tmp_dir, $tree1, $tree2) = @_;
	
	my $trees_file = "$tmp_dir/tmp.trees";
	save_to_file("$tree1\n$tree2", $trees_file);
	
	unlink("$tmp_dir/RAxML_info.tmp") if -e "$tmp_dir/RAxML_info.tmp";
	unlink("$tmp_dir/RAxML_RF-Distances.tmp") if -e "$tmp_dir/RAxML_RF-Distances.tmp";
	
	my $raxml_rf = "raxml -f r -m GTRGAMMA -n 'tmp' -w '$tmp_dir/' -z '$trees_file'";
	execute_check($raxml_rf, 'pair-wise RF distances written', 1, 'RAxML RF', 0);
	my $dist_file = "$tmp_dir/RAxML_RF-Distances.tmp";
	next unless -e $dist_file;
	my $rf = read_from_file($dist_file);
	my ($rf_dist, $rf_dist_norm) = $rf =~ /^0\s+1:\s*(\d+)\s+([\d\.]+)/;
	
	unlink($trees_file);
	unlink("$tmp_dir/RAxML_info.tmp");
	unlink("$tmp_dir/RAxML_RF-Distances.tmp");
	
	return ($rf_dist, $rf_dist_norm);
}

