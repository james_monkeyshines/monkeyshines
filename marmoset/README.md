The Marmoset pipeline
Note that this pipeline isn't intended to be run all at once; rather,
this document shows the progression of the pipeline, the order of the
steps and their requirements.

# Step 0 - Partial Rfam database
Some manual setup is required to create a partial Rfam database; instructions
are given in the 'genomic_rna.pl' documentation; it's quite straightforward.
(Automating it requires the collection of lots of parameters and so much
error-checking that it's best to leave it as a manual step.)
Requires: an RDBMS, MySQL recommended

# Step 1 - Genomic locations of Rfam RNA sequences
For every Rfam seed alignment that includes a sequence for a given reference
sequence, find its most probable genomic location(s). Stores the data in
subdirectories of <dir>/data/, named with the ID and location values.
Requires: BLAT
Execution: perl genomic_rna.pl <dir> <tree_file>
Output:
  <dir>/output/genomic_rna_seqs.txt: All sequences, with flags for gaps at basepair positions and identical sequences.
  <dir>/output/genomic_rna_blat.txt: BLAT information for sequences that don't have gaps at basepair positions or identical sequences.

# Step 1.1 - Filter genomic locations
The BLAT results will probably need some filtering, to remove sequences
with multiple hits, or overlapping regions. These filters will need to be
manually applied (I load them into a spreadsheet, check over the data,
filter it then save as a new text file called genomic_rna_blat.filtered.txt).
Recommended filters:
  BLAT Hits = 1
  BLAT Blocks = 1
  Perfect Match = 1
  Overlapping != 1
  Chr != <awkward chromosomes, such as 'chrM' or 'chrUn'>

# Step 2 - Genomic alignments from EPO (Ensembl)
Retrieve the genomic alignment for a region, including a given amount 
of flanking (default 400 bases either side), for the EPO 12 and 35
species mammalian datasets. Alignments and related files are stored
in sub-subdirectories of <dir>/data/.
Requires: BioPerl, Ensembl and Ensembl-Compara Perl libraries
Execution: perl epo_alignments.pl <dir> <dir>/output/genomic_rna_blat[.filtered].txt
Output: <dir>/output/genomic_alignments.txt

# Step 2.1 - Filter genomic alignments
The genomic alignments will have had poorly-aligned sequences removed,
but the alignments will need to be filtered further, based on the columns
in the output file (as per Step 1.5).
Recommended filters:
  Genomic Blocks = 1
  Ambiguous Bases (Ref) = 0
  Multiple Sections (Ref) = 0
  Long Insert (Ref) = 0
  Too Many Gaps (Ref) = 0
  Effective Species (Gene) > 4
  Insufficient Flanking = 0
  Gene Gain/Loss = 0

# Step 3 (Optional) - Genomic alignments from MultiZ (UCSC)
Retrieve the genomic alignments from the MultiZ/TBA maf dataset (trimmed
to match the species in the EPO 12 species dataset, apart from pig which
is not in the MultiZ alignment). Alignments and related files are stored
in sub-subdirectories of <dir>/data/.
Requires: bx-python, Galaxy
Execution: perl multiz_alignments.pl <dir> <dir>/output/genomic_rna_blat[.filtered].txt
Output: appended to <dir>/output/genomic_alignments.txt

# Step 3.1 - Filter genomic alignments
See Step 2.5.
Recommended filters:
  Ambiguous Bases (Ref) = 0
  Multiple Sections (Ref) = 0
  Long Insert (Ref) = 0
  Too Many Gaps (Ref) = 0
  Effective Species (Gene) > 4
  Insufficient Flanking = 0
  Gene Gain/Loss = 0

# Step 3.2 - Compare EPO and MultiZ alignments
Compare corresponding sequences in the EPO and MultiZ alignments.
Requires: needle, from the EMBOSS suite of tools
Execution: perl epo_multiz_comparison.pl <dir> <dir>/output/genomic_alignments[.filtered].txt
Output: <dir>/output/epo_multiz_comparison.txt

# Step 4 (Optional) - Structurally-aware realignments
Re-align existing EPO alignments of RNA using the X-INS-i program from the
MAFFT suite, and its default of the SCARNA method. Do a further realignment
with PICXAA-R, because that method provides a structure (X-INS-i doesn't).
Alignments and related files are stored in sub-subdirectories of <dir>/data/.
Requires: MAFFT, PICXAA-R
Execution: perl structural_alignments.pl <dir> <dir>/output/genomic_alignments[.filtered].txt
Output: <dir>/output/structural_alignments.txt

# Step 4.1 - Compare EPO and structurally-aware realignments
Calculate the distance between the EPO alignments and the corresponding
structural realignments.
Requires: MetAl
Execution: epo_structural_comparison.pl <dir> <dir>/output/genomic_alignments[.filtered].txt
Output: <dir>/output/epo_structural_comparison.txt

# Step 5 - Alignment Statistics
Calculate a variety of statistics for RNA gene alignments, some generic such
as GC content and gap percentage, others like the proportion of paired bases
and SCI which are only meaningful with an RNA structure.
Requires: RNAfold and RNAalifold, from the VIENNA suite of RNA tools
Execution:
  perl alignment_statistics.pl <dir> <dir>/output/genomic_alignments[.filtered].txt
  perl alignment_statistics.pl <dir> <dir>/output/structural_alignments.txt
Output: <dir>/output/alignment_statistics.txt

# Step 6 (Optional) - Create BIONJ trees
Generate BioNJ tree based on genomic alignments and save it in the
appropriate sub-subdirectory of <dir>/data/.
Requires: PhyML
Execution:
  perl bionj.pl <dir> <dir>/output/genomic_alignments[.filtered].txt
  perl bionj.pl <dir> <dir>/output/structural_alignments.txt

# Step 7 - Detect protein-coding overlap
Detect whether a list of genomic locations overlaps, or is within
2kB of, a protein-coding gene. Note that being upstream or downstream
of a gene is only recorded if the location does not overlap a gene.
Requires: Ensembl Perl libraries
Execution: perl protein_coding_overlap.pl <dir> <dir>/output/genomic_alignments[.filtered].txt
Output: <dir>/output/protein_coding_overlap.txt

# Step 7.1 - Filter genomic alignments
Manual filters might be sensible here; for analysis of alignments of RNA genes,
overlapping with gene features, especially CDSs, may complicate later
analyses (alternatively, RNA that overlap CDS might be interesting to study
in their own right).
Recommended filters:
  Gene Position !~ /(CDS|Overlap)/

# Step 8 - Detect flanking RNA genes
Detect whether genomic alignments have signals of RNA structure in the
flanking regions. This needs to be done as a two-stage process, as the
data must be manually submitted to Rfam.

# Step 8.1 - Generate flanking Fasta files
For submission to Rfam, the data must be partitioned and relabelled.
Execution: perl flanking_rna.pl <dir> <dir>/output/protein_coding_overlap[.filtered].txt 'input'
Output: <dir>/output/flanking_rna/*.fa

# Step 8.2 - Manual submission to Rfam
Manually submit all files in <dir>/output/flanking_rna/*.fa to Rfam,
http://rfam.sanger.ac.uk/search/batch. When the results are returned
via email, save as <dir>/output/flanking_rna/*.out.

# Step 8.3 - Collate flanking RNA results
Execution: perl flanking_rna.pl <dir> <dir>/output/protein_coding_overlap[.filtered].txt 'output'
Output: <dir>/output/flanking_rna.txt

# Step 8.4 - Filter genomic alignments
As with previous filtering steps, remove lines of data that do not match
certain criteria, and save as a new text file.
Recommended filters:
  'Downstream RNA' + 'Upstream RNA' < 1

# Step 9 - PHASE analysis
Get the files required for PHASE analysis in one place; then either run
PHASE, or defer execution to a cluster (the default).
Generates data in <dir>/phase/.
Requires: PHASE
Execution: perl phase_analysis.pl <dir> <dir>/output/flanking_rna[.filtered].txt

