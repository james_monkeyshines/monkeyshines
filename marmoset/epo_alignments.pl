#!/usr/bin/perl
use strict;
use warnings;
use Carp;

=pod

=head1 Name

epo_alignments.pl dir data_file

=head1 Description

Retrieve the genomic alignment for a region, including varying
amounts of flanking, for the EPO 12 and 35 species mammalian
datasets. Then generate randomisations of each alignment.

=head1 Author

James Allen
(james@monkeyshines.co.uk)

=head1 Copyright

Copyright 2012 James Allen; University of Manchester

=head1 Licence

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program. If not, see <http://www.gnu.org/licenses/>.

=cut

use File::Path qw(make_path);

use Bio::EnsEMBL::Registry;
use Bio::EnsEMBL::Utils::Exception qw(throw);
use Bio::AlignIO;

use MonkeyShines::Marmoset qw (
	epo_alignment
	species_locations
	sequence_quality
	remove_long_inserts
	remove_gappy
	remove_species
	crop_alignment
	gene_gain_loss
	effective_species_count
	structure_files
	phase_class_files
	randomisations
);
use MonkeyShines::Sequence qw (
	from_fasta to_fasta to_maf
	complement_rev excise_columns flanking_lengths alignment_columns
);
use MonkeyShines::Tree qw (leaf_nodes parse_tree unroot_tree print_tree);
use MonkeyShines::Utils qw (read_from_file read_from_delim save_to_file simple_diff);

################################################################################
# Initialise Parameters - Start
my $dir = $ARGV[0];
croak "Directory must be specified" unless $dir;
croak "Directory '$dir' does not exist" unless -e $dir;

my $data_file = $ARGV[1];
croak "Data file must be specified" unless $data_file;
croak "Data file '$data_file' does not exist" unless -e $data_file;

my $data_dir = "$dir/data";
my $output_dir = "$dir/output";
my $tree_string = read_from_file("$dir/settings/unrooted.nh");
my @tree_species = leaf_nodes($tree_string);

my $overwrite = 0;
my $flanking = 400;
my $long_insert = 0.1;
my $max_gap = 0.25;
my $randomisations = 10;
my $no_mm_rn = 0;

my $ref_id = 9606;
my $ref_species = 'Homo_sapiens';

# Use two different datasets; the tree for the 12-species set is a pruned
# version of the 35-species set (bar a couple of rounding errors which can
# be ignored), so we can get away with always using the 35-way tree.
my @epo_12 = qw (
	Homo_sapiens
	Pan_troglodytes
	Gorilla_gorilla
	Pongo_abelii
	Macaca_mulatta
	Callithrix_jacchus
	Mus_musculus
	Rattus_norvegicus
	Bos_taurus
	Sus_scrofa
	Equus_caballus
	Canis_familiaris
);
my @epo_2x = qw (
	Nomascus_leucogenys
	Tarsius_syrichta
	Microcebus_murinus
	Otolemur_garnettii
	Tupaia_belangeri
	Dipodomys_ordii
	Cavia_porcellus
	Spermophilus_tridecemlineatus
	Oryctolagus_cuniculus
	Ochotona_princeps
	Vicugna_pacos
	Tursiops_truncatus
	Felis_catus
	Ailuropoda_melanoleuca
	Myotis_lucifugus
	Pteropus_vampyrus
	Erinaceus_europaeus
	Sorex_araneus
	Loxodonta_africana
	Procavia_capensis
	Echinops_telfairi
	Dasypus_novemcinctus
	Choloepus_hoffmanni
);
my @epo_35 = (@epo_12, @epo_2x);

my %datasets = (
	'EPO' => \@epo_12,
	'EPO_LOW_COVERAGE' => \@epo_35
);
# Initialise Parameters - End
################################################################################

################################################################################
# Output Files - Start
my $output_file = "$output_dir/genomic_alignments.txt";
if (!-e $output_file || $overwrite) {
	my @output_cols = (
		'ID', 'Chr', 'Location', 'Strand', 'Start', 'End', 'Dataset',
		'Genomic Blocks', 'Ambiguous Bases (Ref)', 'Multiple Sections (Ref)',
		'Ambiguous Bases', 'Multiple Sections',
		"Long Insert (Ref: $long_insert)", "Too Many Gaps (Ref: $max_gap)",
		"Long Insert ($long_insert)", "Too Many Gaps ($max_gap)",
		'Species', 'Effective Species (Gene)', 'Effective Species (Flanking)',
		'Insufficient Flanking', 'Gene Gain/Loss');
	save_to_file(join("\t", @output_cols), $output_file);
}
my $existing_output = read_from_file($output_file);
# Output Files - End
################################################################################

################################################################################
# Load Ensembl Registry - Start
Bio::EnsEMBL::Registry->load_registry_from_db(
	-host => 'ensembldb.ensembl.org',
	-user => 'anonymous',
	-db_version => 67
);
# Load Ensembl Registry - End
################################################################################

################################################################################
# Fetch Ensembl Data - Start
foreach my $dataset (keys %datasets) {
	my $genome_dbs;
	my $gdb_adaptor = Bio::EnsEMBL::Registry->get_adaptor('Multi', 'compara', 'GenomeDB');
	throw("Cannot connect to Compara") if (!$gdb_adaptor);
	
	foreach my $species (@{$datasets{$dataset}}) {
		my $genome_db = $gdb_adaptor->fetch_by_name_assembly($species);
		push(@$genome_dbs, $genome_db);
	}
	
	my $slice_adaptor = Bio::EnsEMBL::Registry->get_adaptor($ref_species, 'core', 'Slice');
	my $mlss_adaptor = Bio::EnsEMBL::Registry->get_adaptor('Multi', 'compara', 'MethodLinkSpeciesSet');
	my $method_link_species_set = $mlss_adaptor->fetch_by_method_link_type_GenomeDBs($dataset, $genome_dbs);
	
	$dataset = lc($dataset);
	$dataset .= '_no_mm_rn' if $no_mm_rn;
	
	my ($data, $cols) = read_from_delim($data_file, "\t", 1);
	LOCATION: foreach my $line (@$data) {
		my $id = $$line[$$cols{'ID'}];
		my $chr = $$line[$$cols{'Chr'}];
		my $location = $$line[$$cols{'Location'}];
		my $strand = $$line[$$cols{'Strand'}];
		next unless $id && $chr && $location && $strand;
		
		my ($start, $end) = $location =~ /(\d+)\-(\d+)/;
		my $rev_comp = 0;
		if ($strand ne '+') {
			($start, $end) = ($end, $start);
			$rev_comp = 1;
		}
		
		my $processed = $existing_output =~ /^$id\t$chr\t$location[^\n]+$dataset/m;
		
		if (!$processed || $overwrite) {
			my ($ambiguous_ref, $multiple_ref, $ambiguous, $multiple,
				$long_insert_ref, $max_gap_ref, $long_insert_species,
				$max_gap_species, $species_count, $eff_species_gene,
				$eff_species_flanking, $no_flanking, $gain_loss) = 
			(0, 0, '', '', 0, 0, '', '', 0, 0, 0, 0, 0);
			
			my ($blocks, $alignment) = 
				epo_alignment($chr, $start-$flanking, $end+$flanking, $rev_comp, $slice_adaptor, $method_link_species_set);
			
			if ($blocks == 1) {
				($ambiguous_ref, $multiple_ref, $ambiguous, $multiple, $species_count) = 
					sequence_quality($alignment, $ref_species);
				
				if ($ambiguous_ref == 0 && $multiple_ref == 0 && $species_count > 2) {
					# This method doesn't seem to work...
					#$alignment->remove_columns(['all_gaps_columns']);
					
					my $aln_dir = "$data_dir/$id/$chr/$location/$dataset";
					make_path($aln_dir) unless -e $aln_dir;
					my $fasta_file = "$aln_dir/$flanking.fa";
					my $alignIO = Bio::AlignIO->new(
						-file=> ">$fasta_file",
						-format => 'fasta');
					$alignIO->write_aln($alignment);
					
					# Need to remove any columns that consist entirely
					# of gap characters. There is a method in SimpleAlign
					# that should do this, but it doesn't seem to work...
					my ($seqs, $order) = from_fasta(read_from_file($fasta_file));
					$seqs = excise_columns($seqs);
					
					# We'll run into trouble later if the taxa in the alignment
					# file don't match those in the tree file. Save the locations
					# in case we need them later.
					my $genomic_locations = species_locations($seqs, $order);
					
					# Mouse and rat are often poorly aligned...
					if ($no_mm_rn) {
						remove_species($seqs, $order, $genomic_locations, ['Mus_musculus', 'Rattus_norvegicus']);
					}
					
					# Filter on quality of alignment
					($long_insert_ref, $long_insert_species, undef, $seqs) = 
						remove_long_inserts($seqs, $order, $genomic_locations, $start, $end, $ref_species, $long_insert);
					
					($max_gap_ref, $max_gap_species, $species_count, $seqs) = 
						remove_gappy($seqs, $order, $genomic_locations, $ref_species, $max_gap);
					
					if ($long_insert_ref == 1 || $max_gap_ref == 1 || $species_count < 2) {
						unlink $fasta_file;
						rmdir $aln_dir;
					} else {
						my $ref_file = "$data_dir/$id/$chr/$location/reference.fa";
						my $aligned_rna_length;
						($no_flanking, $aligned_rna_length) = crop_alignment($seqs, $order, $flanking, $ref_species, $ref_file);
						
						if (!$no_flanking) {
							# Check for all-gap sequences in trimmed alignment.
							my ($all_gap_ref, $all_gap_species);
							($all_gap_ref, $all_gap_species, $species_count, $seqs) = 
								remove_gappy($seqs, $order, $genomic_locations, $ref_species, 1);
							
							if ($all_gap_ref == 1 || $species_count < 2) {
								$max_gap_ref = 1;
								$max_gap_species = '';
								unlink $fasta_file;
								rmdir $aln_dir;
							} else {
								$max_gap_species .= ",$all_gap_species" if $all_gap_species;
								
								# Finally ready to save the alignment...
								my $maf_file = "$aln_dir/$flanking.maf";
								save_to_file(to_fasta($seqs, $order), $fasta_file);
								save_to_file(to_maf($seqs, $order), $maf_file);
								
								# Save an appropriately-trimmed tree.
								my @aln_species = keys %$seqs;
								my @prune = simple_diff(\@tree_species, \@aln_species);
								my $tree = parse_tree($tree_string, \@prune);
								unroot_tree($tree);
								save_to_file(print_tree($tree), "$aln_dir/$flanking.nh");
								
								# Save the structure with gaps inserted.
								my $structure = read_from_file("$data_dir/$id/$chr/$location/rfam_structure.txt");
								my $aligned_structure = structure_files($structure, $$seqs{$ref_species}, $flanking, $aln_dir);
								phase_class_files($aligned_structure, $flanking, $aln_dir);
								
								# Save the genomic location of the RNA in all species.
								my $g_locations;
								foreach my $species (sort { $$order{$a} <=> $$order{$b} } keys %{$order}) {
									$g_locations .= "$species\t".$$genomic_locations{$species}."\n";
								}
								save_to_file($g_locations, "$aln_dir/0.locations.txt");
								
								# Check for identical sequences, which effectively reduce
								# the number of species in the alignment.
								$eff_species_flanking = effective_species_count($seqs);
								
								# Save the RNA, sans flanking.
								my $fasta_0_file = "$aln_dir/0.fa";
								my $maf_0_file = "$aln_dir/0.maf";
								foreach my $id (sort {$$order{$a} <=> $$order{$b}} keys %$order) {
									$$seqs{$id} = substr($$seqs{$id}, $flanking, $aligned_rna_length);
								}
								save_to_file(to_fasta($seqs, $order), $fasta_0_file);
								save_to_file(to_maf($seqs, $order), $maf_0_file);
								save_to_file(print_tree($tree), "$aln_dir/0.nh");
								$aligned_structure = structure_files($structure, $$seqs{$ref_species}, 0, $aln_dir);
								phase_class_files($aligned_structure, 0, $aln_dir);
								$eff_species_gene = effective_species_count($seqs);
								
								# Check if the RNA is entirely missing in any species.
								$gain_loss = gene_gain_loss($seqs);
								
								if (!-e "$aln_dir/$flanking\_random_1.maf" || $overwrite) {
									# Randomise alignment: MultiPerm must be in the execution path.
									randomisations($maf_file, $randomisations);
									randomisations($maf_0_file, $randomisations);
								}
							}
						}
					}
				}
			}
			
			my $output = "\n$id\t$chr\t$location\t$strand\t$start\t$end\t$dataset".
				"\t$blocks\t$ambiguous_ref\t$multiple_ref\t$ambiguous\t$multiple".
				"\t$long_insert_ref\t$max_gap_ref".
				"\t$long_insert_species\t$max_gap_species".
				"\t$species_count\t$eff_species_gene\t$eff_species_flanking".
				"\t$no_flanking\t$gain_loss";
			save_to_file($output, $output_file, 'append');
		}
	}
}
# Fetch Ensembl Data - End
################################################################################

