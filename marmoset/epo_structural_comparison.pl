#!/usr/bin/perl
use strict;
use warnings;
use Carp;

=pod

=head1 Name

epo_structural_comparison.pl dir data_file

=head1 Description

Calculate distances between EPO alignments and structural realignments.
Requires 'metal' to be in your path
(http://bioinf.man.ac.uk/blog/whelanlab/software/metal).

=head1 Author

James Allen
(james@monkeyshines.co.uk)

=head1 Copyright

Copyright 2012 James Allen; University of Manchester

=head1 Licence

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program. If not, see <http://www.gnu.org/licenses/>.

=cut

use File::Copy;
use File::Path qw(make_path);

use MonkeyShines::Utils qw (read_from_delim read_from_file save_to_file execute_check);

################################################################################
# Initialise Parameters - Start
my $dir = $ARGV[0];
croak "Directory must be specified" unless $dir;
croak "Directory '$dir' does not exist" unless -e $dir;

my $data_file = $ARGV[1];
croak "Data file must be specified" unless $data_file;
croak "File '$data_file' does not exist" unless -e $data_file;

my $output_dir = "$dir/output";

my $overwrite = 0;
my $flanking = 0;
my @structural = ('mafft', 'picxaa-r');
my $dataset = 'epo';
# Initialise Parameters - End
################################################################################

################################################################################
# Output File - Start
my $output_file = "$output_dir/epo_structural_comparison.txt";
if (!-e $output_file || $overwrite) {
	my @columns = ('ID', 'Chr', 'Location', 'Flanking',
		'Dataset 1', 'Dataset 2', 'Numerator', 'Denominator', 'Distance');
	save_to_file(join("\t", @columns), $output_file);
}
my $existing_output = read_from_file($output_file);
# Output File - End
################################################################################

################################################################################
# Compare EPO and Structural Alignments - Start
my ($data, $cols) = read_from_delim($data_file, "\t", 1);
foreach my $line (@$data) {
	my $id = $$line[$$cols{'ID'}];
	my $chr = $$line[$$cols{'Chr'}];
	my $location = $$line[$$cols{'Location'}];
	my $file_dataset = $$line[$$cols{'Dataset'}];
	next unless $file_dataset eq $dataset;
	
	my $epo_file = "$dir/data/$id/$chr/$location/epo/$flanking.fa";
	croak "Missing EPO file: '$epo_file'" unless -e $epo_file;
	foreach my $structural (@structural) {
		my $processed = $existing_output =~ /^$id\t$chr\t$location\t$flanking\tepo\tepo_$structural/m;
		if (!$processed || $overwrite) {
			my $structural_file = "$dir/data/$id/$chr/$location/epo_$structural/$flanking.fa";
			if (-e $structural_file) {
				my ($num, $den, $dist) = run_metal($epo_file, $structural_file);
				my $output = "\n$id\t$chr\t$location\t$flanking".
					"\tepo\tepo_$structural\t$num\t$den\t$dist";
				save_to_file($output, $output_file, 'append');
			} else {
				carp "Missing realignment file: '$structural_file'" unless -e $structural_file;
				my $output = "\n$id\t$chr\t$location\t$flanking\tepo\tepo_$structural\t\t\t";
				save_to_file($output, $output_file, 'append');
			}
		}
	}
	if (scalar(@structural) == 2) {
		my ($dataset1, $dataset2) = map { 'epo_'.$_ } @structural;
		my $structural_file1 = "$dir/data/$id/$chr/$location/$dataset1/$flanking.fa";
		my $structural_file2 = "$dir/data/$id/$chr/$location/$dataset2/$flanking.fa";
		my $processed = $existing_output =~ /^$id\t$chr\t$location\t$flanking\t$dataset1\t$dataset2/m;
		if (!$processed || $overwrite) {
			next unless -e $structural_file1 && -e $structural_file2;
			my ($num, $den, $dist) = run_metal($structural_file1, $structural_file2);
			my $output = "\n$id\t$chr\t$location\t$flanking".
				"\t$dataset1\t$dataset2\t$num\t$den\t$dist";
			save_to_file($output, $output_file, 'append');
		}
	}
}
# Compare EPO and Structural Alignments - End
################################################################################

################################################################################
# Run MetAl - Start
sub run_metal {
	my ($file1, $file2) = @_;
	
	# We need this palaver as MetAl isn't always great at finding input files.
	my $tmp_file1 = 'tmp1.fa';
	my $tmp_file2 = 'tmp2.fa';
	copy($file1, $tmp_file1);
	copy($file2, $tmp_file2);
	
	my $metal = "metal $tmp_file1 $tmp_file2";
	my $out = execute_check($metal, 'metal.exe', 0, 'MetAl', 0);
	my ($num, $den, $dist) = $out =~ /^(\d+)\s*\/\s*(\d+)\s*=\s*(\S+)/m;
	croak "Distance calculation failed for files '$file1' and '$file2'" unless defined $dist;
	
	unlink($tmp_file1);
	unlink($tmp_file1);
	
	return ($num, $den, $dist);
}
# Run MetAl - End
################################################################################

