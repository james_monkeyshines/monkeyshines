#!/usr/bin/perl
use strict;
use warnings;
use Carp;

=pod

=head1 Name

genomic_rna.pl dir tree_file

=head1 Description

For every Rfam seed alignment that includes a sequence for a given
reference sequence, find its most probable genomic location(s).
Before executing this script, Rfam files need to have been downloaded
and processed manually, as follows (a human reference is assumed,
and the taxonomic ID '9606' is thus used - replace appropriately
to use another reference species):

The Rfam data needs to have been downloaded, loaded into a database, and
the pertinent information extracted into a text file. It's easier to do
this by hand than script it: get the following files from the Rfam FTP site
(ftp://ftp.sanger.ac.uk/pub/databases/Rfam/CURRENT):
    Rfam.seed.gz
    database_files/clan_membership.txt.gz
    database_files/clans.txt.gz
    database_files/rfam.txt.gz
    database_files/rfam_reg_seed.txt.gz
    database_files/rfamseq.txt.gz
    database_files/tables.sql
Save them in the directory "<dir>/rfam", where <dir> is the directory
where you want all the data to end up.
Unzip the files, and get rid of the irrelevant bits of rfamseq.txt, since
this is a huge file and is going to slow us down otherwise; execute the
following on the command line, substituting '9606' if necessary:
    sed -n '/\t9606\t/p' rfamseq.txt > rfamseq.tmp
    mv rfamseq.tmp rfamseq.txt
Now create a database with the table definitions in tables.sql, and use
LOAD DATA INFILE to populate the database with the downloaded files. Then
execute the following query, ensuring that the data ends up in the file
"<dir>/rfam/map_9606.txt":
    SELECT DISTINCT t1.rfam_acc, t1.description, t3.rfamseq_acc
    FROM rfam t1
    INNER JOIN rfam_reg_seed t2 ON t1.auto_rfam=t2.auto_rfam
    INNER JOIN rfamseq t3 ON t2.auto_rfamseq=t3.auto_rfamseq
    WHERE t3.ncbi_id = $ref_id
    ORDER BY t1.rfam_acc
    INTO OUTFILE '<dir>/rfam/map_9606.txt';

A mapping between Rfam IDs and clans also comes in handy later on.
    SELECT t3.rfam_acc, t1.clan_acc, t1.clan_description
    FROM clans t1
    INNER JOIN clan_membership t2 ON t1.auto_clan=t2.auto_clan
    INNER JOIN rfam t3 ON t2.auto_rfam=t3.auto_rfam
    ORDER BY t3.rfam_acc, t1.clan_acc
    INTO OUTFILE '<dir>/rfam/rfam_clans.txt';

=head1 Author

James Allen
(james@monkeyshines.co.uk)

=head1 Copyright

Copyright 2012 James Allen; University of Manchester

=head1 Licence

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program. If not, see <http://www.gnu.org/licenses/>.

=cut

use File::Path qw(make_path);

use MonkeyShines::Sequence qw (
	download_ucsc
	to_fasta from_fasta
	complement_rev excise_columns
	blat_server_start blat_server_stop blat blat_top_hits
);
use MonkeyShines::Tree qw (
	parse_tree bifurcate_tree unroot_tree print_tree
);
use MonkeyShines::Utils qw (
	read_from_file save_to_file coord_overlap
);

################################################################################
# Initialise Parameters - Start
my $dir = $ARGV[0];
croak "Directory must be specified" unless $dir;
croak "Directory '$dir/rfam' does not exist" unless -e "$dir/rfam";

my $tree_file = $ARGV[1];
croak "Tree file must be specified" unless $tree_file;
croak "Tree file '$tree_file' does not exist" unless -e $tree_file;

my $data_dir = "$dir/data";
make_path($data_dir) unless -e $data_dir;
my $output_dir = "$dir/output";
make_path($output_dir) unless -e $output_dir;
my $settings_dir = "$dir/settings";
make_path($settings_dir) unless -e $settings_dir;

my $ref_id = 9606;
my $ref_species = 'Homo_sapiens';
my @chrs = qw (1 2 3 4 5 6 7 8 9 10 11 12 13 14 15 16 17 18 19 20 21 22 X Y M);
# Initialise Parameters - End
################################################################################

################################################################################
# Output Files - Start
my $seq_data_file = "$output_dir/genomic_rna_seqs.txt";
my @seq_data_cols = (
	'ID', 'Accession', 'Basepair Gap (Ref)', 'Identical Sequence');
my $seq_data = join("\t", @seq_data_cols);

my $blat_data_file = "$output_dir/genomic_rna_blat.txt";
my @blat_data_cols = (
	'ID', 'Accession',
	'BLAT Hits', 'BLAT Blocks', 'BLAT Score', 'Perfect Match',
	'Overlapping', 'Chr', 'Location', 'Strand', 'Start', 'End');
my $blat_data = join("\t", @blat_data_cols);
# Output Files - End
################################################################################

################################################################################
# Save Phylogenetic Tree - Start
my $tree = parse_tree(read_from_file($tree_file));
bifurcate_tree($tree);
save_to_file(print_tree($tree), "$settings_dir/rooted.nh");
unroot_tree($tree);
save_to_file(print_tree($tree), "$settings_dir/unrooted.nh");
# Save Phylogenetic Tree - End
################################################################################

################################################################################
# Get Fasta Data and Start BLAT Server - Start
# (Requires the BLAT executables (blat faToTwoBit gfClient, gfServer) to be
# in the user's path.) 
my $fasta_dir = "$dir/fasta";
#download_ucsc("fasta", $dir, $ref_species, \@chrs);
blat_server_start($fasta_dir);
sleep 300;
# Get Fasta Data and Start BLAT Server - End
################################################################################

################################################################################
# Parse RNA Data - Start
# The Rfam data needs to have processed already, because this is much easier
# and simpler to do by hand than script. This pre-processing provides a
# Stockholm-format file of sequence/structure data, and a set of mappings
# between Rfam IDs and accessions, which we retrieve here.
my $aln = read_from_file("$dir/rfam/Rfam.seed");
my $map = read_from_file("$dir/rfam/map_$ref_id.txt");
my (%aln_data) = $aln =~ m!\#=GF\s+AC\s+(RF\d{5})(.+?)^//$!gms;
my @map_rows = split(/\n/, $map);
my (%rna, %seqs_seen);
foreach my $row (@map_rows) {
	my ($rfam_id, $description, $accession) = split(/\t/, $row);
	my $aln_data = $aln_data{$rfam_id};
	
	my %accessions;
	my @sequences = $aln_data =~ /^($accession\..+)$/gm;
	foreach my $sequence (@sequences) {
		$sequence =~ /^($accession\S+)\s+(\S+)$/;
		$accessions{$1}{'seq'} .= $2;
	}
	
	my @structure_pieces = $aln_data =~ /^\#=GC\s+SS_cons\s+(.+)/gm;
	my $structure = join("", @structure_pieces);
	
	# Edit the structure to match the ungapped sequence;
	# if any gaps in the sequence align with base pairs,
	# then we don't use that accession/sequence.
	ACC: foreach my $acc (keys %accessions) {
		my %seq_str = ('seq' => $accessions{$acc}{'seq'}, 'str' => $structure);
		my %new_seq_str = excise_columns(\%seq_str);
		if ($new_seq_str{'seq'} =~ /[\-\.]/) {
			$seq_data .= "\n$rfam_id\t$acc\t1\t0";
			next ACC;
		} else {
			$new_seq_str{'seq'} =~ tr/U/T/;
			if (exists $seqs_seen{$new_seq_str{'seq'}}) {
				$seq_data .= "\n$rfam_id\t$acc\t0\t1";
				next ACC;
			} else {
				$seq_data .= "\n$rfam_id\t$acc\t0\t0";
				$seqs_seen{$new_seq_str{'seq'}} = $acc;
				$rna{$rfam_id}{$acc}{'seq'} = $new_seq_str{'seq'};
				$rna{$rfam_id}{$acc}{'str'} = $new_seq_str{'str'};
			}
		}
	}
}
save_to_file($seq_data, $seq_data_file);
# Parse RNA Data - End
################################################################################

################################################################################
# Get Non-Redundant Genomic Locations - Start
# Note that we check for overlapping locations across all Rfam IDs/accessions.
my (%locations, %rna_locations);
foreach my $rfam_id (sort keys %rna) {
	foreach my $acc (sort keys %{$rna{$rfam_id}}) {
		my $rfam_seq_file = "$dir/rfam.fa";
		my $blat_results_file = "$dir/blat.psl";
		save_to_file(to_fasta({$acc => $rna{$rfam_id}{$acc}{'seq'}}), $rfam_seq_file);
		my $results = blat($rfam_seq_file, $blat_results_file);
		my @top_results = blat_top_hits($results);
		$rna{$rfam_id}{$acc}{'blat_results'} = read_from_file($blat_results_file);
		$rna{$rfam_id}{$acc}{'blat_hits'} = scalar(@top_results) || 0;
		unlink($rfam_seq_file);
		unlink($blat_results_file);
		
		foreach my $top_result (@top_results) {
			my ($chr, $start, $end, undef, undef, $strand, $blocks, $target, $score) = @$top_result;
			$target =~ s/,$//;
			$target = uc($target);
			my $location = "$start-$end";
			if ($strand ne '+') {
				$location = "$end-$start";
				$target = complement_rev($target);
			}
			
			$rna_locations{$rfam_id}{$acc}{"$chr/$location"}{'blocks'} = $blocks;
			$rna_locations{$rfam_id}{$acc}{"$chr/$location"}{'target'} = $target;
			$rna_locations{$rfam_id}{$acc}{"$chr/$location"}{'score'} = $score;
			$rna_locations{$rfam_id}{$acc}{"$chr/$location"}{'overlap'} = '';
			
			if ($blocks == 1) {
				$rna_locations{$rfam_id}{$acc}{"$chr/$location"}{'loc_text'} = "$chr\t$location\t$strand\t$start\t$end";
				
				if (scalar(keys(%{$locations{"$chr$strand"}}))) {
					foreach my $loc (sort { $locations{"$chr$strand"}{$b}{'score'} <=> $locations{"$chr$strand"}{$a}{'score'} } keys %{$locations{"$chr$strand"}}) {
						my $overlap = coord_overlap([$start, $end], $locations{"$chr$strand"}{$loc}{'location'});
						if ($overlap) {
							my $old_rfam_id = $locations{"$chr$strand"}{$loc}{'rfam_id'};
							my $old_acc = $locations{"$chr$strand"}{$loc}{'acc'};
							
							if ($score > $locations{"$chr$strand"}{$loc}{'score'}) {
								$rna_locations{$rfam_id}{$acc}{"$chr/$location"}{'overlap'} = 0;
								$rna_locations{$old_rfam_id}{$old_acc}{"$chr/$loc"}{'overlap'} = 1;
								delete $locations{"$chr$strand"}{$loc};
							} else {
								$rna_locations{$rfam_id}{$acc}{"$chr/$location"}{'overlap'} = 1;
								$rna_locations{$old_rfam_id}{$old_acc}{"$chr/$loc"}{'overlap'} = 0;
								next;
							}
						}
						
						$locations{"$chr$strand"}{$location}{'rfam_id'} = $rfam_id;
						$locations{"$chr$strand"}{$location}{'acc'} = $acc;
						$locations{"$chr$strand"}{$location}{'location'} = [$start, $end];
						$locations{"$chr$strand"}{$location}{'score'} = $score;
					}
				} else {
					$locations{"$chr$strand"}{$location}{'rfam_id'} = $rfam_id;
					$locations{"$chr$strand"}{$location}{'acc'} = $acc;
					$locations{"$chr$strand"}{$location}{'location'} = [$start, $end];
					$locations{"$chr$strand"}{$location}{'score'} = $score;
				}
			} else {
				$rna_locations{$rfam_id}{$acc}{"$chr/$location"}{'loc_text'} = "\t\t\t\t";
			}
		}
	}
}

# Get Non-Redundant Genomic Locations - End
################################################################################

################################################################################
# Save Non-Redundant Data - Start
foreach my $rfam_id (sort keys %rna) {
	foreach my $acc (sort keys %{$rna{$rfam_id}}) {
		my $hits = $rna{$rfam_id}{$acc}{'blat_hits'};
		if ($hits > 0) {
			my $rfam_seq = $rna{$rfam_id}{$acc}{'seq'};
			foreach my $chr_loc (sort keys %{$rna_locations{$rfam_id}{$acc}}) {
				my $blocks = $rna_locations{$rfam_id}{$acc}{$chr_loc}{'blocks'};
				my $score = $rna_locations{$rfam_id}{$acc}{$chr_loc}{'score'};
				my $target = $rna_locations{$rfam_id}{$acc}{$chr_loc}{'target'};
				my $perfect = $rfam_seq eq $target ? 1 : 0;
				my $overlap = $rna_locations{$rfam_id}{$acc}{$chr_loc}{'overlap'};
				
				$blat_data .= "\n$rfam_id\t$acc\t$hits\t$blocks\t$score\t$perfect\t$overlap";
				$blat_data .= "\t".$rna_locations{$rfam_id}{$acc}{$chr_loc}{'loc_text'};
				
				if ($blocks == 1 && $overlap ne '1') {
					my $location_dir = "$data_dir/$rfam_id/$chr_loc";
					make_path($location_dir) unless -e $location_dir;
					save_to_file($rna{$rfam_id}{$acc}{'blat_results'}, "$location_dir/blat.psl");
					save_to_file(">reference\n$target", "$location_dir/reference.fa");
					save_to_file(">$acc\n$rfam_seq", "$location_dir/rfam.fa");
					save_to_file($rna{$rfam_id}{$acc}{'str'}, "$location_dir/rfam_structure.txt");
				}
			}
		} else {
			$blat_data .= "\n$rfam_id\t$acc\t$hits\t\t\t\t\t\t\t\t\t";
		}
	}
}
save_to_file($blat_data, $blat_data_file);
# Save Non-Redundant Data - End
################################################################################

################################################################################
# Tidying Up - Start
blat_server_stop();
# Tidying Up - End
################################################################################

