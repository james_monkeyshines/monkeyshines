#!/usr/bin/perl
use strict;
use warnings;
use Carp;

=pod

=head1 Name

phase_results.pl dir data_file [modeltype_file]

=head1 Description

Take the results of PHASE analyses and combine them into a single file.

=head1 Author

James Allen
(james@monkeyshines.co.uk)

=head1 Copyright

Copyright 2012 James Allen; University of Manchester

=head1 Licence

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program. If not, see <http://www.gnu.org/licenses/>.

=cut

use MonkeyShines::Tree qw (tree_length);
use MonkeyShines::Utils qw (read_from_delim read_from_file save_to_file);

################################################################################
# Initialise Parameters - Start
my $dir = $ARGV[0];
croak "Directory must be specified" unless $dir;
croak "Directory '$dir' does not exist" unless -e $dir;

my $data_file = $ARGV[1];
croak "Data file must be specified" unless $data_file;
croak "File '$data_file' does not exist" unless -e $data_file;

my $modeltype_file = $ARGV[2];
my %modeltype;
if ($modeltype_file) {
	croak "File '$modeltype_file' does not exist" unless -e $modeltype_file;
	my ($data, $cols) = read_from_delim($modeltype_file, "\t", 1);
	foreach my $line (@$data) {
		$modeltype{$$line[$$cols{'Model Name'}]} = $$line[$$cols{'Model Type'}];
	}
}

my $data_dir = "$dir/data";
my $phase_dir = "$dir/phase";
my $output_dir = "$dir/output";
my $overwrite = 1;

my @flanking = (0);
my $realignment = 0; # "picxaa-r";
my $bionj = 1;
my $bionj_txt = $bionj ? '.bionj' : '';
# Initialise Parameters - End
################################################################################

################################################################################
# Output File - Start
my $output_file = "$output_dir/phase$bionj_txt\_results.txt";
if (!-e $output_file || $overwrite) {
	my @output_cols = (
		'ID', 'Chr', 'Location', 'Dataset', 'Flanking',
		'Model Name', 'Model Type', 'Free Parameters', 'lnL', 'lnL Adjustment',
		'AIC', 'AICc', 'Delta AIC', 'Delta AICc',
		'Inferred Tree Length', 'Relative Rate'
	);
	save_to_file(join("\t", @output_cols), $output_file);
}
my $existing_output = read_from_file($output_file);
# Output File - End
################################################################################

################################################################################
# Collate PHASE Results - Start
my ($data, $cols) = read_from_delim($data_file, "\t", 1);
foreach my $line (@$data) {
	my $id = $$line[$$cols{'ID'}];
	my $chr = $$line[$$cols{'Chr'}];
	my $location = $$line[$$cols{'Location'}];
	my $dataset = $$line[$$cols{'Dataset'}];
	
	my $results_dir = "$phase_dir$bionj_txt/$id\_$chr\_$location\_$dataset";
	if (-e $results_dir) {
		foreach my $flanking (@flanking) {
			my $results_file = "$results_dir/$flanking\_results.txt";
			if (-e $results_file) {
				my $prefix = "$id\t$chr\t$location\t$dataset\t$flanking";
				my $processed = $existing_output =~ /^$prefix/m;
				if (!$processed || $overwrite) {
					save_to_file(parse_results($prefix, $results_file, "$results_dir/results/$flanking"), $output_file, 'append');
				}
			} else {
				carp "Results file '$results_file' does not exist";
			}
			
			if ($realignment) {
				$results_file = "$results_dir\_$realignment/$flanking\_results.txt";
				if (-e $results_file) {
					my $prefix = "$id\t$chr\t$location\t$dataset\_$realignment\t$flanking";
					my $processed = $existing_output =~ /^$prefix/m;
					if (!$processed || $overwrite) {
						save_to_file(parse_results($prefix, $results_file, "$results_dir\_$realignment/results/$flanking"), $output_file, 'append');
					}
				}
			}
		}
	} else {
		carp "Results directory '$results_dir' does not exist";
	}
}
# Collate PHASE Results - End
################################################################################

################################################################################
# Parse Results File - Start
sub parse_results {
	my ($prefix, $results_file, $basename) = @_;
	my $output;
	
	my ($results, $col) = read_from_delim($results_file, "\t", 1);
	foreach my $line (@$results) {
		my $model_name = $$line[$$col{'Model Name'}];
		$output .= "\n$prefix";
		$output .= "\t$model_name";
		$output .= "\t";
		$output .= $modeltype{$model_name} if exists $modeltype{$model_name};
		$output .= "\t".$$line[$$col{'Free Parameters'}];
		$output .= "\t".$$line[$$col{'lnL'}];
		$output .= "\t".$$line[$$col{'lnL Adjustment'}];
		$output .= "\t".$$line[$$col{'AIC'}];
		$output .= "\t".$$line[$$col{'AICc'}];
		$output .= "\t".$$line[$$col{'Delta AIC'}];
		$output .= "\t".$$line[$$col{'Delta AICc'}];
		
		$model_name =~ s/(\.empirical|\.equal)//;
		my $tree_file = "$basename\_$model_name.tre";
		my $tlength = tree_length(read_from_file($tree_file));
		$output .= "\t$tlength";
		
		my $model_file = "$basename\_$model_name.mod";
		my $model_data = read_from_file($model_file);
		my ($relative_rate) = $model_data =~ /average substitution rate ratio = (\S+)/m;
		$relative_rate = 1 unless defined $relative_rate;
		$output .= "\t$relative_rate";
	}
	return $output;
}
# Parse Results File - End
################################################################################
