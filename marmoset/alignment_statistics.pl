#!/usr/bin/perl
use strict;
use warnings;
use Carp;

=pod

=head1 Name

alignment_statistics.pl dir data_file

=head1 Description

Calculate a variety of statistics for RNA gene alignments,
some generic such as GC content and gap percentage, others
like the proportion of paired bases and SCI which are only
meaningful with an RNA structure.

The MonkeyShines::Baboon module is used to create images
of each alignment.

=head1 Author

James Allen
(james@monkeyshines.co.uk)

=head1 Copyright

Copyright 2012 James Allen; University of Manchester

=head1 Licence

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program. If not, see <http://www.gnu.org/licenses/>.

=cut

use MonkeyShines::Baboon qw (	colour_alignment
								barcode
								annotate_positions
								image_tile );
use MonkeyShines::Sequence qw (	from_fasta
								from_maf
								pairwise_identity
								gc_content_multiple
								excise_columns
								parse_structure
								sci );
use MonkeyShines::Tree qw (tree_length);
use MonkeyShines::Utils qw (read_from_file read_from_delim save_to_file sum );

################################################################################
# Initialise Parameters - Start
my $dir = $ARGV[0];
croak "Directory must be specified" unless $dir;
croak "Directory '$dir' does not exist" unless -e $dir;

my $data_file = $ARGV[1];
croak "Data file must be specified" unless $data_file;
croak "Data file '$data_file' does not exist" unless -e $data_file;

my $data_dir = "$dir/data";
my $output_dir = "$dir/output";

my $overwrite = 0;

# We define the order that is used to draw the alignments here, so that the
# 'Baboon' images that we create are a) consistent across different alignments
# and b) implicitly indicating some information about evolutionary relatedness.
my %order = (
	Homo_sapiens => 1,
	Pan_troglodytes => 2,
	Gorilla_gorilla => 3,
	Pongo_abelii => 4,
	Nomascus_leucogenys => 5,
	Macaca_mulatta => 6,
	Callithrix_jacchus => 7,
	Tarsius_syrichta => 8,
	Microcebus_murinus => 9,
	Otolemur_garnettii => 10,
	Tupaia_belangeri => 11,
	Mus_musculus => 12,
	Rattus_norvegicus => 13,
	Dipodomys_ordii => 14,
	Cavia_porcellus => 15,
	Spermophilus_tridecemlineatus => 16,
	Oryctolagus_cuniculus => 17,
	Ochotona_princeps => 18,
	Bos_taurus => 19,
	Tursiops_truncatus => 20,
	Vicugna_pacos => 21,
	Sus_scrofa => 22,
	Pteropus_vampyrus => 23,
	Myotis_lucifugus => 24,
	Equus_caballus => 25,
	Felis_catus => 26,
	Canis_familiaris => 27,
	Ailuropoda_melanoleuca => 28,
	Erinaceus_europaeus => 29,
	Sorex_araneus => 30,
	Loxodonta_africana => 31,
	Procavia_capensis => 32,
	Echinops_telfairi => 33,
	Dasypus_novemcinctus => 34,
	Choloepus_hoffmanni => 35
);

my @dinucs_wc = ('AU', 'UA', 'CG', 'GC');
my @dinucs_gu = ('GU', 'UG');
my @dinucs_mm = ('AA', 'AC', 'AG', 'CA', 'CC', 'CU', 'GA', 'GG', 'UC', 'UU');
my @dinucs = (@dinucs_wc, @dinucs_gu, @dinucs_mm);

my $ref_id = 9606;
my $rfam_file = "$dir/rfam/map_$ref_id.txt";
my $clan_file = "$dir/rfam/rfam_clans.txt";
my $cats_file = "$dir/rfam/rfam_categories.txt";
my $rfam_data = read_from_file($rfam_file);
my $clan_data = read_from_file($clan_file);
my $cats_data = read_from_file($cats_file);
my %rfam_desc = $rfam_data =~ /^(RF\d{5})\t([^\t]+)/gm;
my %clan = $clan_data =~ /^(RF\d{5})\t([^\t]+)/gm;
my %clan_desc = $clan_data =~ /^RF\d{5}\t([^\t]+)\t([^\t\n]+)$/gm;
my %cats = $cats_data =~ /^(RF\d{5})\t([^\t\n]+)/gm;

my $column_width = 1;
my $row_height = 10;
my $padding = 5;
# Initialise Parameters - End
################################################################################

################################################################################
# Output File - Start
my $output_file = "$output_dir/alignment_statistics.txt";
if (!-e $output_file || $overwrite) {
	my @columns = ('ID', 'Chr', 'Location', 'Dataset', 'Flanking',
					'Alignment Length', 'Aligned RNA Length', 'RNA Length',
					'Reference Tree Length', 'GC Content', 'Gaps',
					'MPI', 'MPI Stem', 'MPI Loop', 'SCI',
					'Dinucleotide Counts', 'Paired Bases',
					'Watson-Crick Pairs', 'Wobble Pairs', 'Mismatch Pairs',
					'Category', 'Description', 'Clan ID', 'Clan Description');
	save_to_file(join("\t", @columns), $output_file);
}
my $existing_output = read_from_file($output_file);
# Output File - End
################################################################################

################################################################################
# Calculate Alignment Statistics - Start
my ($data, $cols) = read_from_delim($data_file, "\t", 1);
foreach my $line (@$data) {
	my $id = $$line[$$cols{'ID'}];
	my $chr = $$line[$$cols{'Chr'}];
	my $location = $$line[$$cols{'Location'}];
	my $dataset = $$line[$$cols{'Dataset'}];
	
	my $rfam_desc = $rfam_desc{$id};
	my $clan = $clan{$id} || '';
	my $clan_desc = $clan_desc{$clan} || '';
	my $category = $cats{$id} || '';
	
	my $aln_dir = "$data_dir/$id/$chr/$location/$dataset";
	my ($start, $stop) = $location =~ /^(\d+)\-(\d+)$/;
	my $rna_length = abs($stop - $start);
	
	next unless -e $aln_dir;
	opendir(ALN_DIR, $aln_dir);
	foreach my $aln_file (sort grep { /^(\d+)\.fa$/ } readdir(ALN_DIR)) {
		my ($flanking) = $aln_file =~ /^(\d+)\.fa$/;
		
		my $processed = $existing_output =~ /^$id\t$chr\t$location\t$dataset\t$flanking/m;
		if (!$processed || $overwrite) {
			my ($seqs, $order) = from_fasta(read_from_file("$aln_dir/$aln_file"));
			unless (scalar(keys(%$seqs)) > 0) {
				carp "No sequences in $aln_dir/$aln_file";
				next;
			}
			my @seqs = values %$seqs;
			
			my $alignment_length = length($seqs[0]);
			my $aligned_rna_length = $alignment_length - 2*$flanking;
			if (!-e "$aln_dir/$flanking.nh") {
				print "rm $aln_dir/$aln_file\n";
				next;
			}
			my $tree_length = tree_length(read_from_file("$aln_dir/$flanking.nh"));
			my $str = read_from_file("$aln_dir/$flanking.structure.txt");
			my $sci = sci($seqs, $str);
			
			my $gc_content = gc_content_multiple(\@seqs);
			my ($gap_count, $total) = (0, 0);
			foreach my $seq (@seqs) {
				$gap_count += () = $seq =~ /[\-\.]/g;
				$total += length($seq);
			}
			my $gaps = sprintf("%.2f", ($gap_count/$total)*100);
			
			my $mpi = pairwise_identity(\@seqs, "exclude_gaps");
			$mpi = sprintf("%.2f", $mpi*100);
			
			# To extract loop and stem sequences, add the structure as the first
			# sequence, and remove columns as necessary.
			$$seqs{'str'} = $str;
			$$order{'str'} = -1;
			my %stem_seqs = excise_columns($seqs, $order, ['\..+']);
			my %loop_seqs = excise_columns($seqs, $order, ['[^\.].+']);
			delete $$seqs{'str'};
			delete $$order{'str'};
			delete $stem_seqs{'str'};
			delete $loop_seqs{'str'};
			my @stem_seqs = values(%stem_seqs);
			my @loop_seqs = values(%loop_seqs);
			my $mpi_stem = length($stem_seqs[0]) > 0 ? pairwise_identity(\@stem_seqs, "exclude_gaps") : 0;
			$mpi_stem = sprintf("%.2f", $mpi_stem*100);
			my $mpi_loop = length($loop_seqs[0]) > 0 ? pairwise_identity(\@loop_seqs, "exclude_gaps") : 0;
			$mpi_loop = sprintf("%.2f", $mpi_loop*100);
			
			my %stems = parse_structure($str);
			my %dinucs = map { $_ => 0 } @dinucs;
			my $pairs = 0;
			foreach my $seq (@seqs) {
				$seq =~ tr/T/U/;
				my @seq = split(//, $seq);
				while (my ($pos1, $pos2) = each(%stems)) {
					if ($seq[$pos1] ne '-' && $seq[$pos2] ne '-') {
						$dinucs{$seq[$pos1].$seq[$pos2]}++;
						$pairs++;
					}
				}
			}
			my @dinuc_counts = map { $dinucs{$_} } sort @dinucs;
			my $dinuc_counts = join(',', @dinuc_counts);
			my @pairs_wc = map { $dinucs{$_} } sort @dinucs_wc;
			my @pairs_gu = map { $dinucs{$_} } sort @dinucs_gu;
			my @pairs_mm = map { $dinucs{$_} } sort @dinucs_mm;
			my $pairs_wc = sum(@pairs_wc);
			my $pairs_gu = sum(@pairs_gu);
			my $pairs_mm = sum(@pairs_mm);
			
			my $stats .= "\n$id\t$chr\t$location\t$dataset\t$flanking".
						"\t$alignment_length\t$aligned_rna_length\t$rna_length".
						"\t$tree_length\t$gc_content\t$gaps".
						"\t$mpi\t$mpi_stem\t$mpi_loop\t$sci".
						"\t$dinuc_counts\t$pairs".
						"\t$pairs_wc\t$pairs_gu\t$pairs_mm".
						"\t$category\t$rfam_desc\t$clan\t$clan_desc";
			save_to_file($stats, $output_file, 'append');
			
			# Create 'barcode' visualisations of the alignment, wherein
			# we produce usefully blurred images.
			my $blur_none_file = "$aln_dir/$flanking\_standard.png";
			if (!-e $blur_none_file || $overwrite) {
				my $blur_column_file = "$aln_dir/$flanking\_barcode.png";
				my $blur_row_file = "$aln_dir/$flanking\_blurred.png";
				my $blur_both_file = "$aln_dir/$flanking\_blurred_barcode.png";
				colour_alignment($seqs, \%order, $blur_none_file, 'nuc', $column_width, $row_height);
				barcode($blur_none_file, $blur_column_file);
				colour_alignment($seqs, \%order, $blur_row_file, 'nuc', $column_width, $row_height, 2);
				barcode($blur_row_file, $blur_both_file);
				
				if ($flanking > 0) {
					my $rna_start = ($column_width * $flanking) + 1;
					my $rna_end = $rna_start + ($column_width * $aligned_rna_length) - 1;
					annotate_positions($blur_none_file, $blur_none_file, "$rna_start,$rna_end", 2, 3);
					annotate_positions($blur_column_file, $blur_column_file, "$rna_start,$rna_end", 2, 3);
					annotate_positions($blur_row_file, $blur_row_file, "$rna_start,$rna_end", 2, 3);
					annotate_positions($blur_both_file, $blur_both_file, "$rna_start,$rna_end", 2, 3);
				}
			}
		}
	}
	closedir(ALN_DIR);
}
# Calculate Alignment Statistics - End
################################################################################

