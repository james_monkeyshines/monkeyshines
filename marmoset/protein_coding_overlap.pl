#!/usr/bin/perl
use strict;
use warnings;
use Carp;

=pod

=head1 Name

protein_coding_overlap.pl dir data_file

=head1 Description

Detect whether a list of genomic locations overlaps, or is within
2kB of, a protein-coding gene. Note that being upstream or downstream
of a gene is only recorded if the location does not overlap a gene.
The <data_file> must have columns headed 'Chr', 'Start', and 'End'.
Results will be saved in <dir>/output/protein_coding_overlap.txt'.

The script uses the Ensembl API (http://www.ensembl.org/info/data/api.html),
and treats anything with the Ensembl biotype 'protein_coding' as, erm,
protein-coding. The 'Start' value is expected to be 0-based ("UCSC-style"),
and will be converted to 1-based for use with Ensembl. Each location can
either be Within or Overlap one or more UTRs, CDSs, or Introns. If there are
multiple transcripts for an overlapping gene, the position within each
is reported if they are different. E.g. if a location is within an intron
in two transcripts its position will be "Within Intron"; if it is within
a CDS in one transcript and an intron in another, its position will be
"Within CDS; Within Intron". If a location overlaps two or more gene
regions, they will be separated by commas, e.g. "Overlap CDS,Overlap Intron".

=head1 Author

James Allen
(james@monkeyshines.co.uk)

=head1 Copyright

Copyright 2012 James Allen; University of Manchester

=head1 Licence

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program. If not, see <http://www.gnu.org/licenses/>.

=cut

use Bio::EnsEMBL::Registry;
use File::Path qw(make_path);

use MonkeyShines::Marmoset qw (gene_position flanking_genes);
use MonkeyShines::Utils qw (read_from_file read_from_delim save_to_file);

################################################################################
# Initialise Parameters - Start
my $dir = $ARGV[0];
croak "Directory must be specified" unless $dir;
croak "Directory '$dir' does not exist" unless -e $dir;

my $data_file = $ARGV[1];
croak "Data file must be specified" unless $data_file;
croak "Data file '$data_file' does not exist" unless -e $data_file;

my $output_dir = "$dir/output";
make_path $output_dir unless -e $output_dir;

my $overwrite = 0;
my $flanking = 2000;
my $ref_species = 'Homo_sapiens';
# Initialise Parameters - End
################################################################################

################################################################################
# Load Ensembl Registry - Start
Bio::EnsEMBL::Registry->load_registry_from_db(
	-host => 'ensembldb.ensembl.org',
	-user => 'anonymous'
);
# Load Ensembl Registry - End
################################################################################

################################################################################
# Output File - Start
my ($data, $cols) = read_from_delim($data_file, "\t", 1);
my $output_file = "$output_dir/protein_coding_overlap.txt";
if (!-e $output_file || $overwrite) {
	my @output_cols;
	foreach my $id (sort {$$cols{$a} <=> $$cols{$b}} keys %$cols) {
		push @output_cols, $id;
	}
	push @output_cols, 'Gene Position';
	save_to_file(join("\t", @output_cols), $output_file);
}
my $existing_output = read_from_file($output_file);
# Output File - End
################################################################################

################################################################################
# Find Genes Within a Region - Start
foreach my $line (@$data) {
	my $chr = $$line[$$cols{'Chr'}];
	my $start = $$line[$$cols{'Start'}];
	my $end = $$line[$$cols{'End'}];
	my $dataset = $$line[$$cols{'Dataset'}];
	
	my $processed = $existing_output =~ /$chr\t[^\n]+\t$start\t$end\t$dataset\t/m;
	if (!$processed || $overwrite) {
		$chr =~ s/chr//;
		$start += 1;
		my ($position) = gene_position($chr, $start, $end, $ref_species);
		if ($position eq '') {
			($position) = flanking_genes($chr, $start, $end, $ref_species, $flanking);
		}
		save_to_file("\n".join("\t", @$line)."\t$position", $output_file, 'append');
	}
}
# Find Genes Within a Region - End
################################################################################

